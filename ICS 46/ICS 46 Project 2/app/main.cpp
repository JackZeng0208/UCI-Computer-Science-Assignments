#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include "Queue.hpp"
using namespace std;

void find_min(unsigned int &index, unsigned int &size, vector<Queue<unsigned int>> all_lines)
{
    for (int i = 0; i < all_lines.size(); i++)
    {
        if (all_lines[i].size() < size)
        {
            index = i;
            size = all_lines[i].size();
        }
    }
}

int register_left(vector<pair<unsigned int, bool>> register_status)
{
    int left_in_register = 0;
    for (int i = 0; i < register_status.size(); i++)
    {
        if (register_status[i].first != 0)
            left_in_register++;
        else if (register_status[i].second == false)
            left_in_register++;
    }
    return left_in_register;
}

void exited_output(unsigned int current_time, unsigned int output_index, unsigned int output_size)
{
    std::cout << current_time << " "
              << "entered line"
              << " " << output_index << " "
              << "length " << output_size << endl;
}

void initialize(unsigned int &when_arrive, unsigned int &customer_number, vector<pair<unsigned int, unsigned int>> customer_status, unsigned int customer_vector_index)
{
    when_arrive = customer_status[customer_vector_index].first;
    customer_number = customer_status[customer_vector_index].second;
}

int main()
{
    unsigned int sim_length = 0, register_num = 0, max_line_length = 0;
    char line_input_model;
    unsigned int actual_sim_length = 0;
    vector<Queue<unsigned int>> all_lines;
    Queue<unsigned int> single_line;
    vector<pair<unsigned int, bool>> register_status;
    vector<unsigned int> time;
    vector<pair<unsigned int, unsigned int>> customer_status;
    cin >> sim_length;
    cin >> register_num;
    cin >> max_line_length;
    cin >> line_input_model;
    unsigned int temp = register_num;
    actual_sim_length = sim_length * 60;
    for (int i = 0; i < temp; i++)
        all_lines.push_back(Queue<unsigned int>{});

    pair<unsigned int, bool> temp_pair(0, false);
    unsigned int register_time = 0;
    for (int i = 0; i < temp; i++)
    {
        cin >> register_time;
        register_status.push_back(temp_pair);
        time.push_back(register_time);
    }
    string num_string = "";
    unsigned int when_arrive = 0;
    while (true)
    {
        cin >> num_string;
        if (num_string == "END")
            break;
        if (num_string != "END")
        {
            int num = stoi(num_string);
            cin >> when_arrive;
            pair<unsigned int, unsigned int> customer_temp(when_arrive, num);
            customer_status.push_back(customer_temp);
        }
    }
    std::cout << "LOG" << endl;
    std::cout << "0 start" << endl;
    unsigned int current_time = 0;
    unsigned int total_line_number = 0;
    unsigned int customer_vector_index = 0;
    unsigned int customer_number = 0;
    double total_wait_time = 0.0;
    unsigned int total_exited_line_number = 0;
    unsigned int total_exited_register_number = 0;
    when_arrive = 0;
    unsigned int total_lost_number = 0;
    unsigned int left_in_line = 0;
    if (line_input_model == 'M')
    {
        do
        {
            if (current_time >= actual_sim_length)
                break;
            unsigned int customer_size = customer_status.size();
            if (customer_vector_index <= customer_size)
                initialize(when_arrive, customer_number, customer_status, customer_vector_index);
            if (current_time == when_arrive)
            {
                unsigned int min_index = 0;
                unsigned int min_size = 0;
                for (int i = 0; i < customer_number; i++)
                {
                    min_size = all_lines[0].size();
                    min_index = 0;
                    find_min(min_index, min_size, all_lines);
                    if (max_line_length != min_size)
                    {
                        total_line_number++;
                        all_lines[min_index].enqueue(current_time);
                        int output_size = min_size + 1;
                        int output_index = min_index + 1;
                        exited_output(current_time, output_index, output_size);
                    }
                    else if (max_line_length == min_size)
                    {
                        total_lost_number++;
                        std::cout << current_time << " "
                                  << "lost" << endl;
                    }
                }
                customer_vector_index++;
            }
            int register_status_size = register_status.size();
            for (int i = 0; i < register_status_size; i++)
            {
                int output_line = i + 1;
                int output_size;
                int output_wait_time;
                if (register_status[i].first == time[i])
                {
                    std::cout << current_time << " "
                              << "exited register"
                              << " " << output_line << endl;
                    total_exited_register_number++;
                    register_status[i].first = 0;
                    register_status[i].second = false;
                }
                if (register_status[i].second == false)
                {
                    if (all_lines[i].size() != 0)
                    {
                        output_size = all_lines[i].size() - 1;
                        output_wait_time = current_time - all_lines[i].front();
                        std::cout << current_time << " "
                                  << "exited line"
                                  << " " << output_line << " "
                                  << "length"
                                  << " " << output_size << " "
                                  << "wait time"
                                  << " " << output_wait_time << endl;
                        total_wait_time = total_wait_time + output_wait_time;
                        all_lines[i].dequeue();
                        register_status[i].second = true;
                        total_exited_line_number++;
                        std::cout << current_time << " "
                                  << "entered register"
                                  << " " << output_line << endl;
                    }
                    else
                        continue;
                }
            }
            for (int i = 0; i < register_status_size; i++)
            {
                if (register_status[i].second == false)
                    continue;
                else if (register_status[i].second == true)
                    register_status[i].first++;
            }
            current_time++;
        } while (current_time < actual_sim_length);
    }

    if (line_input_model == 'S')
    {
        do
        {
            if (current_time >= actual_sim_length)
                break;
            unsigned int customer_size = customer_status.size();
            if (customer_vector_index <= customer_size)
                initialize(when_arrive, customer_number, customer_status, customer_vector_index);
            if (current_time == when_arrive)
            {
                int line_size = single_line.size();
                for (int i = 0; i < customer_number; i++)
                {
                    if (max_line_length != line_size)
                    {
                        total_line_number++;
                        single_line.enqueue(current_time);
                        int output_size = line_size + 1;
                        int output_index = 1;
                        exited_output(current_time, output_index, output_size);
                    }
                    else if (max_line_length == line_size)
                    {
                        total_lost_number++;
                        std::cout << current_time << " "
                                  << "lost" << endl;
                    }
                }
                customer_vector_index++;
            }
            int register_status_size = register_status.size();
            for (int i = 0; i < register_status_size; i++)
            {
                int output_line = 1;
                int output_size;
                int output_wait_time;
                if (register_status[i].first == time[i])
                {
                    std::cout << current_time << " "
                              << "exited register"
                              << " " << output_line << endl;
                    total_exited_register_number++;
                    register_status[i].first = 0;
                    register_status[i].second = false;
                }
                if (register_status[i].second == false)
                {
                    if (single_line.size() != 0)
                    {

                        output_size = single_line.size() - 1;
                        output_wait_time = current_time - single_line.front();
                        std::cout << current_time << " "
                                  << "exited line"
                                  << " " << output_line << " "
                                  << "length"
                                  << " " << output_size << " "
                                  << "wait time"
                                  << " " << output_wait_time << endl;
                        total_wait_time = total_wait_time + output_wait_time;
                        single_line.dequeue();
                        total_exited_line_number++;
                        register_status[i].second = true;
                        std::cout << current_time << " "
                                  << "entered register"
                                  << " " << output_line << endl;
                    }
                }
            }
            for (int i = 0; i < register_status_size; i++)
            {
                if (register_status[i].second == false)
                    continue;
                else if (register_status[i].second == true)
                    register_status[i].first++;
            }
            current_time++;
        } while (current_time < actual_sim_length);
    }
    std::cout << actual_sim_length << " "
              << "end" << endl;

    double average_wait_time;
    for (int i = 0; i < all_lines.size(); i++)
        left_in_line += all_lines[i].size();
    average_wait_time = total_wait_time / total_exited_line_number;
    std::cout << endl;
    std::cout << "STATS" << endl;
    std::cout << "Entered Line    : " << total_line_number << endl;
    std::cout << "Exited Line     : " << total_exited_line_number << endl;
    std::cout << "Exited Register : " << total_exited_register_number << endl;
    std::printf("Avg Wait Time   : %.2f", average_wait_time);
    std::cout << endl;
    if (line_input_model == 'M')
        std::cout << "Left In Line    : " << left_in_line << endl;
    else
        std::cout << "Left In Line    : " << single_line.size() << endl;
    std::cout << "Left In Register: " << register_left(register_status) << endl;
    std::cout << "Lost            : " << total_lost_number << endl;
    return 0;
}