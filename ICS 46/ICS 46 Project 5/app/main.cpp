// main.cpp
//
// ICS 46 Winter 2022
// Project #5: Rock and Roll Stops the Traffic
//
// This is the program's main() function, which is the entry point for your
// console user interface.
#include <iostream>
#include <string>
#include <cstdio>
#include <vector>
#include <map>
#include <iomanip>
#include <functional>
#include "Digraph.hpp"
#include "RoadMapReader.hpp"
#include "RoadSegment.hpp"
#include "RoadMapWriter.hpp"
#include "Trip.hpp"
#include "TripMetric.hpp"
#include "TripReader.hpp"
using namespace std;
// RoadSegment: miles and milesPerHour
function<double(RoadSegment)> dis = [](const RoadSegment road)
{
    return road.miles;
};
function<double(RoadSegment)> time_calculation = [](const RoadSegment road)
{
    double result = road.miles / road.milesPerHour * 60 * 60;
    return result;
};
void print_time(double t_in_sec)
{
    int hours = t_in_sec / 3600;
    double temp = t_in_sec - hours * 3600;
    int mins = temp / 60;
    double secs = temp - mins * 60;
    if (hours != 0)
        printf("%d hrs ", hours);
    if (mins != 0 || hours != 0)
        printf("%d mins ", mins);
    printf("%2.1f secs", secs);
}

int main()
{
    InputReader input_reader(cin);
    RoadMapReader road_map_reader;
    RoadMap road_map = road_map_reader.readRoadMap(input_reader);

    TripReader trip_reader;
    if (road_map.isStronglyConnected() == false)
    {
        printf("Disconnected Map");
        return 0;
    }
    vector<Trip> trip = trip_reader.readTrips(input_reader);
    double output_time = 0.0;
    double output_distance = 0.0;
    for (vector<Trip>::iterator i = trip.begin(); i != trip.end(); i++)
    {
        int temp = i->endVertex;
        vector<int> dir;
        dir.push_back(temp);
        if (i->metric == TripMetric::Distance)
        {
            string starting_point = road_map.vertexInfo(i->startVertex);
            string destination = road_map.vertexInfo(i->endVertex);
            output_distance = 0.0;
            std::cout << "Shortest distance from " << starting_point << " "
                 << "to"
                 << " " << destination << endl;
            map<int, int> pathway = road_map.findShortestPaths(i->startVertex, dis);
            while (temp != i->startVertex)
            {
                int t = pathway.at(temp);
                dir.push_back(t);
                temp = t;
            }
            int start_vertex = dir.back();
            std::cout << "  "
                 << "Begin at " << road_map.vertexInfo(start_vertex) << endl;
            dir.pop_back();
            while (true)
            {
                int direction_size = dir.size();
                if (direction_size <= 0)
                    break;
                int end_vertex = dir.back();
                string location = road_map.vertexInfo(end_vertex);
                double mile = road_map.edgeInfo(start_vertex, end_vertex).miles;
                std::cout << "  "
                     << "Continue to " << location << " "
                     << "(" << fixed << setprecision(1) << mile << " miles)" << endl;
                output_distance += mile;
                start_vertex = end_vertex;
                dir.pop_back();
            }
            printf("Total distance: %.1f miles\n", output_distance);
        }
        
        if (i->metric == TripMetric::Time)
        {
            string starting_point = road_map.vertexInfo(i->startVertex);
            string destination = road_map.vertexInfo(i->endVertex);
            std::cout << "Shortest driving time from " << starting_point << " "
                 << "to"
                 << " " << destination << endl;
            output_time = 0.0;
            map<int, int> pathway = road_map.findShortestPaths(i->startVertex, time_calculation);
            while (temp != i->startVertex)
            {
                int t = pathway.at(temp);
                dir.push_back(t);
                temp = t;
            }
            int start_vertex = dir.back();
            dir.pop_back();
            std::cout << "  "
                 << "Begin at " << road_map.vertexInfo(start_vertex) << endl;
            while (true)
            {
                int direction_size = dir.size();
                if (direction_size <= 0)
                    break;
                int end_vertex = dir.back();
                string location = road_map.vertexInfo(end_vertex);
                double mile = road_map.edgeInfo(start_vertex, end_vertex).miles;
                double mph = road_map.edgeInfo(start_vertex, end_vertex).milesPerHour;
                double temp_time = time_calculation(road_map.edgeInfo(start_vertex, end_vertex));
                std::cout << "  "
                     << "Continue to " << location << " "
                     << "(" << fixed << setprecision(1) << mile << " miles @ " << fixed << setprecision(1) << mph << "mph"
                     << " = ";
                print_time(temp_time);
                std::cout << ")" << endl;
                start_vertex = end_vertex;
                output_time += temp_time;
                dir.pop_back();
            }
            printf("Total time: ");
            print_time(output_time);
            std::cout << endl;
        }
        std::cout << endl;
    }
    return 0;
}