#include <ics46/factory/DynamicFactory.hpp>
#include "JackZengOthelloAI.hpp"
#include "OthelloException.hpp"

ICS46_DYNAMIC_FACTORY_REGISTER(OthelloAI, yixiaz8::JackZengOthelloAI, "Yixiao(Jack) Zeng's AI (Required)");
void yixiaz8::JackZengOthelloAI::colors(OthelloCell &current_color, const OthelloGameState &state)
{
    if (state.isWhiteTurn() == true)
        current_color = OthelloCell::white;
    else
        current_color = OthelloCell::black;
}

void yixiaz8::JackZengOthelloAI::colors(OthelloCell &current_color, OthelloGameState &state)
{
    if (state.isWhiteTurn() == true)
        current_color = OthelloCell::white;
    else
        current_color = OthelloCell::black;
}

std::pair<int, int> yixiaz8::JackZengOthelloAI::highest_evalution(int &best_value, int current_value, std::pair<int, int> best_movement, std::pair<int, int> possible_movement)
{
    if (best_value < current_value)
    {
        best_value = current_value;
        best_movement = std::make_pair(possible_movement.first, possible_movement.second);
    }
    return best_movement;
}

std::pair<int, int> yixiaz8::JackZengOthelloAI::chooseMove(const OthelloGameState &state)
{
    int best_value = INT32_MIN;
    std::vector<std::pair<int, int>> possible_movement = possible_move(state, state.board());
    OthelloCell current_color;
    std::pair<int, int> best_movement = std::make_pair(0, 0);
    colors(current_color, state);
    if (possible_movement.empty() == false)
    {
        for (std::vector<std::pair<int, int>>::iterator i = possible_movement.begin(); i != possible_movement.end(); i++)
        {
            std::unique_ptr<OthelloGameState> game_state_temp = state.clone();
            game_state_temp->makeMove(i->first, i->second);
            int current_value = search(*game_state_temp, 3, current_color);
            if (current_color == OthelloCell::white)
            {
                int temp = game_state_temp->whiteScore() - game_state_temp->blackScore();
                current_value += temp;
            }
            else
            {
                int temp = game_state_temp->blackScore() - game_state_temp->whiteScore();
                current_value += temp;
            }
            std::pair<int, int> pair_temp = std::make_pair(i->first, i->second);
            best_movement = highest_evalution(best_value, current_value, best_movement, pair_temp);
        }
    }
    else
    {
        throw OthelloException("Possible movement is empty");
    }
    return best_movement;
}

int yixiaz8::JackZengOthelloAI::search(OthelloGameState &state, int depth, OthelloCell &current_color)
{
    if (depth == 0)
    {
        return Evaluation(state);
    }
    else
    {
        int my_score = INT32_MIN;
        int opponent_score = INT32_MAX;
        OthelloCell color_temp;
        colors(color_temp, state);
        std::vector<std::pair<int, int>> move_temp = possible_move(state, state.board());
        if (current_color == color_temp)
        {
            for (std::vector<std::pair<int, int>>::iterator i = move_temp.begin(); i != move_temp.end(); i++)
            {
                std::unique_ptr<OthelloGameState> game_state_temp = state.clone();
                game_state_temp->makeMove(i->first, i->second);
                int current_value = search(*game_state_temp, depth - 1, current_color);
                if (my_score < current_value)
                    my_score = current_value;
            }
            return my_score;
        }
        else
        {
            for (std::vector<std::pair<int, int>>::iterator i = move_temp.begin(); i != move_temp.end(); i++)
            {
                std::unique_ptr<OthelloGameState> game_state_temp = state.clone();
                game_state_temp->makeMove(i->first, i->second);
                int current_value = search(*game_state_temp, depth - 1, current_color);
                if (opponent_score > current_value)
                    opponent_score = current_value;
            }
            return opponent_score;
        }
    }
}
std::vector<std::pair<int, int>> yixiaz8::JackZengOthelloAI::possible_move(const OthelloGameState &state, const OthelloBoard &game_board)
{
    std::vector<std::pair<int, int>> movement;
    for (int i = 0; i < game_board.height(); i++)
    {
        for (int j = 0; j < game_board.width(); j++)
        {
            std::pair<int, int> move = std::make_pair(i, j);
            if (state.isValidMove(i, j) == true)
                movement.push_back(move);
        }
    }
    return movement;
}

int yixiaz8::JackZengOthelloAI::Evaluation(OthelloGameState &state)
{
    OthelloCell opponent_color;
    OthelloCell player_color;
    const OthelloBoard &board_copy = state.board();
    int evaluation_value = 0;
    colors(player_color, state);
    if (player_color == OthelloCell::black)
        opponent_color = OthelloCell::white;
    else
        opponent_color = OthelloCell::black;

    int static_evaluation_table_1[8][8] = {
        {4, -3, 2, 2, 2, 2, -3, 4},
        {-3, -4, -1, -1, -1, -1, -4, -3},
        {2, -1, 1, 0, 0, 1, -1, 2},
        {2, -1, 0, 1, 1, 0, -1, 2},
        {2, -1, 0, 1, 1, 0, -1, 2},
        {2, -1, 1, 0, 0, 1, -1, 2},
        {-3, -4, -1, -1, -1, -1, -4, -3},
        {4, -3, 2, 2, 2, 2, -3, 4}};

    int static_evaluation_table_2[8][8] = {
        {100, -10, 11, 6, 6, 11, -10, 100},
        {-10, -20, 1, 2, 2, 1, -20, -10},
        {10, 1, 5, 4, 4, 5, 1, 10},
        {6, 2, 4, 2, 2, 4, 2, 6},
        {6, -2, 4, 2, 2, 4, 2, 6},
        {10, 1, 5, 4, 4, 5, 1, 10},
        {-10, -20, 1, 2, 2, 1, -20, -10},
        {100, -10, 11, 6, 6, 11, -10, 100}};

    int static_evaluation_table_3[8][8] = {
        {500, -150, 30, 10, 10, 30, -150, 500},
        {-150, -250, 0, 0, 0, 0, -250, -150},
        {30, 0, 1, 2, 2, 1, 0, 30},
        {10, 0, 2, 16, 16, 2, 0, 30},
        {10, 0, 2, 16, 16, 2, 0, 30},
        {30, 0, 1, 2, 2, 1, 0, 30},
        {-150, -250, 0, 0, 0, 0, -250, -150},
        {500, -150, 30, 10, 10, 30, -150, 500}};

    int static_evaluation_table_4[8][8] = {
        {10000, -3000, 1000, 800, 800, 1000, -3000, 10000},
        {-3000, -5000, -450, -500, -500, -450, -5000, -3000},
        {1000, -450, 30, 10, 10, 30, -450, 1000},
        {800, -500, 10, 50, 50, 10, -500, 800},
        {800, -500, 10, 50, 50, 10, -500, 800},
        {1000, -450, 30, 10, 10, 30, -450, 1000},
        {-3000, -5000, -450, -500, -500, -450, -5000, -3000},
        {10000, -3000, 1000, 800, 800, 1000, -3000, 10000}};

    int which_table = 1;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (which_table == 1)
            {
                if (board_copy.cellAt(i, j) == player_color)
                    evaluation_value += static_evaluation_table_1[i][j];
                else if (board_copy.cellAt(i, j) == opponent_color)
                    evaluation_value -= static_evaluation_table_1[i][j];
            }
            if (which_table == 2)
            {
                if (board_copy.cellAt(i, j) == player_color)
                    evaluation_value += static_evaluation_table_2[i][j];
                else if (board_copy.cellAt(i, j) == opponent_color)
                    evaluation_value -= static_evaluation_table_2[i][j];
            }
            if (which_table == 3)
            {
                if (board_copy.cellAt(i, j) == player_color)
                    evaluation_value += static_evaluation_table_3[i][j];
                else if (board_copy.cellAt(i, j) == opponent_color)
                    evaluation_value -= static_evaluation_table_3[i][j];
            }
            if (which_table == 4)
            {
                if (board_copy.cellAt(i, j) == player_color)
                    evaluation_value += static_evaluation_table_4[i][j];
                else if (board_copy.cellAt(i, j) == opponent_color)
                    evaluation_value -= static_evaluation_table_4[i][j];
            }
        }
    }
    return evaluation_value;
}