#ifndef JACKZENGOTHELLOAI_HPP
#define JACKZENGOTHELLOAI_HPP
#include "OthelloAI.hpp"
#include "OthelloGameState.hpp"
#include "OthelloBoard.hpp"

namespace yixiaz8
{
    class JackZengOthelloAI : public OthelloAI
    {
        public:
            virtual std::pair<int, int> chooseMove(const OthelloGameState& state) override;
            std::vector<std::pair<int, int>> possible_move(const OthelloGameState& state, const OthelloBoard& game_board);
            int search(OthelloGameState& state, int depth, OthelloCell& current_color);
            int Evaluation (OthelloGameState& state);
            void colors(OthelloCell& current_color, const OthelloGameState& state);
            void colors(OthelloCell& current_color, OthelloGameState& state);
            std::pair<int, int> highest_evalution(int& best_value, int current_value, std::pair<int, int> best_movement, std::pair<int, int> possible_movement);
    };
}
#endif