// WordChecker.cpp
//
// ICS 46 Winter 2022
// Project #4: Set the Controls for the Heart of the Sun
//
// Replace and/or augment the implementations below as needed to meet
// the requirements.

#include "WordChecker.hpp"
#include <vector>
#include <string>
#include <algorithm>

WordChecker::WordChecker(const Set<std::string> &words)
    : words{words}
{
}

bool WordChecker::wordExists(const std::string &word) const
{
    return words.contains(word);
}

bool WordChecker::results_reach_ending(const std::string &word, std::vector<std::string> &results) const
{
    std::vector<std::string>::iterator it;
    it = std::find(results.begin(), results.end(), word);
    if (it == results.end())
        return true;
    else
        return false;
}

void WordChecker::push_back_word(const std::string &word, std::vector<std::string> &results) const
{
    if (results_reach_ending(word, results) == true)
        if (wordExists(word) == true)
            results.push_back(word);
}
void WordChecker::swap_pair(const std::string &word, std::vector<std::string> &results, int word_size) const
{
    word_size--;
    for (int i = 0; i < word_size; i++)
    {
        std::string temp_word = word;
        char temp = temp_word[i];
        temp_word[i] = temp_word[i + 1];
        temp_word[i + 1] = temp;
        push_back_word(temp_word, results);
    }
}

void WordChecker::delete_char(const std::string &word, std::vector<std::string> &results, int word_size) const
{
    std::string temp_word = word;
    for (int i = 0; i < word_size; i++)
    {
        temp_word = word;
        temp_word.erase(i, 1);
        push_back_word(temp_word, results);
    }
}

void WordChecker::splitting(const std::string &word, std::vector<std::string> &results, int word_size) const
{
    std::string temp_word = word;
    std::string sub_string_1;
    std::string sub_string_2;
    std::string result;
    for (int i = 1; i < word_size; i++)
    {
        sub_string_1 = temp_word.substr(0, i);
        sub_string_2 = temp_word.substr(i);
        result = sub_string_1 + " " + sub_string_2;
        if (results_reach_ending(sub_string_1, results) == true)
            if(wordExists(sub_string_1) == true && wordExists(sub_string_2) == true)
                results.push_back(result);
    }
}

void WordChecker::replacing(const std::string &word, std::vector<std::string> &results, int word_size) const
{
    std::string temp_word = word;
    for (int i = 0; i < word_size; i++)
    {
        for (int j = 0; j < 26; j++)
        {
            temp_word = word;
            std::string temp1 = Capitalized_Alphabets.substr(j, 1);
            temp_word.replace(i, 1, temp1);
            push_back_word(temp_word, results);
        }
    }
}

void WordChecker::in_between(const std::string &word, std::vector<std::string> &results, int word_size) const
{
    std::string temp_word = word;
    word_size += 1;
    for (int i = 0; i < word_size; i++)
    {
        for (int j = 0; j < 26; j++)
        {
            temp_word = word;
            std::string temp1 = Capitalized_Alphabets.substr(j, 1);
            temp_word.insert(i, temp1);
            push_back_word(temp_word, results);
        }
    }
}

std::vector<std::string> WordChecker::findSuggestions(const std::string &word) const
{
    // Nonsensical code because the compiler requires the member variables
    // 'words' to be used somewhere, or else it becomes a warning (which
    // turns into an error).
    std::vector<std::string> all_result;
    int word_size = word.size();
    if (wordExists(word) == false)
    {
        swap_pair(word, all_result, word_size);
        delete_char(word, all_result, word_size);
        splitting(word, all_result, word_size);
        replacing(word, all_result, word_size);
        in_between(word, all_result, word_size);
    }
    return all_result;
}
