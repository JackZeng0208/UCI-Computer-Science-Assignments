// WordChecker.hpp
//
// ICS 46 Winter 2022
// Project #4: Set the Controls for the Heart of the Sun
//
// The WordChecker class can check the spelling of single words and generate
// suggestions for words that have been misspelled.
//
// You can add anything you'd like to this class, but you will not be able
// to modify the declarations of any of its member functions, since the
// provided code calls into this class and expects it to look as originally
// given.
//
// You are permitted to use the C++ Standard Library in this class.

#ifndef WORDCHECKER_HPP
#define WORDCHECKER_HPP

#include <string>
#include <vector>
#include "Set.hpp"



class WordChecker
{
public:
    // The constructor requires a Set of words to be passed into it.  The
    // WordChecker will store a reference to a const Set, which it will use
    // whenever it needs to look up a word.
    WordChecker(const Set<std::string>& words);


    // wordExists() returns true if the given word is spelled correctly,
    // false otherwise.
    bool wordExists(const std::string& word) const;


    // findSuggestions() returns a vector containing suggested alternative
    // spellings for the given word, using the five algorithms described in
    // the project write-up.
    std::vector<std::string> findSuggestions(const std::string& word) const;
    void swap_pair(const std::string& word, std::vector<std::string>& results, int word_size) const;
    void in_between(const std::string& word, std::vector<std::string>& results, int word_size) const;
    void delete_char(const std::string& word, std::vector<std::string>& results, int word_size) const;
    void replacing(const std::string& word, std::vector<std::string>& results, int word_size) const;
    void splitting(const std::string& word, std::vector<std::string>& results, int word_size) const;
    bool results_reach_ending(const std::string& word, std::vector<std::string>& results) const;
    void push_back_word(const std::string& word, std::vector<std::string>& results) const;
    


private:
    const Set<std::string>& words;
    std::string Capitalized_Alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
};



#endif

