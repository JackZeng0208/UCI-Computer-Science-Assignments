// AVLSet.hpp
//
// ICS 46 Winter 2022
// Project #4: Set the Controls for the Heart of the Sun
//
// An AVLSet is an implementation of a Set that is an AVL tree, which uses
// the algorithms we discussed in lecture to maintain balance every time a
// new element is added to the set.  The balancing is actually optional,
// with a bool parameter able to be passed to the constructor to explicitly
// turn the balancing on or off (on is default).  If the balancing is off,
// the AVL tree acts like a binary search tree (e.g., it will become
// degenerate if elements are added in ascending order).
//
// You are not permitted to use the containers in the C++ Standard Library
// (such as std::set, std::map, or std::vector) to store the information
// in your data structure.  Instead, you'll need to implement your AVL tree
// using your own dynamically-allocated nodes, with pointers connecting them,
// and with your own balancing algorithms used.

#ifndef AVLSET_HPP
#define AVLSET_HPP

#include <functional>
#include "Set.hpp"

template <typename ElementType>
class AVLSet : public Set<ElementType>
{
public:
    // A VisitFunction is a function that takes a reference to a const
    // ElementType and returns no value.
    using VisitFunction = std::function<void(const ElementType &)>;

public:
    // Initializes an AVLSet to be empty, with or without balancing.
    explicit AVLSet(bool shouldBalance = true);

    // Cleans up the AVLSet so that it leaks no memory.
    ~AVLSet() noexcept override;

    // Initializes a new AVLSet to be a copy of an existing one.
    AVLSet(const AVLSet &s);

    // Initializes a new AVLSet whose contents are moved from an
    // expiring one.
    AVLSet(AVLSet &&s) noexcept;

    // Assigns an existing AVLSet into another.
    AVLSet &operator=(const AVLSet &s);

    // Assigns an expiring AVLSet into another.
    AVLSet &operator=(AVLSet &&s) noexcept;

    // isImplemented() should be modified to return true if you've
    // decided to implement an AVLSet, false otherwise.
    bool isImplemented() const noexcept override;

    // add() adds an element to the set.  If the element is already in the set,
    // this function has no effect.  This function always runs in O(log n) time
    // when there are n elements in the AVL tree.
    void add(const ElementType &element) override;

    // contains() returns true if the given element is already in the set,
    // false otherwise.  This function always runs in O(log n) time when
    // there are n elements in the AVL tree.
    bool contains(const ElementType &element) const override;

    // size() returns the number of elements in the set.
    unsigned int size() const noexcept override;

    // height() returns the height of the AVL tree.  Note that, by definition,
    // the height of an empty tree is -1.
    int height() const noexcept;

    // preorder() calls the given "visit" function for each of the elements
    // in the set, in the order determined by a preorder traversal of the AVL
    // tree.
    void preorder(VisitFunction visit) const;

    // inorder() calls the given "visit" function for each of the elements
    // in the set, in the order determined by an inorder traversal of the AVL
    // tree.
    void inorder(VisitFunction visit) const;

    // postorder() calls the given "visit" function for each of the elements
    // in the set, in the order determined by a postorder traversal of the AVL
    // tree.
    void postorder(VisitFunction visit) const;

private:
    // You'll no doubt want to add member variables and "helper" member
    // functions here.
    struct Node
    {
        ElementType key;
        Node *left;
        Node *right;
    };

    int AVL_height;
    int AVL_size;
    Node *root_node;
    Node *current_node;
    bool balance_judge;

    void delete_all(Node *node);
    void copy_all(const Node *current_root, Node *&new_root);
    void add_node(const ElementType element, Node *&node);
    void balanced(const ElementType element, Node *&node);
    int calculate_height(Node *node) const;
    void rotation(Node*& node, bool left_or_right);
    void rotate_twice(Node*& node, bool left_or_right);
    bool exist_or_not(const ElementType element, Node *node) const;
    void preorder_recursion(VisitFunction visit, Node* node) const;
    void inorder_recursion(VisitFunction visit, Node* node) const;
    void postorder_recursion(VisitFunction visit, Node* node) const;


};

template <typename ElementType>
AVLSet<ElementType>::AVLSet(bool shouldBalance)
{
    root_node = nullptr;
    balance_judge = shouldBalance;
    AVL_height = 0;
    AVL_size = 0;
}

template <typename ElementType>
AVLSet<ElementType>::~AVLSet() noexcept
{
    delete_all(root_node);
}

template <typename ElementType>
AVLSet<ElementType>::AVLSet(const AVLSet &s)
{
    balance_judge = s.balance_judge;
    AVL_size = s.AVL_size;
    AVL_height = s.AVL_height;
    copy_all(s.root_node, root_node);
}

template <typename ElementType>
AVLSet<ElementType>::AVLSet(AVLSet &&s) noexcept
{
    balance_judge = s.balance_judge;
    AVL_size = s.AVL_size;
    AVL_height = s.AVL_height;
    copy_all(s.root_node, root_node);
    delete_all(s.root_node);
}

template <typename ElementType>
AVLSet<ElementType> &AVLSet<ElementType>::operator=(const AVLSet &s)
{
    if (this == &s)
        return *this;
    else
    {
        delete_all(this->root_node);
        AVL_size = s.AVL_size;
        AVL_height = s.AVL_height;
        copy_all(s.root_node, root_node);
        return *this;
    }
}

template <typename ElementType>
AVLSet<ElementType> &AVLSet<ElementType>::operator=(AVLSet &&s) noexcept
{
    if (this == &s)
        return *this;
    else
    {
        delete_all(this->root_node);
        AVL_size = s.AVL_size;
        AVL_height = s.AVL_height;
        copy_all(s.root_node, root_node);
        delete_all(s.root_node);
        return *this;
    }
}

template <typename ElementType>
bool AVLSet<ElementType>::isImplemented() const noexcept
{
    return true;
}

template <typename ElementType>
void AVLSet<ElementType>::add(const ElementType &element)
{
    if (contains(element) == true)
        return;
    else
    {
        if (balance_judge == true)
            balanced(element, root_node);
        else
            add_node(element, root_node);
        AVL_size += 1;
    }  
}
template <typename ElementType>
bool AVLSet<ElementType>::contains(const ElementType &element) const
{
    return exist_or_not(element, root_node);
}

template <typename ElementType>
unsigned int AVLSet<ElementType>::size() const noexcept
{
    return AVL_size;
}

template <typename ElementType>
int AVLSet<ElementType>::height() const noexcept
{
    return calculate_height(root_node);
}

template <typename ElementType>
void AVLSet<ElementType>::preorder(VisitFunction visit) const
{
    preorder_recursion(visit, root_node);
}

template <typename ElementType>
void AVLSet<ElementType>::inorder(VisitFunction visit) const
{
    inorder_recursion(visit, root_node);
}

template <typename ElementType>
void AVLSet<ElementType>::postorder(VisitFunction visit) const
{
    postorder_recursion(visit, root_node);
}

// Some extra function

template <typename ElementType>
void AVLSet<ElementType>::delete_all(Node *current_node)
{
    if (current_node == nullptr)
        return;
    else
    {
        delete_all(current_node->left);
        delete_all(current_node->right);
        delete current_node;
    }
}

template <typename ElementType>
void AVLSet<ElementType>::copy_all(const Node *current_root, Node *&new_root)
{
    if (current_root == nullptr)
        new_root = nullptr;
    else
    {
        new_root = new Node{current_root->key};
        if (current_root->left == nullptr)
            if (current_root->right == nullptr)
                return;
        copy_all(current_root->left, new_root->left);
        copy_all(current_root->right, new_root->right);
    }
}

template <typename ElementType>
void AVLSet<ElementType>::add_node(const ElementType element, Node *&node)
{
    if (node == nullptr)
        node = new Node{element};
    else if (element >= node->key)
        add_node(element, node->right);
    else
        add_node(element, node->left);
}

template <typename ElementType>
void AVLSet<ElementType>::balanced(const ElementType element, Node *&node)
{
    if (node == nullptr)
        node = new Node{element};
    else if (element > node->key)
    {
        balanced(element, node->right);
        int dis = calculate_height(node->right) - calculate_height(node->left);
        if(dis >= 2)
        {
            if (element <= node->right->key)
            {
                bool left_or_right = true;
                rotate_twice(node, left_or_right);
            }
            else
            {
                bool left_or_right = false;
                rotation(node, left_or_right);
            }

        }
    }
    else
    {
        balanced(element, node->left);
        int dis = calculate_height(node->right) - calculate_height(node->left);
        if(dis >= 2)
        {
            if (element >= node->left->key)
            {
                bool left_or_right = false; //rotate right
                rotate_twice(node, left_or_right);
            }
            else
            {
                bool left_or_right = true; //rotate left
                rotation(node, left_or_right);
            }

        }
    }
}

template <typename ElementType>
int AVLSet<ElementType>::calculate_height(Node *node) const
{
    if (node == nullptr)
        return -1;
    else
    {
        int height_left = calculate_height(node->left);
        int height_right = calculate_height(node->right);
        if (height_left <= height_right)
            return height_right + 1;
        else
            return height_left + 1;
    }
}

template <typename ElementType>
void AVLSet<ElementType>::rotation(Node*& node, bool left_or_right)
{
    // left rotation: left_or_right = true
    // right rotation: left_or_right = false
    Node* temp;
    if (left_or_right == true)
    {
        temp = node->left;
        node->left = temp->right;
        temp->right = node;
        node = temp;
    }
    else if(left_or_right == false)
    {
        temp = node->right;
        node->right = temp->left;
        temp->left = node;
        node = temp;
    }
}

template <typename ElementType>
void AVLSet<ElementType>::rotate_twice(Node*& node, bool left_or_right)
{
    //left: true
    //right: false
    if (left_or_right == true)
    {
        rotation(node->right, left_or_right);
        rotation(node, false);
    }
    else
    {
        rotation(node->right, left_or_right);
        rotation(node, true);
    }
}

template <typename ElementType>
bool AVLSet<ElementType>::exist_or_not(const ElementType element, Node *node) const
{
    if (node == nullptr)
        return false;
    else if(node->key == element)
        return true;
    else
    {
        if(node->key <= element)
            return exist_or_not(element, node->right);
        else
            return exist_or_not(element, node->left);
    }
}

template <typename ElementType>
void AVLSet<ElementType>::preorder_recursion(VisitFunction visit, Node* node) const
{
    if (node == nullptr)
        return;
    else
    {
        visit(node->key);
        preorder_recursion(visit, node->left);
        preorder_recursion(visit, node->right);
    }
}

template <typename ElementType>
void AVLSet<ElementType>::inorder_recursion(VisitFunction visit,  Node* node) const
{
    if (node == nullptr)
        return;
    else
    {
        inorder_recursion(visit, node->left);
        visit(node->key);
        inorder_recursion(visit, node->right);
    }
}

template <typename ElementType>
void AVLSet<ElementType>::postorder_recursion(VisitFunction visit,  Node* node) const
{
    if (node == nullptr)
        return;
    else
    {
        postorder_recursion(visit, node->left);
        postorder_recursion(visit, node->right);
        visit(node->key);
    }
}
#endif