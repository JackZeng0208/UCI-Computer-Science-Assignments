#ifndef YIXIAOMAZEGENERATOR_HPP
#define YIXIAOMAZEGENERATOR_HPP

#include "MazeGenerator.hpp"
#include "Maze.hpp"
#include "Direction.hpp"
#include <vector>

class YixiaoMazeGenerator : public MazeGenerator
{
public:
    YixiaoMazeGenerator();
    void generateMaze(Maze& maze) override;
    void generate_maze(Maze& maze, int x, int y);
    void direction_movement(int& x, int& y);
    void is_visited_initialization(Maze& maze);

    Direction random_distribution_generator(int index);
    std::vector<Direction> adjacent_check(Maze& maze, int x, int y);
    bool check_right(int x, int y, Maze& maze);
    bool check_left(int x, int y, Maze& maze);
    bool check_up(int x, int y, Maze& maze);
    bool check_down(int x, int y, Maze& maze);
    bool is_visited_all_visited(Maze& maze);
private:
    std::vector<Direction> direct;
    std::vector<std::vector<bool>> is_visited;
    Direction potential_direction;
    int temp_x = 0;
    int temp_y = 0;
};

#endif //YIXIAOMAZEGENERATOR_HPP