#ifndef YIXIAOMAZESOLVER_HPP
#define YIXIAOMAZESOLVER_HPP

#include "MazeSolver.hpp"
#include "MazeSolution.hpp"
#include "Maze.hpp"
#include "Direction.hpp"
#include <vector>

class YixiaoMazeSolver : public MazeSolver
{
public:
    YixiaoMazeSolver();
    void solveMaze(const Maze& maze, MazeSolution& mazeSolution) override;
    void maze_solver(const Maze& maze, MazeSolution& mazeSolution, int x, int y);
    void direction_movement(int& x, int& y);
    void is_visited_initialization(const Maze& maze);
    Direction random_distribution_generator(int index);
    std::vector<Direction> adjacent_check(const Maze& maze, int x, int y);
    bool check_right(int x, int y, const Maze& maze);
    bool check_left(int x, int y, const Maze& maze);
    bool check_up(int x, int y, const Maze& maze);
    bool check_down(int x, int y, const Maze& maze);
    void delete_last_movement(MazeSolution& mazeSolution);
    bool is_visited_all_visited(const Maze& maze, int x, int y);
private:
    std::vector<Direction> direct;
    std::vector<std::vector<bool>> is_visited;
    Direction potential_direction;
    int temp_x = 0;
    int temp_y = 0;
};

#endif //YIXIAOMAZESOLVER_HPP