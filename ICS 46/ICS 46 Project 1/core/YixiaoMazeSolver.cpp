#include "YixiaoMazeSolver.hpp"
#include <ics46/factory/DynamicFactory.hpp>
#include <random>
#include <vector>
#include "Maze.hpp"
#include "Direction.hpp"
#include "MazeSolver.hpp"
using namespace std;
ICS46_DYNAMIC_FACTORY_REGISTER(MazeSolver, YixiaoMazeSolver, "Yixiao Zeng Solver (Required)");



YixiaoMazeSolver::YixiaoMazeSolver()
{

}

random_device device2;
default_random_engine engine2{device2()};


void YixiaoMazeSolver::solveMaze(const Maze& maze, MazeSolution& mazeSolution)
{
    mazeSolution.restart();
    is_visited_initialization(maze);
    potential_direction = Direction::down;
    is_visited[mazeSolution.getStartingCell().first][mazeSolution.getStartingCell().second] = true;
    maze_solver(maze, mazeSolution, mazeSolution.getStartingCell().first, mazeSolution.getStartingCell().second);
}

bool YixiaoMazeSolver::check_right(int x, int y, const Maze& maze)
{
    int new_step = x+1;
    return (maze.getWidth() > new_step && is_visited[new_step][y] == false && maze.wallExists(x, y, Direction::right) == false);
}
bool YixiaoMazeSolver::check_left(int x, int y, const Maze& maze)
{
    int new_step = x-1;
    return (new_step >= 0 && is_visited[new_step][y] == false && maze.wallExists(x, y, Direction::left) == false);
}
bool YixiaoMazeSolver::check_up(int x, int y, const Maze& maze)
{
    int new_step = y-1;
    return (new_step >= 0 && is_visited[x][new_step] == false && maze.wallExists(x, y, Direction::up) == false);
}
bool YixiaoMazeSolver::check_down(int x, int y, const Maze& maze)
{
    int new_step = y+1;
    return (maze.getHeight() > new_step && is_visited[x][new_step] == false && maze.wallExists(x, y, Direction::down) == false);
}

std::vector<Direction> YixiaoMazeSolver::adjacent_check(const Maze& maze, int x, int y)
{
    vector<Direction> direct_temp;
    if (check_right(x,y,maze))
        direct_temp.push_back(Direction::right);
    if (check_left(x,y,maze))
        direct_temp.push_back(Direction::left);
    if (check_up(x,y,maze))
        direct_temp.push_back(Direction::up);
    if (check_down(x,y,maze))
        direct_temp.push_back(Direction::down);
    return direct_temp;
}

Direction YixiaoMazeSolver::random_distribution_generator(int index)
{
    uniform_int_distribution<int> distribution{0,index};
    unsigned int random_number = distribution(engine2);
    return direct[random_number];
}

void YixiaoMazeSolver::direction_movement(int &x, int& y)
{
    switch (potential_direction)
    {
    case Direction::right:
        x++;
        break;
    case Direction::left:
        x--;
        break;
    case Direction::up:
        y--;
        break;
    case Direction::down:
        y++;
        break;
    default:
        break;
    }
}

void YixiaoMazeSolver::maze_solver(const Maze& maze, MazeSolution& mazeSolution, int x, int y)
{
    is_visited[x][y] = true;
    direct = adjacent_check(maze, x, y);
    if(direct.size() > 0)
    {
        if(mazeSolution.isComplete() == false)
        {
            do
            {
                int len = direct.size();
                int index = len-1;
                temp_x = x;
                temp_y = y;
                potential_direction = random_distribution_generator(index);
                direction_movement(temp_x, temp_y);
                if (is_visited[temp_x][temp_y] == false)
                    if (maze.wallExists(x, y, potential_direction) == false)
                    {
                        mazeSolution.extend(potential_direction);
                        maze_solver(maze, mazeSolution, temp_x, temp_y);
                    }
                direct = adjacent_check(maze, x, y);
            } while (direct.size() > 0 && mazeSolution.isComplete() == false);
        }
        else if (mazeSolution.isComplete() == true)
            return;
    }
    delete_last_movement(mazeSolution);
}

void YixiaoMazeSolver::delete_last_movement(MazeSolution& mazeSolution)
{
    if (!mazeSolution.isComplete())
        mazeSolution.backUp();
}

void YixiaoMazeSolver::is_visited_initialization(const Maze& maze)
{
    is_visited.resize(maze.getHeight());
    for(int i=0;i<maze.getWidth();i++)
        is_visited[i].resize(maze.getWidth());
    for (int i=0;i<maze.getWidth();i++)
        for(int j=0;j<maze.getHeight();j++)
            is_visited[i][j] = false;
}

bool YixiaoMazeSolver::is_visited_all_visited(const Maze& maze, int x, int y)
{
    int temp_width = maze.getWidth();
    int temp_height = maze.getHeight();
    while (temp_width>0)
    {
        while (temp_height>0)
        {
            if(is_visited[temp_width][temp_height] == false)
                return false;
            temp_height--;
        }
        temp_width--;
    }
    return true;
}