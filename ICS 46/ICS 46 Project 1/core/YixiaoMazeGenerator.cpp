#include <ics46/factory/DynamicFactory.hpp>
#include "YixiaoMazeGenerator.hpp"
#include "Maze.hpp"
#include "MazeGenerator.hpp"
#include "Direction.hpp"
#include <random>
#include <vector>
using namespace std;

ICS46_DYNAMIC_FACTORY_REGISTER(MazeGenerator, YixiaoMazeGenerator, "Yixiao Zeng Generator (Required)");


random_device device;
default_random_engine engine{device()};

YixiaoMazeGenerator::YixiaoMazeGenerator()
{
    
}

Direction YixiaoMazeGenerator::random_distribution_generator(int index)
{
    uniform_int_distribution<int> distribution{0,index};
    unsigned int random_number = distribution(engine);
    return direct[random_number];
}

void YixiaoMazeGenerator::generateMaze(Maze& maze)
{
    maze.addAllWalls();
    is_visited_initialization(maze);
    is_visited[0][0] = true;
    potential_direction = Direction::down;
    generate_maze(maze, temp_x, temp_y);
}

void YixiaoMazeGenerator::generate_maze(Maze& maze, int x, int y)
{
    is_visited[x][y] = true;
    direct = adjacent_check(maze, x, y);
    while(true)
    {
        int len = direct.size();
        int index = len -1;
        if (len <= 0)
            break;
        else
        {
            temp_x = x;
            temp_y = y;
            potential_direction = random_distribution_generator(index);
            direction_movement(temp_x, temp_y);
            if (is_visited[temp_x][temp_y] == false)
                if(maze.wallExists(x, y, potential_direction) == true)
                {
                    maze.removeWall(x, y, potential_direction);
                    generate_maze(maze, temp_x, temp_y);
                }
            direct = adjacent_check(maze, x, y);
        }
    }
}

std::vector<Direction> YixiaoMazeGenerator::adjacent_check(Maze& maze, int x, int y)
{
    vector<Direction> direct_temp;
    if (check_right(x,y,maze))
        direct_temp.push_back(Direction::right);
    if (check_left(x,y,maze))
        direct_temp.push_back(Direction::left);
    if (check_up(x,y,maze))
        direct_temp.push_back(Direction::up);
    if (check_down(x,y,maze))
        direct_temp.push_back(Direction::down);
    return direct_temp;
}

bool YixiaoMazeGenerator::check_right(int x, int y, Maze& maze)
{
    int new_step = x+1;
    return (maze.getWidth() > new_step && is_visited[new_step][y] == false);
}
bool YixiaoMazeGenerator::check_left(int x, int y, Maze& maze)
{
    int new_step = x-1;
    return (new_step >= 0 && is_visited[new_step][y] == false);
}
bool YixiaoMazeGenerator::check_up(int x, int y, Maze& maze)
{
    int new_step = y-1;
    return (new_step >= 0 && is_visited[x][new_step] == false);
}
bool YixiaoMazeGenerator::check_down(int x, int y, Maze& maze)
{
    int new_step = y+1;
    return (maze.getHeight() > new_step && is_visited[x][new_step] == false);
}

bool YixiaoMazeGenerator::is_visited_all_visited(Maze& maze)
{
    int temp_width = maze.getWidth();
    int temp_height = maze.getHeight();
    while (temp_width>0)
    {
        while (temp_height>0)
        {
            if(is_visited[temp_width][temp_height] == false)
                return false;
            temp_height--;
        }
        temp_width--;
    }
    return true;
}

void YixiaoMazeGenerator::direction_movement(int& x, int& y)
{
    switch (potential_direction)
    {
    case Direction::right:
        x++;
        break;
    case Direction::left:
        x--;
        break;
    case Direction::up:
        y--;
        break;
    case Direction::down:
        y++;
        break;
    default:
        break;
    }
}

void YixiaoMazeGenerator::is_visited_initialization(Maze& maze)
{
    is_visited.resize(maze.getHeight());
    for(int i=0;i<maze.getWidth();i++)
        is_visited[i].resize(maze.getWidth());
    for (int i=0;i<maze.getWidth();i++)
        for(int j=0;j<maze.getHeight();j++)
            is_visited[i][j] = false;
}

