// String.cpp
//
// ICS 46 Winter 2022
// Project #0: Getting to Know the ICS 46 VM
//
// Implement all of your String member functions in this file.
//
// Note that the entire standard library -- both the C Standard
// Library and the C++ Standard Library -- is off-limits for this
// task, as the goal is to exercise your low-level implementation
// skills (pointers, memory management, and so on).

#include "String.hpp"
#include "OutOfBoundsException.hpp"

unsigned int String::length() const noexcept
{
    return str_size;
}

String::String()
{
    ptr = new char[1];
    ptr[0] = '\0';
    str_size = 0;
}

String::String(const char* chars)
{
    if(chars == nullptr)
    {
        ptr = new char[1];
        ptr[0] = '\0';
        str_size = 0;
    }
    else
    {
        str_size = 0;
        while(chars[str_size] != '\0')
            str_size++;
        ptr = new char[str_size+1];
        ptr[str_size] = '\0';
        for(unsigned int i=0;i<str_size;i++)
            ptr[i] = chars[i];
    }
}

String::~String() noexcept
{
    delete[] ptr;
}

char String::operator[](int num) const
{
    if(num<=str_size)
        return ptr[num];
    else
        return ptr[str_size-1];
}

String& String::operator=(const String &s)
{
    delete[] ptr;
    str_size = s.length();
    ptr = new char [str_size+1];
    ptr[str_size] = '\0';
    for(int i=0;i<str_size;i++)
        ptr[i] = s[i];
    return *this;
}

String::String(const String& s)
{
    str_size = s.length();
    ptr = new char[str_size+1];
    ptr[str_size] = '\0';
    for(int i=0;i<str_size;i++)
        ptr[i] = s[i];
}

void String::append(const String &s)
{
    char* tmp = new char[str_size+1];
    tmp[str_size] = '\0';
    int i=0;
    for(;i<str_size;i++)
        tmp[i] = ptr[i];
    delete[] ptr;
    ptr = new char[1];
    ptr[0] = '\0';
    for(int j=0;j<s.length();i++,j++)
        tmp[i] = s[j];
    tmp[str_size + s.length()] = '\0';
    for(int k=0;k<str_size + s.length();k++)
        ptr[k] = tmp[k];
    str_size += s.length();
    delete[] tmp;
}

char &String::at(unsigned int index)
{
    if(index >= str_size)
        throw OutOfBoundsException{};
    else
        return ptr[index];
}

char String::at(unsigned int index) const
{
    if(index >= str_size)
        throw OutOfBoundsException{};
    else
        return ptr[index];
}

void String::clear()
{
    delete[] ptr;
    ptr = new char[1];
    ptr[0] = '\0';
    str_size = 0;
}

int String::compareTo(const String &s) const noexcept
{
    int len = str_size;
    if(str_size > s.str_size)
        len = s.str_size;
    int i=0;
    while(i<len)
    {
        if(ptr[i] > s[i])
            return 1;
        if(ptr[i] < s[i])
            return -1;
        i++;
    }
    return 0;
}

String String::concatenate(const String &s) const
{
    char* tmp = new char[str_size+1];
    tmp[str_size] = '\0';
    int i=0;
    for(;i<str_size;i++)
        tmp[i] = ptr[i];
    for(int j=0;j<s.length();i++,j++)
        tmp[i] = s[j];
    tmp[str_size + s.length()] = '\0';
    return String(tmp);
}

bool String::contains(const String &substring) const noexcept
{
    String s = ptr;
    if (s.find(substring) == -1)
        return false;
    else
        return true;
}
bool String::equals(const String &s) const noexcept
{
    if(str_size != s.str_size)
        return false;
    else if(str_size == s.str_size)
    {
        int i=0;
        while(i<str_size)
        {
            if(ptr[i] != s[i])
                return false;
            i++;
        }
        return true;
    }
    return false;
}

int String::find(const String &substring) const noexcept
{
    String s = ptr;
    for (int i=0;i<str_size;i++)
    {
        if((i+substring.str_size)<str_size)
        {
            String temp;
            temp = s.substring(i, i + substring.str_size);
            if (substring.compareTo(temp) == 0)
                return i;
        }
    }
    return -1;
}

bool String::isEmpty() const noexcept
{
    if(ptr[0] == '\0')
        return true;
    else
        return false;

}

String String::substring(unsigned int startIndex, unsigned int endIndex) const
{
    if(startIndex > str_size || endIndex > str_size)
        throw OutOfBoundsException{};
    else
    {
        char* result = new char[endIndex-startIndex+1];
        result[endIndex-startIndex] = '\0';
        int j=0;
        for(unsigned int i = startIndex;i<endIndex;i++,j++)
            result[j] = ptr[i];
        result[endIndex-startIndex] = '\0';
        return String(result);
    }
}

const char *String::toChars() const noexcept
{
    char *result = new char [str_size+1];
    result[str_size] = '\0';
    for(int i=0;i<str_size;i++)
        result[i] = ptr[i];
    return result;
}