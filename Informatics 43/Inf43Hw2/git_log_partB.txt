commit abe76ee1d3348d03b172a4c634fe1d90a6cae65f
Author: Jack Zeng <yixiaozeng0208@outlook.com>
Date:   Sun May 1 21:43:57 2022 -0700

    Re-adding file2.txt

diff --git a/file2.txt b/file2.txt
new file mode 100644
index 0000000..e69de29

commit d441551ccf0d855c9d1832e2c851b5899f769dcf
Author: Jack Zeng <yixiaozeng0208@outlook.com>
Date:   Sun May 1 21:41:46 2022 -0700

    Deleting file2.txt

diff --git a/file1.txt b/file1.txt
index 541cb2f..8ed08ad 100644
--- a/file1.txt
+++ b/file1.txt
@@ -1 +1,2 @@
-Yixiao Zeng Computer Science Taco Bell The Godfather
\ No newline at end of file
+Yixiao Zeng Computer Science Taco Bell The Godfather
+Blue
\ No newline at end of file
diff --git a/file2.txt b/file2.txt
deleted file mode 100644
index e69de29..0000000

commit 6b912e9dbb04db954d274ddeb2916976193fdb7d
Author: Jack Zeng <yixiaozeng0208@outlook.com>
Date:   Sun May 1 21:39:59 2022 -0700

    Committing Homework 2 file2

diff --git a/file2.txt b/file2.txt
new file mode 100644
index 0000000..e69de29

commit 9e9cfad4b1d1eeb8e670c3dc0116bd23a5731f3b
Author: Jack Zeng <yixiaozeng0208@outlook.com>
Date:   Sun May 1 21:22:00 2022 -0700

    Added favorite restaurant and movie

diff --git a/file1.txt b/file1.txt
index 7614a44..541cb2f 100644
--- a/file1.txt
+++ b/file1.txt
@@ -1 +1 @@
-Yixiao Zeng Computer Science
\ No newline at end of file
+Yixiao Zeng Computer Science Taco Bell The Godfather
\ No newline at end of file

commit 18a942c82db541b2d543cbf8fd6e2ab450e7da75
Author: Jack Zeng <yixiaozeng0208@outlook.com>
Date:   Sun May 1 21:20:00 2022 -0700

    Now has my major

diff --git a/file1.txt b/file1.txt
index bdb234b..7614a44 100644
--- a/file1.txt
+++ b/file1.txt
@@ -1 +1 @@
-Yixiao Zeng 84444007
\ No newline at end of file
+Yixiao Zeng Computer Science
\ No newline at end of file

commit e03cb852f2be6ccfe2da1953b325dcd0f1c46ace
Author: Jack Zeng <yixiaozeng0208@outlook.com>
Date:   Sun May 1 21:18:26 2022 -0700

    Committing a new file with my name

diff --git a/file1.txt b/file1.txt
new file mode 100644
index 0000000..bdb234b
--- /dev/null
+++ b/file1.txt
@@ -0,0 +1 @@
+Yixiao Zeng 84444007
\ No newline at end of file
