import math
class Paths(object):
    def eval(self, list2):
        n = int(list2.strip())
        ans = math.comb(2*n, n) // (n + 1)
        return ans%2022

if __name__ == '__main__':
    calc = Paths()
    print(calc.eval("4"))
