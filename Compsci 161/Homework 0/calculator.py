import re
class Calculator(object):
    def eval(self, str_of_ints):
        return max(list(map(int, str_of_ints.split())))

if __name__ == '__main__':
    calc = Calculator()
    print(calc.eval('1 2 3 4 5 6 7 8 9 10'))
