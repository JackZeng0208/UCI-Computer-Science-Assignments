import re
import math

class Zapping(object):
    def eval(self, list2):
        tokens2 = list2.split()
        list3 = list(map(int, tokens2))
        num_channels = list3[0]
        tv_channels = list3[1:]
        min_changes = float('inf')
        def has_number_greater_than_half_sum(arr, total_sum):
            half_sum = math.ceil(total_sum/2)
            for num in arr:
                if num > half_sum:
                    return True
            return False
        for i in range (1, num_channels+1):
            dis_target_channel = []
            for j in tv_channels:
                dis_target_channel.append((i-j)%num_channels)
            # print(dis_target_channel)
            temp_s = sum(dis_target_channel)
            if not has_number_greater_than_half_sum(dis_target_channel, temp_s) and temp_s < min_changes:
                min_changes = temp_s
        return min_changes
                

if __name__ == '__main__':
    calc = Zapping()
    print(calc.eval("19204 3047 13933 15743 5518 17526 12672 14915 10143 15889 15068 14322 13687 3109 8402 15550 12914 7742 14721 16026 2178"))