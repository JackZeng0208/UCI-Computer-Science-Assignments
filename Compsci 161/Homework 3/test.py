visited = [] # List for visited nodes.
queue = []     #Initialize a queue

def bfs(visited, graph, node): #function for BFS
  visited.append(node)
  queue.append(node)

  while queue:          # Creating loop to visit each node
    m = queue.pop(0) 
    print (m, end = " ") 

    for neighbour in sorted(graph[m]):
      if neighbour not in visited:
        visited.append(neighbour)
        queue.append(neighbour)

# example usage
graph = {
    'A': ['B', 'D'],
    'B': ['C', 'G'],
    'C': ['A'],
    'D': ['C'],
    'E': ['G','F'],
    'F': [],
    'G': ['F']
}
DAG = {
    'A':['L', 'R', 'H'],
    'L':['R', 'G', 'M', 'S'],
    'G':['T', 'O', 'I'],
    'O':['S']
}

print("BFS Traversal:")
bfs(visited, graph, 'A')  # Starting from vertex 'A'

