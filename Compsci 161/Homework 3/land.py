class Land(object):
    def eval(self, listone):
        tokensone = listone.split()
        listar = list(map(int, tokensone))
        n = listar[0]
        grid = [[listar[j+(i-1)*n] for j in range(1, n+1)]for i in range(1, n+1)]
        visited = [[False for _ in range(n)] for _ in range(n)]
        if not grid:
            return 0
        def dfs(i, j):
            if i < 0 or i >= n or j < 0 or j >= n or grid[i][j] == 0 or visited[i][j]:
                return
            visited[i][j] = True
            dfs(i + 1, j)
            dfs(i - 1, j)
            dfs(i, j + 1)
            dfs(i, j - 1)
        count = 0
        for i in range(n):
            for j in range(n):
                if not visited[i][j] and grid[i][j] == 1:
                    dfs(i, j)
                    count += 1
        return count
        
if __name__ == '__main__':
    calc = Land()
    print(calc.eval('6 0 1 1 0 0 1 0 1 0 1 1 0 0 1 0 1 1 0 0 0 1 0 1 0 1 1 1 0 1 1 0 0 1 0 1 1'))
