package labs.lab2;

import java.util.Scanner;

public class Main {

    /**
     * Reads a number between 1,000 and 999,999 from the user and prints it with a
     * comma separating the thousands.
     *
     * @param in the Scanner to be used for user input
     *           <p>
     *           To run this method using the keyboard for user input, call it like
     *           this: problem2_printWithCommas(new Scanner(System.in));
     */
    public static void problem2_printWithCommas(Scanner in) {
        int num = in.nextInt();
        System.out.printf("Please enter an integer between 1000 and 999999: %s", String.format("%,d", num));
    }


    /**
     * Computes the total of a sticker order based on the price per sticker and number
     * of stickers, which are prompted for from the user
     *
     * @param in the Scanner to be used for user input
     *           <p>
     *           To run this method using the keyboard for user input, call it like
     *           this: problem3_calculateTotal(new Scanner(System.in));
     */
    public static void problem3_calculateTotal(Scanner in) {
        System.out.print("Enter price per sticker: ");
        double price_per_sticker = in.nextDouble();
        System.out.print("Enter the number of stickers: ");
        double sticker_num = in.nextInt();
        double result = (price_per_sticker * sticker_num) + ((price_per_sticker * sticker_num) * 0.1) + 0.15 * sticker_num;
        System.out.printf("Your total is: $%.2f", result);

    }


    /**
     * Computes the total of a sticker order based on the price per sticker and number
     * of stickers, which are prompted for from the user
     *
     * @param in the Scanner to be used for user input
     *           <p>
     *           To run this method using the keyboard for user input, call it like
     *           this: problemr_compoundInterest(new Scanner(System.in));
     */
    public static void problem4_compoundInterest(Scanner in) {
        System.out.print("Enter principal amount: ");
        double P = in.nextDouble();
        System.out.print("Enter the annual rate of interest: ");
        double r = in.nextDouble();
        System.out.print("Enter the number of years the amount is invested: ");
        int t = in.nextInt();
        System.out.print("Enter the number of times the interest is compounded per year: ");
        int n = in.nextInt();
        double result = P * Math.pow(1 + ((r * 0.01) / n), n * t);
        System.out.printf("$%.2f", P);
        System.out.print(" invested at " + r + "%" + " for " + t + " years compounded " + n + " times annually is ");
        System.out.printf("$%.2f.", result);
    }


    /**
     * Computes the number of months it will take to pay off a credit card balance, based
     * on the balance, APR, and monthly payment which are prompted for from the user
     *
     * @param in the Scanner to be used for user input
     *           <p>
     *           To run this method using the keyboard for user input, call it like
     *           this: problem5_creditCardPayoff(new Scanner(System.in));
     */
    public static void problem5_creditCardPayoff(Scanner in) {
        System.out.print("What is your balance? ");
        double b = in.nextDouble();
        System.out.print("What is the APR on the card? ");
        double i = in.nextDouble();
        double i_percent = (i / 100) / 365;
        System.out.print("What is the monthly payment you can make? ");
        double p = in.nextDouble();
        double result = (-1 / 30.0) * (Math.log(1 + (b / p) * (1 - Math.pow((1 + i_percent), 30))) / Math.log(1 + i_percent));
        System.out.printf("It will take you %d months to pay off this card.", (int) Math.ceil(result));
    }


    /**
     * Walk the user through troubleshooting issues with a car.
     *
     * @param in the Scanner to be used for user input
     *           <p>
     *           To run this method using the keyboard for user input, call it like
     *           this: problem6_troubleshootCarIssues(new Scanner(System.in));
     */
    public static void problem6_troubleshootCarIssues(Scanner in) {
        String s = "";
        System.out.print("Is the car silent when you turn the key? ");
        s = in.nextLine();
        if (s.toLowerCase().charAt(0) == 'y') {
            System.out.print("Are the battery terminals corroded? ");
            s = in.nextLine();
            if (s.toLowerCase().charAt(0) == 'y') {
                System.out.print("Clean terminals and try starting again.");
            } else if (s.toLowerCase().charAt(0) == 'n') {
                System.out.print("Replace cables and try again.");
            } else {
                System.out.print("Invalid input. Exiting.");
                return;
            }
        } else if (s.toLowerCase().charAt(0) == 'n') {
            System.out.print("Does the car make a clicking noise? ");
            s = in.nextLine();
            if (s.toLowerCase().charAt(0) == 'y') {
                System.out.print("Replace the battery.");
            } else {
                System.out.print("Does the car crank up but fail to start? ");
                s = in.nextLine();
                if (s.toLowerCase().charAt(0) == 'y') {
                    System.out.print("Check spark plug connections.");
                } else if (s.toLowerCase().charAt(0) == 'n') {
                    System.out.print("Does the engine start and then die? ");
                    s = in.nextLine();
                    if (s.toLowerCase().charAt(0) == 'y') {
                        System.out.print("Does your car have fuel injection? ");
                        s = in.nextLine();
                        if (s.toLowerCase().charAt(0) == 'y') {
                            System.out.print("Get it in for service.");
                        } else {
                            System.out.print("Check to ensure the choke is opening and closing.");
                        }
                    } else if (s.toLowerCase().charAt(0) == 'n') {
                        System.out.print("Get it in for service.");
                    } else {
                        System.out.print("Invalid input. Exiting.");
                        return;
                    }
                } else {
                    System.out.print("Invalid input. Exiting.");
                    return;
                }
            }
        } else {
            System.out.print("Invalid input. Exiting.");
            return;
        }
    }


    /**
     * Assesses the strength of a password based on these rules:
     * <p>
     * * A very weak password contains only digits and is fewer than eight characters
     * * A weak password contains only letters and is fewer than eight characters
     * * A strong password contains at least one letter and at least one digit and is at least
     * eight characters
     * * A very strong password contains at least one letter, at least one digit, and at least
     * one special character (non letter or digit) and is at least eight characters
     * * All other passwords are medium strength
     *
     * @param password the password to assess
     * @return a string describing its strength
     */
    public static String problem7_assessPasswordStrength(String password) {
        int l = password.length();
        boolean d = false;
        boolean alpha = false;
        boolean s = false;
        for (int i = 0; i < l; i++) {
            if (password.charAt(i) >= '0' && password.charAt(i) <= '9')
                d = true;
            else if ((password.charAt(i) >= 'a' && password.charAt(i) <= 'z') || (password.charAt(i) >= 'A' && password.charAt(i) <= 'Z'))
                alpha = true;
            else
                s = true;
        }
        if (d && !alpha && l < 8 && !s)
            return "very weak";
        else if (!d && alpha && l < 8 && !s)
            return "weak";
        else if (d && alpha && l >= 8 && !s)
            return "strong";
        else if (d && alpha && l >=8 && s)
            return "very strong";
        else
            return "medium";
    }


    /**
     * Translates a letter grade into a numeric grade
     *
     * @param letterGrade the letter grade to translate
     * @return the numeric grade
     */
    public static double problem8_getNumericGrade(String letterGrade) {
        int l = letterGrade.length();
        if (l == 2)
        {

            if (letterGrade.equals("A+") || letterGrade.equals("a+"))
                return 4.0;
            else if (letterGrade.equals("A-") || letterGrade.equals("a-"))
                return 3.7;
            else if (letterGrade.equals("B+") || letterGrade.equals("b+"))
                return 3.3;
            else if (letterGrade.equals("B-") || letterGrade.equals("b-"))
                return 2.7;
            else if (letterGrade.equals("C+") || letterGrade.equals("c+"))
                return 2.3;
            else if (letterGrade.equals("C-") || letterGrade.equals("c-"))
                return 1.7;
            else if (letterGrade.equals("D+") || letterGrade.equals("d+"))
                return 1.3;
            else if (letterGrade.equals("D-") || letterGrade.equals("d-"))
                return 0.7;
            else
                return -1.0;
        } else if (l == 1)
        {
            if (letterGrade.equals("A") || letterGrade.equals("a"))
                return 4.0;
            else if (letterGrade.equals("B") || letterGrade.equals("b"))
                return 3.0;
            else if (letterGrade.equals("C") || letterGrade.equals("c"))
                return 2.0;
            else if (letterGrade.equals("D") || letterGrade.equals("d"))
                return 1.0;
            else if (letterGrade.equals("F") || letterGrade.equals("f"))
                return 0.0;
            else
                return -1.0;
        }
        else
            return -1.0;
    }


    /**
     * Translates a numeric grade into a letter grade
     *
     * @param numericGrade the numeric grade to translate
     * @return the letter grade
     */
    public static String problem8_getLetterGrade(double numericGrade) {
        if (numericGrade >= 0 && numericGrade <= 4)
        {
            if (numericGrade == 4.0)
                return "A+";
            else if (numericGrade >= 3.85)
                return "A";
            else if (numericGrade >= 3.5)
                return "A-";
            else if (numericGrade >= 3.15)
                return "B+";
            else if (numericGrade >= 2.85)
                return "B";
            else if (numericGrade >= 2.5)
                return "B-";
            else if (numericGrade >= 2.15)
                return "C+";
            else if (numericGrade >= 1.85)
                return "C";
            else if (numericGrade >= 1.5)
                return "C-";
            else if (numericGrade >= 1.15)
                return "D+";
            else if (numericGrade >= 0.85)
                return "D";
            else if (numericGrade >= 0.5)
                return "D-";
            else
                return "F";
        }
        else
            return "Error";
    }


    /**
     * If one or both of the first 2 chars in the given string is the char 'x'
     * (lower case only), returns the string without those 'x' chars. Otherwise,
     * returns the string unchanged.
     *
     * @param str the string to change
     * @return the changed string
     */
    public static String problem10_withoutX2(String str) {
        String result = "";
        if (str.length() != 0 && (str.charAt(0) == 'x' || str.charAt(1) == 'x'))
        {
            for (int i = 0; i < str.length(); i++)
                if (str.charAt(i) == 'x' && i <= 1)
                    continue;
                else
                    result += str.charAt(i);
            return result;
        }
        return str;
    }
}
