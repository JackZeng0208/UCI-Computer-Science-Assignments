package labs.lab3;

import static org.junit.Assert.*;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import java.util.Arrays;

public class Lab3Test {

	/* 2 points */
	@Test
	public void problem1_1() {
		assertEquals("z1z", Main.problem1_mirrorEnds("z1z"));
		assertEquals("askksa", Main.problem1_mirrorEnds("askksa"));
	}

	/* 2 points */
	@Test
	public void problem1_2() {
		assertEquals("89", Main.problem1_mirrorEnds("89and 98"));
		assertEquals("89", Main.problem1_mirrorEnds("89 and98"));
	}


	/* 2 points */
	@Test
	public void problem1_3() {
		assertEquals("", Main.problem1_mirrorEnds("c11"));
		assertEquals("", Main.problem1_mirrorEnds("Cba abc"));
	}

	/* 2 points */
	@Test
	public void problem1_4() {
		assertEquals("er", Main.problem1_mirrorEnds("erkore"));
		assertEquals("CA", Main.problem1_mirrorEnds("CAcaACAC"));
	}

	/* 2 points */
	@Test
	public void problem1_5() {
		assertEquals("Jj 8 8 jJ", Main.problem1_mirrorEnds("Jj 8 8 jJ"));
		assertEquals("and ", Main.problem1_mirrorEnds("and 23 dna"));
	}

	/* 2 points */
	@Test
	public void problem2_1() {
		assertEquals(2, Main.problem2_maxBlock("ppllaa"));
		assertEquals(2, Main.problem2_maxBlock("ppla"));
	}

	/* 2 points */
	@Test
	public void problem2_2() {
		assertEquals(2, Main.problem2_maxBlock("hoopla"));
		assertEquals(2, Main.problem2_maxBlock("hoopla"));

	}

	/* 2 points */
	@Test
	public void problem2_3() {
		assertEquals(2, Main.problem2_maxBlock("h hh"));
		assertEquals(3, Main.problem2_maxBlock("qqq ll"));
	}

	/* 2 points */
	@Test
	public void problem2_4() {
		assertEquals(4, Main.problem2_maxBlock("2232332222"));
		assertEquals(4, Main.problem2_maxBlock("2333mmmm"));
	}

	/* 2 points */
	@Test
	public void problem2_5() {
		assertEquals(1, Main.problem2_maxBlock("3ji1os2"));
		assertEquals(2, Main.problem2_maxBlock("  3987"));
	}


	/* 2 points */
	@Test
	public void problem3_1() {
		assertTrue(Main.problem3_EHappy("xyEE"));
		assertFalse(Main.problem3_EHappy("xyeE"));

	}

	/* 2 points */
	@Test
	public void problem3_2() {
		assertTrue(Main.problem3_EHappy("  eeEE"));
		assertFalse(Main.problem3_EHappy("123eeE"));

	}

	/* 2 points */
	@Test
	public void problem3_3() {
		assertFalse(Main.problem3_EHappy("32EExEx"));
		assertFalse(Main.problem3_EHappy("33E"));

	}

	/* 2 points */
	@Test
	public void problem3_4() {
		assertTrue(Main.problem3_EHappy("xxEExEEx"));
		assertFalse(Main.problem3_EHappy("1Ee1"));

	}

	/* 2 points */
	@Test
	public void problem3_5() {
		assertTrue(Main.problem3_EHappy("  "));
		assertTrue(Main.problem3_EHappy("x2"));

	}


	/* 2 points */
	@Test
	public void problem4_1() {
		assertEquals(8, Main.problem4_getNumTwists(1629, 3614));
		assertEquals(9, Main.problem4_getNumTwists(1329, 5038));
	}

	/* 2 points */
	@Test
	public void problem4_2() {
		assertEquals(9, Main.problem4_getNumTwists(0111, 9888));
		assertEquals(16, Main.problem4_getNumTwists(3333, 9999));

	}

	/* 2 points */
	@Test
	public void problem4_3() {
		assertEquals(0, Main.problem4_getNumTwists(4590, 4590));
		assertEquals(0, Main.problem4_getNumTwists(3300, 3300));
	}

	/* 2 points */
	@Test
	public void problem4_4() {
		assertEquals(4, Main.problem4_getNumTwists(4712, 4729));
		assertEquals(7, Main.problem4_getNumTwists(1212, 3228));
	}

	/* 2 points */
	@Test
	public void problem4_5() {
		assertEquals(1, Main.problem4_getNumTwists(1111, 1101));
		assertEquals(1, Main.problem4_getNumTwists(1119, 1110));
	}

	/* 2 points */
	@Test
	public void problem5_1() {
		// Test case 1
		String input = "20\n5\n3\n2\n";

		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		Main.problem5_officeCrowdControl(new Scanner(System.in), 30);

		String result = output.toString();
		assertEquals("People entering or leaving: "
				+ "People in office: 20 | People entering or leaving: "
				+ "People in office: 25 | People entering or leaving: "
				+ "People in office: 28 | People entering or leaving: "
				+ "People in office: 30 | Office is full", result);

	}

	/* 2 points */
	@Test
	public void problem5_2() {
		// Test case 1
		String input = "10\n-5\n-3\n4\n14\n";

		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		Main.problem5_officeCrowdControl(new Scanner(System.in), 20);

		String result = output.toString();
		assertEquals("People entering or leaving: "
				+ "People in office: 10 | People entering or leaving: "
				+ "People in office: 5 | People entering or leaving: "
				+ "People in office: 2 | People entering or leaving: "
				+ "People in office: 6 | People entering or leaving: "
				+ "People in office: 20 | Office is full", result);

	}

	/* 2 points */
	@Test
	public void problem5_3() {
		// Test case 1
		String input = "5\n-5\n-2\n-7\n10\n0\n4\n6\n";

		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		Main.problem5_officeCrowdControl(new Scanner(System.in), 20);

		String result = output.toString();
		assertEquals("People entering or leaving: "
						+ "People in office: 5 | People entering or leaving: "
						+ "People in office: 0 | People entering or leaving: "
						+ "People in office: 0 | People entering or leaving: "
						+ "People in office: 0 | People entering or leaving: "
						+ "People in office: 10 | People entering or leaving: "
						+ "People in office: 10 | People entering or leaving: "
						+ "People in office: 14 | People entering or leaving: "
						+ "People in office: 20 | Office is full",
				result);

	}

	/* 2 points */
	@Test
	public void problem5_4() {
		// Test case 1
		String input = "3000\n10\n2000\n-1000\n-2000\n10\n2990\n";

		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		Main.problem5_officeCrowdControl(new Scanner(System.in), 3000);

		String result = output.toString();
		assertEquals("People entering or leaving: "
				+ "People in office: 3000 | Office is full", result);

	}

	/* 2 points */
	@Test
	public void problem5_5() {
		// Test case 1
		String input = "3\n2\n2\n5\n-1\n-2\n15\n-2\n8\n-8\n16\n14\n-1\n14\n";

		InputStream in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		Main.problem5_officeCrowdControl(new Scanner(System.in), 20);

		String result = output.toString();
		assertEquals("People entering or leaving: " +
						"People in office: 3 | People entering or leaving: "
						+ "People in office: 5 | People entering or leaving: "
						+ "People in office: 7 | People entering or leaving: "
						+ "People in office: 12 | People entering or leaving: "
						+ "People in office: 11 | People entering or leaving: "
						+ "People in office: 9 | People entering or leaving: "
						+ "People in office: 9 | People entering or leaving: "
						+ "People in office: 7 | People entering or leaving: "
						+ "People in office: 15 | People entering or leaving: "
						+ "People in office: 7 | People entering or leaving: "
						+ "People in office: 7 | People entering or leaving: "
						+ "People in office: 7 | People entering or leaving: "
						+ "People in office: 6 | People entering or leaving: "
						+ "People in office: 20 | Office is full",
				result);

	}


	/* 2 points */
	@Test
	public void problem6_1() {
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{1, 0, 0, 1, 1})));
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{3, 0, 3, 2})));
	}


	/* 2 points */
	@Test
	public void problem6_2() {
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{6, 6, 8})));
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{3, 3, 7})));
	}


	/* 2 points */
	@Test
	public void problem6_3() {
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{1, 1, 1, 1, 0})));
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{90, 10})));
	}


	/* 2 points */
	@Test
	public void problem6_4() {
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{11, 12})));
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{212, 1, 0})));
	}


	/* 2 points */
	@Test
	public void problem6_5() {
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{})));
		assertTrue(evensFirst(Main.problem6_evenOdd(new int[]{0})));
	}

	// returns true if all the evens come before all the odds in an array of ints
	private boolean evensFirst(int[] nums) {
		// if an even comes after an odd, return false:
		for (int i = 1; i < nums.length; i++) {
			if ((nums[i - 1] % 2 == 1) && (nums[i] % 2 == 0)) {
				return false;
			}
		}
		return true;
	}


	/* 2 points */
	@Test
	public void problem7_1() {
		assertArrayEquals(new int[]{3, 2}, Main.problem7_after4(new int[]{3, 4, 3, 2}));
		assertArrayEquals(new int[]{5}, Main.problem7_after4(new int[]{2, 1, 4, 5}));
	}

	/* 2 points */
	@Test
	public void problem7_2() {
		assertArrayEquals(new int[]{0, 0, 0}, Main.problem7_after4(new int[]{4, 4, 0, 0, 0}));
		assertArrayEquals(new int[]{}, Main.problem7_after4(new int[]{1, 0, 4}));
	}

	/* 2 points */
	@Test
	public void problem7_3() {
		assertArrayEquals(new int[]{2}, Main.problem7_after4(new int[]{0, 4, 2}));
		assertArrayEquals(new int[]{3}, Main.problem7_after4(new int[]{0, 4, 3}));
	}

	/* 2 points */
	@Test
	public void problem7_4() {
		assertArrayEquals(new int[]{}, Main.problem7_after4(new int[]{4, 4, 4}));
		assertArrayEquals(new int[]{}, Main.problem7_after4(new int[]{4}));
	}

	/* 2 points */
	@Test
	public void problem7_5() {
		assertArrayEquals(new int[]{2, 6, 2}, Main.problem7_after4(new int[]{4, 4, 4, 2, 6, 2}));
		assertArrayEquals(new int[]{6, 3, 2}, Main.problem7_after4(new int[]{4, 6, 3, 2}));
	}


	/* 2 points */
	@Test
	public void test_problem8_1() {
		// Test case 1:
		int[][] nums1 = {
				{2, 7, 6},
				{9, 5, 1},
				{4, 3, 8}

		};
		Square s1 = new Square(nums1);
		assertEquals(15, s1.rowSum(0));
		assertEquals(15, s1.rowSum(2));
		assertEquals(15, s1.columnSum(1));
		assertEquals(15, s1.columnSum(2));
		assertEquals(15, s1.diagonalSum(true));
		assertTrue(s1.isMagic());
	}

	/* 4 points */
	@Test
	public void test_problem8_2() {
		int[][] nums1 = {
				{6, 3, 10, 15},
				{9, 16, 5, 4},
				{7, 2, 11, 14},
				{12, 13, 8, 1}
		};
		Square s1 = new Square(nums1);
		assertEquals(34, s1.rowSum(1));
		assertEquals(34, s1.rowSum(2));
		assertEquals(34, s1.columnSum(1));
		assertEquals(34, s1.columnSum(2));
		assertEquals(34, s1.diagonalSum(true));
		assertTrue(s1.isMagic());
	}

	/* 4 points */
	@Test
	public void test_problem8_3() {
		int[][] nums1 = {
				{6, 3, 20},
				{15, 0, 1},
				{6, 2, 17}
		};
		Square s1 = new Square(nums1);
		assertEquals(16, s1.rowSum(1));
		assertEquals(25, s1.rowSum(2));
		assertEquals(27, s1.columnSum(0));
		assertEquals(5, s1.columnSum(1));
		assertEquals(26, s1.diagonalSum(false));
		assertFalse(s1.isMagic());
	}

	/* 3 points */
	@Test
	public void problem9_1() {
		SeatingChart chart = new SeatingChart();
		String seating = "40 50 50 50 40" +System.lineSeparator()  +
				"30 40 40 40 30" +System.lineSeparator() +
				"20 30 30 30 20" +System.lineSeparator() +
				"10 20 20 20 10" +System.lineSeparator() +
				"10 10 10 10 10" +System.lineSeparator();
		assertEquals(seating, chart.getSeatingChart());

		chart.sellSeatByNumber('A', 3);
		chart.sellSeatByNumber('B', 4);
		chart.sellSeatByNumber('E', 2);
		chart.sellSeatByPrice(50);
		chart.sellSeatByPrice(40);
		chart.sellSeatByPrice(30);
		chart.getSeatingChart();
		seating = " 0  0  0 50 40" +System.lineSeparator() +
				" 0 40 40  0 30" +System.lineSeparator() +
				"20 30 30 30 20" +System.lineSeparator() +
				"10 20 20 20 10" +System.lineSeparator() +
				"10  0 10 10 10" +System.lineSeparator();
		assertEquals(seating, chart.getSeatingChart());
	}

	/* 3 points */
	@Test
	public void problem9_2() {
		SeatingChart chart = new SeatingChart();
		String seating = "40 50 50 50 40" +System.lineSeparator() +
				"30 40 40 40 30" +System.lineSeparator() +
				"20 30 30 30 20" +System.lineSeparator() +
				"10 20 20 20 10" +System.lineSeparator() +
				"10 10 10 10 10" +System.lineSeparator();
		assertEquals(seating, chart.getSeatingChart());

		chart.sellSeatByNumber('A', 5);
		chart.sellSeatByNumber('A', 2);
		chart.sellSeatByNumber('C', 3);
		chart.sellSeatByPrice(50);
		chart.sellSeatByPrice(50);
		chart.sellSeatByPrice(30);
		chart.getSeatingChart();
		seating = "40  0  0  0  0" +System.lineSeparator() +
				" 0 40 40 40 30" +System.lineSeparator() +
				"20 30  0 30 20" +System.lineSeparator() +
				"10 20 20 20 10" +System.lineSeparator() +
				"10 10 10 10 10" +System.lineSeparator();
		assertEquals(seating, chart.getSeatingChart());
	}

	/* 3 points */
	@Test
	public void problem9_3() {
		SeatingChart chart = new SeatingChart();
		String seating = "40 50 50 50 40" +System.lineSeparator() +
				"30 40 40 40 30" +System.lineSeparator() +
				"20 30 30 30 20" +System.lineSeparator() +
				"10 20 20 20 10" +System.lineSeparator() +
				"10 10 10 10 10" +System.lineSeparator();
		assertEquals(seating, chart.getSeatingChart());

		chart.sellSeatByNumber('A', 5);
		chart.sellSeatByNumber('B', 2);
		chart.sellSeatByNumber('B', 3);
		chart.sellSeatByPrice(40);
		chart.sellSeatByPrice(50);
		chart.sellSeatByPrice(40);
		chart.getSeatingChart();
		seating = " 0  0 50 50  0" +System.lineSeparator() +
				"30  0  0  0 30" +System.lineSeparator() +
				"20 30 30 30 20" +System.lineSeparator() +
				"10 20 20 20 10" +System.lineSeparator() +
				"10 10 10 10 10" +System.lineSeparator();
		assertEquals(seating, chart.getSeatingChart());
	}

	/* 1 points */
	@Test
	public void problem9_4() {
		SeatingChart chart = new SeatingChart();
		String seating = "40 50 50 50 40" +System.lineSeparator() +
				"30 40 40 40 30" +System.lineSeparator() +
				"20 30 30 30 20" +System.lineSeparator() +
				"10 20 20 20 10" +System.lineSeparator() +
				"10 10 10 10 10" +System.lineSeparator();
		assertEquals(seating, chart.getSeatingChart());

		chart.sellSeatByNumber('b', 5);
		chart.sellSeatByNumber('c', 2);
		chart.sellSeatByNumber('b', 3);
		chart.sellSeatByPrice(10);
		chart.sellSeatByPrice(10);
		chart.sellSeatByPrice(30);
		chart.getSeatingChart();
		seating = "40 50 50 50 40" +System.lineSeparator() +
				" 0 40 40 40 30" +System.lineSeparator() +
				"20 30 30 30 20" +System.lineSeparator() +
				" 0 20 20 20  0" +System.lineSeparator() +
				"10 10 10 10 10" +System.lineSeparator();
		assertEquals(seating, chart.getSeatingChart());
	}


	/* 2 points */
	@Test
	public void problem10_1() {

		// test case 1:
		double[][] heights1 = {
				{0, 1, 2},
				{2, 5, 6},
				{3, -2, 0}
		};
		Terrain terrain = new Terrain(heights1);
		char[][] floodMap1 = {
				{'*', '*', '*'},
				{'*', '*', '*'},
				{'*', '*', '*'}
		};
		assertTrue(Arrays.deepEquals(floodMap1, terrain.getFloodMap(9)));
	}

	/* 2 points */
	@Test
	public void problem10_2() {

		// test case 2:
		double[][] heights1 = {
				{2, 1, 2},
				{2, 5, 6},
				{3, 1, 2}
		};
		Terrain terrain = new Terrain(heights1);
		// test case 2:
		char[][] floodMap1 = {
				{'-', '-', '-'},
				{'-', '-', '-'},
				{'-', '-', '-'}
		};
		assertTrue(Arrays.deepEquals(floodMap1, terrain.getFloodMap(1)));
	}

	/* 2 points */
	@Test
	public void problem10_3() {

		// test case 3:
		double[][] heights1 = {
				{4, 1, 2, 3},
				{2, 5, 6, 3},
				{3, -2, 3, 4},
				{6, 2, 10, 7}
		};
		Terrain terrain = new Terrain(heights1);
		// test case 2:
		char[][] floodMap1 = {
				{'-', '*', '*', '-'},
				{'*', '-', '-', '-'},
				{'-', '*', '-', '-'},
				{'-', '*', '-', '-'}
		};
		assertTrue(Arrays.deepEquals(floodMap1, terrain.getFloodMap(3)));
	}

	/* 2 points */
	@Test
	public void problem10_4() {

		// test case 4:
		double[][] heights1 = {
				{0, 1, 2, 0},
				{2, 5, 6, 0},
				{3, -2, 0, 3},
				{2, 4, 6, 0},
		};
		Terrain terrain = new Terrain(heights1);
		// test case 2:
		char[][] floodMap1 = {
				{'*', '*', '*', '*'},
				{'*', '*', '-', '*'},
				{'*', '*', '*', '*'},
				{'*', '*', '-', '*'}
		};
		assertTrue(Arrays.deepEquals(floodMap1, terrain.getFloodMap(6)));
	}

	/* 2 points */
	@Test
	public void problem10_5() {

		// test case 5:
		double[][] heights1 = {
				{0, -1, 2},
				{2, -5, -6},
				{3, -2, 0}
		};
		Terrain terrain = new Terrain(heights1);
		// test case 2:
		char[][] floodMap1 = {
				{'-', '-', '-'},
				{'-', '-', '-'},
				{'-', '-', '-'}
		};
		assertTrue(Arrays.deepEquals(floodMap1, terrain.getFloodMap(-10)));
	}

}