package labs.lab3;

import java.util.Scanner;
import java.util.ArrayList;

public class Main {

    /**
     * Accepts a String as a parameter and looks for a mirror image (backwards)
     * string at both the beginning and end of the given string. In other words,
     * zero or more characters at the very beginning of the given string, and at the
     * very end of the string in reverse order (possibly overlapping). For example,
     * the string "abXYZba" has the mirror end "ab".
     *
     * @param str the string to check for mirror ends
     * @return the mirror end, or the empty string if no mirror end exists
     */
    public static String problem1_mirrorEnds(String str) {
        String result = "";
        for (int i = 0, j = str.length() - 1; i < str.length(); i++) {
            if (j < 0)
                break;
            if (str.charAt(i) == str.charAt(j)) {
                result += str.charAt(i);
            } else {
                return result;
            }
            j--;
        }
        return result;
    }


    /**
     * Given a string, returns the length of the largest "block" in the string. A
     * block is a run of adjacent chars that are the same.
     *
     * @param str the string to check for blocks
     * @return the length of the largest block in the string
     */
    public static int problem2_maxBlock(String str) {
        int max_count = 1, count = 1;
        if (str.length() == 0)
            return 0;
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) == str.charAt(i - 1)) {
                count++;
                if (max_count < count)
                    max_count = count;
            } else
                count = 1;
        }
        return max_count;
    }


    /**
     * An uppercase 'E' in a string is "happy" if there is another uppercase 'E'
     * immediately to its left or right. Returns true if all the E's in the given
     * string are happy. (A string with no 'E' in it returns true.)
     *
     * @param str
     * @return whether or not all E's in the string are happy
     */
    public static boolean problem3_EHappy(String str) {
        if (str.length() == 0)
            return true;
        if (str.length() == 1 && str.charAt(0) == 'E')
            return false;
        if (str.length() == 1 && str.charAt(0) != 'E')
            return true;
        if (str.length() == 2 && str.charAt(0) == 'E' && str.charAt(1) == 'E')
            return true;
        if (str.charAt(0) == 'E' && str.charAt(1) != 'E') {
            return false;
        }
        if (str.charAt(str.length() - 1) == 'E' && str.charAt(str.length() - 2) != 'E') {
            return false;
        }
        int i = 1;
        do {
            if (str.charAt(i) == 'E' && str.charAt(i + 1) != 'E' && str.charAt(i - 1) != 'E')
                return false;
            i++;
        } while (i < str.length() - 2);
        return true;
    }


    /**
     * Returns the minimum number of twists to unlock a lock, based on the starting
     * and target values
     *
     * @param starting the current numbers of the lock
     * @param target   the combination required to unlock the lock
     * @return the minimum number of twists to unlock the lock
     */
    public static int problem4_getNumTwists(int starting, int target) {
        int result = 0;
        ArrayList<Integer> starting_digit = new ArrayList<Integer>();
        ArrayList<Integer> target_digit = new ArrayList<Integer>();
        while (starting > 0) {
            starting_digit.add(0, starting % 10);
            starting = starting / 10;
        }
        while (target > 0) {
            target_digit.add(0, target % 10);
            target = target / 10;
        }
        if (starting == 0) {
            starting_digit.add(0);
            starting_digit.add(0);
            starting_digit.add(0);
            starting_digit.add(0);
        }
        int twist_up_num = 0;
        int twist_down_num = 0;
        for (int i = 0; i < 4; i++) {
            int temp_start = starting_digit.get(i);
            while (temp_start != target_digit.get(i)) {
                twist_up_num++;
                if (temp_start == 9) {
                    temp_start = 0;
                } else {
                    temp_start++;
                }
            }
            temp_start = starting_digit.get(i);
            while (temp_start != target_digit.get(i)) {
                twist_down_num++;
                if (temp_start == 0) {
                    temp_start = 9;
                } else {
                    temp_start--;
                }
            }
            result += Math.min(twist_up_num, twist_down_num);
            twist_up_num = 0;
            twist_down_num = 0;
        }
        return result;
    }


    /**
     * Manages crowd control in an office
     *
     * @param in       the Scanner to be used for user input
     * @param capacity of the office
     *                 <p>
     *                 To run this method using the keyboard for user input, call it
     *                 like this: problem5_officeCrowdControl(new Scanner(System.in), 100);
     */
    public static void problem5_officeCrowdControl(Scanner in, int capacity) {
        int enter_or_leaving;
        int office_num = 0;
        System.out.print("People entering or leaving: ");
        enter_or_leaving = in.nextInt();
        office_num += enter_or_leaving;
        System.out.printf("People in office: %d | ", office_num);
        while (true) {
            System.out.print("People entering or leaving: ");
            enter_or_leaving = in.nextInt();
            if (office_num >= 0 && enter_or_leaving > 0 && (office_num + enter_or_leaving <= capacity))
                office_num += enter_or_leaving;
            if (office_num >= 0 && enter_or_leaving < 0 && (office_num - Math.abs(enter_or_leaving) >= 0))
                office_num -= Math.abs(enter_or_leaving);
            if (office_num == capacity) {
                System.out.printf("People in office: %d | ", office_num);
                System.out.print("Office is full");
                break;
            }
            System.out.printf("People in office: %d | ", office_num);
        }

    }


    /**
     * Takes an array of ints and returns an array that contains the exact same numbers
     * as the given array, but rearranged so that all the even numbers come before all
     * the odd numbers
     *
     * @param nums the input array
     * @return the rearranged array with all even numbers coming before all odd numbers
     */
    public static int[] problem6_evenOdd(int[] nums) {
        int[] even = new int[nums.length];
        int[] odd = new int[nums.length];
        int[] result = new int[nums.length];
        int temp_i = 0, temp_j = 0;
        int temp_k = 0;
        for (int i : nums) {
            if (i % 2 == 0) {
                even[temp_i] = i;
                temp_i++;
            } else {
                odd[temp_j] = i;
                temp_j++;
            }
        }
        for (int i = 0; i < temp_i; i++) {
            result[temp_k] = even[i];
            temp_k++;
        }
        for (int i = 0; i < temp_j; i++) {
            result[temp_k] = odd[i];
            temp_k++;
        }
        return result;
    }


    /**
     * Given a non-empty array of ints, returns a new array containing the elements from
     * the original array that come after the last occurrence of the number 4 in the original
     * array
     *
     * @param nums the input array
     * @return a new array containing the elements from the original array that come after
     * the last occurrence of the number 4 in the original array
     */
    public static int[] problem7_after4(int[] nums) {
        int position = 0;
        for (int i = nums.length - 1; i >= 0; i--) {
            if (nums[i] == 4) {
                position = i;
                break;
            }
        }
        int[] result = new int[nums.length - position - 1];
        for (int i = 0; i < result.length; i++) {
            position++;
            result[i] = nums[position];
        }
        return result;
    }
}