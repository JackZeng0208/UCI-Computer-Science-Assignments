package labs.lab3;

import java.util.ArrayList;

/**
 * Represents the height of a terrain at different points in a square
 */
public class Terrain {
	
	// ADD YOUR INSTANCE VARIABLES HERE
	private ArrayList<ArrayList<Double>> h = new ArrayList<ArrayList<Double>>();
	
	/**
	 * Constructor
	 * 
	 * @param heights	heights of the terrain at different points in a square
	 */
	public Terrain(double[][] heights) {
		for (int i = 0; i < heights.length; i++)
		{
			h.add(new ArrayList<Double>());
			for (int j = 0; j < heights[0].length; j++)
			{
				h.get(i).add(j, heights[i][j]);
			}
		}

	}

	
	/**
	 * Returns a flood map, showing which of the points in the terrain would be flooded
	 * if the water level was the given value. A point is flooded if the water level
	 * is > the height of the terrain.
	 * 
	 * In the flood map, each flooded point is represented by a * and each non-flooded
	 * point is represented by a -.
	 * 
	 * @param waterLevel	the water level
	 * 
	 * @return	the flood map
	 */
	public char[][] getFloodMap(double waterLevel) {
		char[][] result = new char[h.size()][h.size()];
		for (int i = 0; i < h.size(); i++)
			for (int j = 0; j < h.size(); j++)
			{
				if (h.get(i).get(j) < waterLevel)
					result[i][j] = '*';
				else
				{
					result[i][j] = '-';
				}
			}
		return result;
	}
}
