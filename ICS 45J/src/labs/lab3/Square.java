package labs.lab3;

/**
 * A magic square is an n x n matrix which, if filled with numbers, the sum of
 * the elements in each row, each column, and the two diagonal is the same
 * value.
 */
public class Square {
	// ADD YOUR INSTANCE VARIABLES HERE
	private int[][] s = new int[1001][1001];
	private int r = 0;

	/**
	 * Construct a Square object.
	 * 
	 * @param nums	the square array (assume it will always be n x n)
	 */
	public Square(int[][] nums) {
		this.r = nums.length;
		for(int i = 0; i < nums.length; i++)
			for(int j = 0; j < nums[0].length; j++)
			{
				s[i][j] = nums[i][j];
			}
	}

	/**
	 * Add the numbers in a row of the square.
	 * 
	 * @param i the row index
	 * @return the sum of the row
	 */
	public int rowSum(int i) {
		int result = 0;
		for (int j = 0; j < r; j++)
		{
			result += s[i][j];
		}
		return result;
	}

	/**
	 * Add the numbers in a column of the square.
	 * 
	 * @param i the column index
	 * @return the sum of the column
	 */
	public int columnSum(int i) {
		int result = 0;
		for (int j = 0; j < r; j++)
		{
			result += s[j][i];
		}
		return result;
	}

	/**
	 * Find the sum of the diagonal.
	 * 
	 * @param mainDiagonal true if it is the main diagonal (left/top to right/bottom),
	 * false otherwise (right/top to left/bottom)
	 * 
	 * @return sum the sum of the diagonal
	 */
	public int diagonalSum(boolean mainDiagonal) {
		int result = 0;
		if(mainDiagonal)
		{
			for(int i = 0; i < r; i++)
				result += s[i][i];
		}
		else
		{
			for(int i = r - 1; i >= 0; i--)
				result += s[r - 1 - i][i];
		}
		return result;
	}

	/**
	 * Determine if the square is a magic square.
	 * 
	 * @return true if square is a magic square, false otherwise
	 */
	public boolean isMagic() {
		int prev = 0;
		prev = this.rowSum(0);
		for(int i = 0; i < r; i++)
		{
			if (prev != this.rowSum(i))
				return false;
			if (prev != this.columnSum(i))
				return false;
		}
		if (this.diagonalSum(true) != prev)
			return false;
		if (this.diagonalSum(false) != prev)
			return false;
		return true;
	}
}
