package labs.lab9;

import java.util.Comparator;
import java.util.Map;

public class item_struct_comparator<K extends Comparable<? super K>, V extends Comparable<? super V>> implements Comparator<Map.Entry<K, V>> {
    // Reference: https://stackoverflow.com/questions/3074154/sorting-a-hashmap-based-on-value-then-key
    public int compare(Map.Entry<K, V> a, Map.Entry<K, V> b) {
        int cmp1 = a.getValue().compareTo(b.getValue());
        if (cmp1 != 0) {
            return cmp1;
        } else {
            return a.getKey().compareTo(b.getKey());
        }
    }

}