package labs.lab9;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import java.time.LocalDate;
import java.util.*;
import java.util.List;

public class ToDos_Frame extends JFrame
{
    private static final int FRAME_WIDTH = 1000;
    private static final int FRAME_HEIGHT = 320;
    private JLabel item_label, priority_label, note_label, month_label, day_label, year_label;
    private JTextField item_name, priority;
    private JCheckBox deadline;

    private DefaultListModel<String> model = new DefaultListModel<>();
    private JList<String> task_list = new JList<>(model);
    private Map<String, item_struct> task_info = new HashMap<>();
    private List<Map.Entry<String, item_struct>> list = new ArrayList<>();
    private Comparator<Map.Entry<String, item_struct>> comparator = new item_struct_comparator<>();


    private JComboBox<Integer> day;
    private JComboBox<Integer> year;
    private JTextArea note;
    private JButton save_item;
    private JButton new_item;
    private JButton toggle;
    private JButton delete;

    ItemListener date_listen = new date_listener();
    ActionListener button_listen = new button_listener();
    ListSelectionListener list_listen = new list_listener();
    ActionListener todoist_button_listen = new todoist_button_listener();

    private String item;
    private int p; // Priority

    private int d; // day
    private String m; // month
    private int y; // year



    public void enable_date(){
        day.setEnabled(true);
        year.setEnabled(true);
        month.setEnabled(true);
    }

    public void disable_date(){
        day.setEnabled(false);
        year.setEnabled(false);
        month.setEnabled(false);
    }

    public void initialize_date()
    {
        year = new JComboBox<>();
        day = new JComboBox<>();
        month = new JComboBox<>();
        set_month();
        set_year();
        set_default_day();
    }

    public void sort_model()
    {
        list = new ArrayList<>(task_info.entrySet());
        Collections.sort(list, comparator);
        model.clear();
        for (Map.Entry<String, item_struct> temp : list)
        {
            String temp_key = temp.getKey();
            if(task_info.get(temp_key).getDone())
            {
                String new_value = "<html><strike>" + temp_key + "</strike></html>";
                model.addElement(new_value);
            }
            else
                model.addElement(temp_key);
        }
    }

    public void clear_item()
    {
        item_name.setText("");
        priority.setText("");
        deadline.setSelected(false);
        disable_date();
        year.setSelectedItem(2022);
        day.setSelectedItem(1);
        month.setSelectedItem("January");
        note.setText("");
        task_list.clearSelection();
    }
    public void item_input()
    {
        if(deadline.isSelected())
        {
            try{
                p = Integer.parseInt(priority.getText());
                d = (int) day.getSelectedItem();
                m = (String) month.getSelectedItem();
                y = (int) year.getSelectedItem();
                task_info.put(item_name.getText(), new item_struct(p, d, y, m, note.getText(), true));
                sort_model();
                success_message();
            } catch (NumberFormatException nfe)
            {
                error_message();
            }
        }
        else
        {
            try{
                p = Integer.parseInt(priority.getText());
                task_info.put(item_name.getText(), new item_struct(p, 1, 2022, "January", note.getText(), false));
                sort_model();
                success_message();
            } catch (NumberFormatException nfe)
            {
                error_message();
            }
        }
    }
    public class list_listener implements ListSelectionListener{
        @Override
        public void valueChanged(ListSelectionEvent e) {
            if(!task_list.isSelectionEmpty())
            {
                if(task_info.containsKey(task_list.getSelectedValue()) && !save_item.isSelected())
                {
                    item_struct temp = new item_struct();
                    temp = task_info.get(task_list.getSelectedValue());
                    item_name.setText(task_list.getSelectedValue());
                    priority.setText(Integer.toString(temp.getStruct_p()));
                    note.setText(temp.getStruct_note());
                    deadline.setSelected(temp.getDeadline_status());
                    if(temp.getDeadline_status())
                    {
                        day.setSelectedItem(temp.getStruct_day());
                        month.setSelectedItem(temp.getStruct_month());
                        year.setSelectedItem(temp.getStruct_year());
                        enable_date();
                    }
                    else
                    {
                        disable_date();
                        year.setSelectedItem(2022);
                        day.setSelectedItem(1);
                        month.setSelectedItem("January");
                    }
                }
            }
        }
    }
    public class button_listener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String clicked_button = e.getActionCommand();
            if (deadline.isSelected())
                enable_date();
            else
                disable_date();
            if (clicked_button.equals("Save Item"))
            {
                if(item_name.getText().isEmpty() || priority.getText().isEmpty())
                    error_message();
                else
                {
                    if(task_info.containsKey(item_name.getText()))
                    {
                        if (task_list.isSelectionEmpty())
                            error_message();
                        else
                        {
                            if (task_list.getSelectedValue().equals(item_name.getText()))
                                item_input();
                            else
                                error_message();
                        }
                    }
                    else
                        item_input();
                }
            }
            if (clicked_button.equals("New Item"))
                clear_item();
        }
    }

    public class todoist_button_listener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String clicked_button = e.getActionCommand();
            if (!task_list.isSelectionEmpty())
            {
                int index = task_list.getSelectedIndex();
                String value = task_list.getSelectedValue().replaceAll("<html><strike>","").replaceAll("</strike></html>", "");
                if(clicked_button.equals("Toggle Done"))
                {
                    if (!task_info.get(value).getDone())
                    {
                        model.remove(index);
                        task_info.get(value).set_done_status(true);
                        sort_model();
                    }
                    else
                    {
                        model.remove(index);
                        task_info.get(value).set_done_status(false);
                        sort_model();
                    }
                }
                if(clicked_button.equals("Delete"))
                {
                    model.remove(index);
                    task_info.remove(value);
                    clear_item();
                }
            }
        }
    }


    public class date_listener implements ItemListener {
        //Reference: https://stackoverflow.com/questions/47999378/how-to-handle-2-different-combobox-itemlistener-in-same-itemlistener-function
        @Override
        public void itemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.DESELECTED)
            {
                if (e.getSource() == year || e.getSource() == month)
                {
                    int i = (int) year.getSelectedItem();
                    String j = (String) month.getSelectedItem();
                    set_day(j, i);
                }
            }
        }
    }

    public void error_message()
    {
        JOptionPane.showMessageDialog(null, "Invalid input!", "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void success_message()
    {
        JOptionPane.showMessageDialog(null, "Item saved!", "Success", JOptionPane.PLAIN_MESSAGE);
    }
    private JComboBox<String> month;
    public void set_month()
    {
        month.addItem("January");
        month.addItem("February");
        month.addItem("March");
        month.addItem("April");
        month.addItem("May");
        month.addItem("June");
        month.addItem("July");
        month.addItem("August");
        month.addItem("September");
        month.addItem("October");
        month.addItem("November");
        month.addItem("December");
    }

    public void set_year()
    {
        for (int i = 2022; i <= 2031; i++)
        {
            year.addItem(i);
        }
    }

    public void set_day(String month, int year)
    {
        day.removeAllItems();
        int subject_month = switch (month) {
            case "January" -> 1;
            case "February" -> 2;
            case "March" -> 3;
            case "April" -> 4;
            case "May" -> 5;
            case "June" -> 6;
            case "July" -> 7;
            case "August" -> 8;
            case "September" -> 9;
            case "October" -> 10;
            case "November" -> 11;
            case "December" -> 12;
            default -> 0;
        };

        LocalDate start_date = LocalDate.of(year,subject_month, 1);
        int i = 1;
        while (i <= start_date.lengthOfMonth())
        {
            day.addItem(i);
            i++;
        }

    }

    public void set_default_day()
    {
        for(int i = 1; i <= 31; i++)
            day.addItem(i);
    }
    public JMenu create_file_menu()
    {
        JMenu menu = new JMenu("File");
        JMenuItem exit_item = new JMenuItem("Exit");
        ActionListener listener = new exit_item_listener();
        exit_item.addActionListener(listener);
        menu.add(exit_item);
        return menu;
    }

    class exit_item_listener implements ActionListener
    {
        public void actionPerformed(ActionEvent event) {
            System.exit(0);
        }
    }

    public JPanel todoist_panel()
    {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(new TitledBorder(new EtchedBorder(), "ToDo List"));


        JScrollPane todo_pane = new JScrollPane(task_list);
        task_list.addListSelectionListener(list_listen);
        panel.add(todo_pane, BorderLayout.CENTER);

        JPanel right_panel = new JPanel(new GridBagLayout());

        toggle = new JButton("Toggle Done");
        delete = new JButton("Delete");

        toggle.addActionListener(todoist_button_listen);
        delete.addActionListener(todoist_button_listen);

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 5;
        c.gridy = 10;
        right_panel.add(toggle, c);
        c.gridy = 11;
        right_panel.add(delete, c);
        panel.add(right_panel, BorderLayout.EAST);

        return panel;
    }
    public JPanel item_panel()
    {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(new TitledBorder(new EtchedBorder(), "Item"));

        JPanel upper_panel = new JPanel(new BorderLayout());
        JPanel medium_panel = new JPanel();
        JPanel lower_panel = new JPanel(new BorderLayout());

        JPanel upper_sub_l1 = new JPanel();
        JPanel upper_sub_l2 = new JPanel();

        initialize_date();

        button_listen = new button_listener();

        item_label = new JLabel("Item:");
        upper_sub_l1.add(item_label);
        item_name = new JTextField(20);
        upper_sub_l1.add(item_name);
        upper_panel.add(upper_sub_l1, BorderLayout.NORTH);


        item_name.addActionListener(button_listen);

        priority_label = new JLabel("Priority:");
        upper_sub_l2.add(priority_label);
        priority = new JTextField(13);
        upper_sub_l2.add(priority);
        upper_panel.add(upper_sub_l2, BorderLayout.CENTER);

        panel.add(upper_panel, BorderLayout.NORTH);

        priority.addActionListener(button_listen);

        deadline = new JCheckBox("Deadline");
        deadline.addActionListener(button_listen);
        medium_panel.add(deadline);


        month_label = new JLabel("Month:");

        month.setSelectedItem("January");
        month.addItemListener(date_listen);
        medium_panel.add(month_label);
        medium_panel.add(month);


        day_label = new JLabel("Day:");
        medium_panel.add(day_label);

        day.setSelectedItem(1);
        medium_panel.add(day);

        year_label = new JLabel("Year:");
        medium_panel.add(year_label);


        year.setSelectedItem(2022);
        medium_panel.add(year);
        panel.add(medium_panel, BorderLayout.CENTER);
        year.addItemListener(date_listen);

        disable_date();

        JPanel lower_sub_l1 = new JPanel();
        JPanel lower_sub_l2 = new JPanel();
        note_label = new JLabel("Notes:");
        lower_sub_l1.add(note_label);
        note = new JTextArea(5, 30);
        JScrollPane scroll_pane = new JScrollPane(note);
        lower_sub_l1.add(scroll_pane);

        lower_panel.add(lower_sub_l1, BorderLayout.NORTH);

        save_item = new JButton("Save Item");
        save_item.addActionListener(button_listen);
        lower_sub_l2.add(save_item);

        new_item = new JButton("New Item");
        new_item.addActionListener(button_listen);
        lower_sub_l2.add(new_item);

        lower_panel.add(lower_sub_l2, BorderLayout.CENTER);
        panel.add(lower_panel, BorderLayout.SOUTH);
        return panel;
    }
    public void create_all_panel()
    {
        JPanel tpanel = todoist_panel();
        JPanel ipanel = item_panel();

        JPanel main_panel = new JPanel();
        main_panel.setLayout(new GridLayout(1, 2));
        main_panel.add(tpanel);
        main_panel.add(ipanel);
        add(main_panel, BorderLayout.CENTER);

    }
    public ToDos_Frame()
    {
        JMenuBar menu_bar = new JMenuBar();
        setJMenuBar(menu_bar);
        menu_bar.add(create_file_menu());
        create_all_panel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }
}
