package labs.lab9;

public class item_struct implements Comparable<item_struct>
{
    private int struct_p;
    private int struct_day;
    private int struct_year;
    private String struct_month;
    private String struct_note;
    private boolean deadline_status;

    private boolean done;


    public item_struct(int p, int day, int year, String month, String note, boolean b)
    {
        struct_p = p;
        struct_day = day;
        struct_month = month;
        struct_year = year;
        struct_note = note;
        deadline_status = b;
        done = false;
    }
    public item_struct()
    {
        struct_p = 0;
        struct_day = 1;
        struct_month = "January";
        struct_year = 2022;
        struct_note = "";
        deadline_status = false;
        done = false;
    }

    public int getStruct_day() {
        return struct_day;
    }
    public int getStruct_p() {
        return struct_p;
    }
    public int getStruct_year() {
        return struct_year;
    }
    public String getStruct_month() {
        return struct_month;
    }
    public String getStruct_note() {
        return struct_note;
    }


    public void set_done_status(boolean b)
    {
        done = b;
    }
    public boolean getDeadline_status() {
        return deadline_status;
    }

    public boolean getDone() {
        return done;
    }

    /*
    TODO:
        Check compareTo() rule
     */
    @Override
    public int compareTo(item_struct o) {
        if(this.done && !o.done)
        {
            return 1;
        }
        else if(!this.done && o.done)
        {
            return -1;
        }
        else
        {
            if(this.struct_p != o.struct_p)
                return this.struct_p - o.struct_p;
            else
                return 0;
        }
    }
}