package labs.lab6;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * A bank contains account numbers and balances of each customer.
 */
public class Bank {
	private String file_name;


	/**
	 * Construct a Bank object with accounts read from the given file
	 * 
	 * @param fileName the name of the file
	 */
	public Bank(String fileName) {
		try
		{
			File input_file = new File(fileName);
			if(!input_file.exists())
			{
				throw new FileNotFoundException();
			}
			this.file_name = fileName;
		} catch (FileNotFoundException e) {
			System.out.print("File: " + fileName + " not found");
		}
	}


	/**
	 * Gets the account with the lowest balance.
	 * 
	 * @return the account with the lowest balance, or null if there are
	 * no accounts in this bank
	 */
	public BankAccount getLowestBalanceAccount() {
		try
		{
			File input_file = new File(file_name);
			Scanner in = new Scanner(input_file);
			BufferedReader br = new BufferedReader(new FileReader(input_file));
			int min_account_num = 0;
			double min_account_balance = Double.MAX_VALUE;
			if (br.readLine() == null)
			{
				return null;
			}
			else
			{
				while(in.hasNextLine())
				{
					String line = in.nextLine();
					String[] temp = line.split(" ");
					if (Double.parseDouble(temp[1]) < min_account_balance)
					{
						min_account_balance = Double.parseDouble(temp[1]);
						min_account_num = Integer.parseInt(temp[0]);
					}
				}
				return new BankAccount(min_account_num, min_account_balance);
			}
		}catch (IOException e)
		{
			System.out.print("File: " + file_name + " not found");
		}
		return null;
	}
}
