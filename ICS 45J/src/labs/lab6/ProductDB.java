package labs.lab6;

import java.io.*;
import java.util.Scanner;

public class ProductDB {

	// ADD YOUR INSTANCE VARIABLES EHRE
	private String file_name;
	/**
	 * Constructor that reads in the product data from a file
	 * 
	 * @param productsFileName name of file with product data
	 */
	public ProductDB(String productsFileName) {

		try
		{
			File input_file = new File(productsFileName);
			if (!input_file.exists())
			{
				throw new FileNotFoundException();
			}
			this.file_name = productsFileName;

		}catch (FileNotFoundException e)
		{
			System.out.print("File: "+ productsFileName + " not found");
		}
	}


	/**
	 * Searches for the product with the given name and returns it if found,
	 * otherwise returns null.
	 * 
	 * @param productName name of product to search for
	 * 
	 * @return product with given name, or null if not found
	 */
	public Product findProduct(String productName) {
		try
		{
			File input_file = new File(file_name);
			String temp = "";
			Scanner in = new Scanner(input_file);
			while (in.hasNextLine())
			{
				String line = in.nextLine();
				if (line.length() != 0)
				{
					temp += line + System.lineSeparator();
				}
			}
			temp = temp.trim();
			PrintWriter out = new PrintWriter(input_file);
			in.close();
			out.print(temp);
			out.close();
			Scanner in_2 = new Scanner(input_file);
			while (in_2.hasNextLine())
			{
				String line = in_2.nextLine();
				Scanner line_scanner = new Scanner(line).useDelimiter(";");
				String temp_product_name = line_scanner.next();
				double temp_price = line_scanner.nextDouble();
				int temp_q = line_scanner.nextInt();
				if (temp_product_name.equals(productName)) {
					in_2.close();
					return new Product(temp_product_name, temp_price, temp_q);
				}
			}
			in_2.close();
			return null;
		}catch (FileNotFoundException e)
		{
			System.out.print("File: "+ file_name + " not found");
		}
		return null;
	}


	/**
	 * Adds a new product to the products DB if a product with the given name
	 * doesn't already exist
	 * 
	 * @param name     name of product
	 * @param price    price of product
	 * @param quantity quantity of product
	 */
	public void addProduct(String name, double price, int quantity) throws IOException {
		File input_file = new File(file_name);
		Scanner in = new Scanner(input_file);
		FileWriter fr = new FileWriter(input_file,true);
		BufferedWriter br = new BufferedWriter(fr);
		PrintWriter pr = new PrintWriter(br);
		if (this.findProduct(name) == null)
		{
				pr.print(System.lineSeparator()+name+";"+price+";"+quantity);
		}
		in.close();
		pr.close();
		fr.close();
		br.close();
	}
}
