package labs.lab6;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * An email messaging system.
 */
public class MessagingSystem {
	// ADD YOUR INSTANCE VARIABLES HERE
	private ArrayList<Mailbox> all_user;
	/**
	 * Constructs a MessagingSystem object
	 */
	public MessagingSystem() {
		all_user = new ArrayList<>();
	}


	/**
	 * Delivers a message to the recipient
	 * 
	 * @param sender		message sender
	 * @param recipient		message recipient
	 * @param text			text of the message
	 */
	public void deliver(String sender, String recipient, String text) {
		boolean recipient_existed = false;
		for (Mailbox i : all_user)
		{
			if (i.getUser().equals(recipient))
			{
				recipient_existed = true;
				break;
			}
		}
		if (!recipient_existed)
		{
			all_user.add(new Mailbox(recipient));
		}
		for (Mailbox i : all_user)
		{
			if(i.getUser().equals(recipient))
			{
				i.addMessage(new Message(sender, recipient, text));
			}
		}
	}

	
	/**
	 * Retrieves the messages for a user
	 * 
	 * @param user the user
	 */
	public String getMessages(String user) {
		for (Mailbox i : all_user)
		{
			if(i.getUser().equals(user))
			{
				return i.getAllMessages();
			}
		}
		return "";
	}
}
