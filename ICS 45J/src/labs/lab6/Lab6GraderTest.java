package labs.lab6;

import static org.junit.Assert.*;

import static org.junit.Assert.*;

import org.junit.Test;

import labs.lab6.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Lab6GraderTest {
	
	/* 6 points */
	@Test
	public void problems1And2_1() throws IOException {
		TextImprover ti = new TextImprover("res/overused-words-test.txt");
		
		// overused1-test.txt:
		String testFileName = "res/overused1-test.txt";
		ti.improveText(testFileName);
		List<String> expLinesList = Arrays.asList(
				"Cplusplus is the high-level, class-based, object-oriented programming language.",
				"The initial and reference implementation Cplusplus compilers, virtual machines,",
				"and class libraries were originally created by Dennis Ritchie and published by Belllabs under proprietary licenses."
				);
		List<String> actualLinesList = java.nio.file.Files.readAllLines(new File(testFileName).toPath());
		// trim the actual lines in case of any trailing spaces at the end of a line:
		for (int i=0; i<actualLinesList.size(); i++) {
			actualLinesList.set(i, actualLinesList.get(i).trim());
		}
		assertEquals(expLinesList, actualLinesList);
		
	}
	
	/* 6 points */
	@Test
	public void problems1And2_2() throws IOException {
		TextImprover ti = new TextImprover("res/overused-words-test.txt");
		
		// overused1-test.txt:
		String testFileName = "res/overused2-test.txt";
		ti.improveText(testFileName);
		List<String> expLinesList = Arrays.asList(
				"\"Cplusplus\" is THE high-level, class-based, object-oriented programming language.",
				"",
				"The initial and reference implementation Cplusplus compilers, virtual machines,",
				"and class libraries were originally created by \"Dennis Ritchie\" and published by Belllabs under proprietary licenses."
				);
		List<String> actualLinesList = java.nio.file.Files.readAllLines(new File(testFileName).toPath());
		// trim the actual lines in case of any trailing spaces at the end of a line:
		for (int i=0; i<actualLinesList.size(); i++) {
			actualLinesList.set(i, actualLinesList.get(i).trim());
		}
		assertEquals(expLinesList, actualLinesList);
		
	}
	/* 4 points */
	/* Test blank input file */
	@Test
	public void problems1And2_3() throws IOException {
		TextImprover ti = new TextImprover("res/overused-words-test.txt");
		
		String testFileName = "res/blank.txt";
		ti.improveText(testFileName);
		List<String> expLinesList = new ArrayList<String>();
		List<String> actualLinesList = java.nio.file.Files.readAllLines(new File(testFileName).toPath());
		assertEquals(expLinesList, actualLinesList);
	
	}
	/* 4 points */
	/* Test non existent input file */
	@Test
	public void problems1And2_4() throws IOException {
		TextImprover ti = new TextImprover("res/overused-words-test.txt");
		// test exception message is printed out:
		String nonExistentFileName = "res/Random.txt";
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));
		
		ti.improveText(nonExistentFileName);
		
		String result = output.toString();
		assertEquals("File: " + nonExistentFileName + " not found", result);
	
	}	
	
	/* 2 points */
	@Test
	public void problems3_1() {
		Student robert = new Student("Robert Navarro", 54);
		assertEquals(54, robert.getID());
		assertEquals("Robert Navarro", robert.getName());
	}
	
	/* 2 points */
	@Test
	public void problems3_2() {
		Student robert = new Student("Robert Navarro", 54);
		robert.addClass("ICS 45J");
		robert.addClass("ICS 45C");
		robert.addClass("INF 113");
		robert.dropClass("ICS 45C");
		List<String> expClasses = Arrays.asList("ICS 45J", "INF 113");
		assertEquals(expClasses, robert.getClasses());
	}
	
	/* 3 points */
	@Test
	public void problems3_3() {
		Student robert = new Student("Robert Navarro", 54);
		robert.addClass("ICS 45J");
		robert.addClass("ICS 45C");
		robert.addClass("INF 113");
		robert.dropClass("ICS 45C");
		// dropping a class that the student doesn't have:
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			robert.dropClass("ICS 45C");
		});
		String expectedMessage = "Cannot drop class ICS 45C because student is not enrolled in it";
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}
	
	/* 3 points */
	@Test
	public void problems3_4() {
		// assigning negative ID:
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			Student robert = new Student("Rob Navo", -1);
		});
		String expectedMessage = "ID cannot be negative";
		String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}
	
	/* 3 points */
	@Test
	public void problems4_1() {
		String prodFileName = "res/products1-test.txt";
		ProductDB db = new ProductDB(prodFileName);

		assertEquals(new Product("Seat Belt",8.29,16), db.findProduct("Seat Belt"));
		assertEquals(new Product("Seat Cover",7.2,3), db.findProduct("Seat Cover"));
		assertEquals(new Product("Green Bandana",5,2), db.findProduct("Green Bandana"));
	}
	
	/* 1 point */
	@Test
	public void problems4_2() {
		String prodFileName = "res/products1-test.txt";
		ProductDB db = new ProductDB(prodFileName);

		assertNull(db.findProduct("Dog Toy"));
	}
	
	/* 2 points */
	@Test
	public void problems4_3() throws IOException {
		String prodFileName = "res/products2-test.txt";
		ProductDB db = new ProductDB(prodFileName);
		assertNull(db.findProduct("Dog Toy1"));
		db.addProduct("Dog Toy1", 5.99, 3);
		db.addProduct("Dog Toy2", 3, 1);
		
		assertEquals(new Product("Dog Toy1", 5.99, 3), db.findProduct("Dog Toy1"));
		assertEquals(new Product("Dog Toy2", 3.0, 1), db.findProduct("Dog Toy2"));
		// check that file was updated:
		List<String> linesList = Arrays.asList("Seat Belt;8.29;16", "Seat Cover;7.2;3",
				"Green Bandana;5;2", "Dog Toy1;5.99;3",
				"Dog Toy2;3.0;1");
		assertEquals(linesList, java.nio.file.Files.readAllLines(new File(prodFileName).toPath()));
	}
	
	/* 2 point */
	@Test
	public void problems4_4() throws IOException {
		String prodFileName = "res/products1-test.txt";
		ProductDB db = new ProductDB(prodFileName);
		
		db.addProduct("Seat Belt",4.29,16);
		assertEquals(new Product("Seat Belt",8.29,16), db.findProduct("Seat Belt"));
		List<String> linesList = Arrays.asList("Seat Belt;8.29;16", "Seat Cover;7.2;3",
				"Green Bandana;5;2");
		assertEquals(linesList, java.nio.file.Files.readAllLines(new File(prodFileName).toPath()));
	}
	
	/* 2 points */
	@Test
	public void problems4_5() throws IOException {
		// test exception message is printed out:
		String nonExistentFileName = "res/Random.txt";
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));
		ProductDB db2 = new ProductDB(nonExistentFileName);

		String result = output.toString();
		assertEquals("File: " + nonExistentFileName + " not found", result);
	}	
	
	/*3 points */
	@Test
	public void problems5_1() {
		Bank bank1 = new Bank("res/accounts1-test.dat");
		assertEquals(new BankAccount(10, 10.0), bank1.getLowestBalanceAccount());
	}	
	
	/* 3 points */
	@Test
	public void problems5_2() {
		Bank bank1 = new Bank("res/accounts2-test.dat");
		assertEquals(new BankAccount(3348, 53.50), bank1.getLowestBalanceAccount());
	}
	
	/* 2 points */
	@Test
	public void problems5_3() {
		Bank bank5 = new Bank("res/blank.txt");
		assertNull(bank5.getLowestBalanceAccount());
	}
	
	/* 2 points */
	@Test
	public void problems5_4() throws IOException {
		// test exception message is printed out:
		String nonExistentFileName = "res/Random.txt";
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));
		Bank bank6 = new Bank(nonExistentFileName);

		String result = output.toString();
		assertEquals("File: " + nonExistentFileName + " not found", result);
	}	
	
	/* 5 points */
	@Test
	public void problems6through10_1() {
		MessagingSystem system = new MessagingSystem();
		system.deliver("Joe", "Emily", "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n");
		assertEquals(" -----\n"
				+ "From: Joe\n"
				+ "To: Emily\n"
				+ "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n", system.getMessages("Emily"));
		system.deliver("Robert", "Emily", "You are the best!!\n"
				+ "-Robert\n");
		assertEquals(" -----\n"
				+ "From: Joe\n"
				+ "To: Emily\n"
				+ "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n"
				+ " -----\n"
				+ "From: Robert\n"
				+ "To: Emily\n"
				+ "You are the best!!\n"
				+ "-Robert\n", system.getMessages("Emily"));
		
	}
	
	/* 5 points */
	@Test
	public void problems6through10_2() {
		MessagingSystem system = new MessagingSystem();
		system.deliver("John", "Rahul", "Hi Rahul,\n"
				+ "Hope you are doing good.\n"
				+ "Let's catch up today.\n"
				+ "-John\n");
		assertEquals(" -----\n"
				+ "From: John\n"
				+ "To: Rahul\n"
				+ "Hi Rahul,\n"
				+ "Hope you are doing good.\n"
				+ "Let's catch up today.\n"
				+ "-John\n", system.getMessages("Rahul"));
	}
	
	/* 5 points */
	@Test
	public void problems6through10_3() {
		MessagingSystem system = new MessagingSystem();
		system.deliver("Joe", "Emily", "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n");
		
		system.deliver("Emily", "Robert", "You are the best!!\n"
				+ "-Emily\n");
		
		assertEquals(" -----\n"
				+ "From: Joe\n"
				+ "To: Emily\n"
				+ "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n", system.getMessages("Emily"));
		
		assertEquals(" -----\n"
				+ "From: Emily\n"
				+ "To: Robert\n"
				+ "You are the best!!\n"
				+ "-Emily\n", system.getMessages("Robert"));
		
	}
	
	/* 5 points */
	@Test
	public void problems6through10_4() {
		MessagingSystem system = new MessagingSystem();
		system.deliver("John", "Rahul", "Hi Rahul,\n"
				+ "Hope you are doing good.\n"
				+ "Let's catch up today.\n"
				+ "-John\n");
		
		assertEquals(" -----\n"
				+ "From: John\n"
				+ "To: Rahul\n"
				+ "Hi Rahul,\n"
				+ "Hope you are doing good.\n"
				+ "Let's catch up today.\n"
				+ "-John\n", system.getMessages("Rahul"));
		
	}
	
	/* 8 points */
	@Test
	public void problems6through10_5() {
		Message message = new Message("Joe", "Emily", "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n");
		
		assertEquals("Emily",message.getRecipient());
		
	}
	
	/* 8 points */
	@Test
	public void problems6through10_6() {
		Message message = new Message("Joe", "Emily", "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n");
		
		assertEquals("From: Joe\n"
				+ "To: Emily\n"
				+ "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n",message.toString());	
	}
	
	/* 4 points */
	@Test
	public void problems6through10_7() {
		Mailbox mailbox = new Mailbox("Emily");
		assertEquals("Emily", mailbox.getUser());

	}
	
	/* 5 points */
	@Test
	public void problems6through10_8() {
		Mailbox mailbox = new Mailbox("Emily");
		assertEquals("Emily", mailbox.getUser());
		Message message1 = new Message("Joe", "Emily", "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n");
		
		Message message2 = new Message("Robert", "Emily", "You are the best!!\n"
				+ "-Robert\n");
		mailbox.addMessage(message1);
		mailbox.addMessage(message2);
		assertEquals(" -----\n"
				+ "From: Joe\n"
				+ "To: Emily\n"
				+ "Hi Emily,\n"
				+ "ICS 45J is the best course ever.\n"
				+ "Thanks for offering it.\n"
				+ "-Joe\n"
				+ " -----\n"
				+ "From: Robert\n"
				+ "To: Emily\n"
				+ "You are the best!!\n"
				+ "-Robert\n",mailbox.getAllMessages());
	}
	
	/* 5 points */
	@Test
	public void problems6through10_9() {
		Mailbox mailbox = new Mailbox("Rahul");
		assertEquals("Rahul", mailbox.getUser());
		Message message1 = new Message("John", "Rahul", "Hi Rahul,\n"
				+ "Hope you are doing good.\n"
				+ "Let's catch up today.\n"
				+ "-John\n");

		mailbox.addMessage(message1);
		assertEquals(" -----\n"
				+ "From: John\n"
				+ "To: Rahul\n"
				+ "Hi Rahul,\n"
				+ "Hope you are doing good.\n"
				+ "Let's catch up today.\n"
				+ "-John\n",mailbox.getAllMessages());
	}
}
