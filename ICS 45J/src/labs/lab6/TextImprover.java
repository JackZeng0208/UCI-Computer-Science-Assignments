package labs.lab6;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class to improve a piece of text by replacing over-used words with better
 * word choices
 */

public class TextImprover {
	private class Pair{
		private String origin;
		private String new_word;
		public Pair(String origin, String new_word)
		{
			this.origin = origin;
			this.new_word = new_word;
		}
		public String getOrigin()
		{
			return origin;
		}
		public String getNew_word()
		{
			return new_word;
		}
	}
	private ArrayList<Pair> word_list = new ArrayList<Pair>();


	/**
	 * Constructor
	 * 
	 * @param wordMapFileName	name of the file containing the over-used words and their replacements
	 */
	public TextImprover(String wordMapFileName) throws FileNotFoundException {
		File input_file = new File(wordMapFileName);
		Scanner in = new Scanner(input_file);
		while (in.hasNextLine())
		{
			String line = in.nextLine();
			int i = 0;
			while(!Character.isWhitespace(line.charAt(i)))
			{
				i++;
			}
			word_list.add(new Pair(line.substring(0,i), line.substring(i+1)));
		}
	}

	/**
	 * Replaces all of the over-used words in the given file with better words, based on the word map
	 * used to create this TextImprover
	 * 
	 * @param fileName	name of the file containing the text to be improved
	 */
	public void improveText(String fileName) throws IOException {
		try
		{
			File input_file = new File(fileName);
			ArrayList<String> content = new ArrayList<>();
			Scanner in = new Scanner(input_file);
			while(in.hasNextLine())
			{
				String line = in.nextLine();
				content.add(line);
			}
			String [] temp;
			PrintWriter out = new PrintWriter(input_file);
			int index = 0;
			for(String i : content)
			{
				temp = i.split(" ");
				int l = temp.length;
				int count = 0;
				for (String j : temp)
				{
					String word = j.replaceAll("[^A-Za-z]+", "");
					boolean judge = false;
					for (Pair k : word_list)
					{
						if(k.getOrigin().equalsIgnoreCase(word))
						{
							String all_upper = k.getOrigin().toUpperCase();
							String all_lower = k.getOrigin().toLowerCase();
							String first_upper = k.getOrigin().substring(0,1).toUpperCase() + k.getOrigin().substring(1);
							if (word.equals(all_lower))
							{
								out.print(j.replaceAll(word, k.getNew_word().toLowerCase()));
							}
							if (word.equals(all_upper))
							{
								out.print(j.replaceAll(word, k.getNew_word().toUpperCase()));
							}
							if (word.equals(first_upper) && word.length()!=1)
							{
								out.print(j.replaceAll(word, k.getNew_word().substring(0,1).toUpperCase() + k.getNew_word().substring(1)));
							}
							judge = true;
							break;
						}
					}
					if (!judge)
					{
						out.print(j);
					}
					if (count < l - 1)
					{
						out.print(" ");
						count++;
					}
					else if (index < content.size() - 1){
						out.println();
					}
				}
				index++;
			}
			in.close();
			out.close();
		}
		catch (FileNotFoundException exception)
		{
			System.out.printf("File: %s not found", fileName);
		}

	}
}