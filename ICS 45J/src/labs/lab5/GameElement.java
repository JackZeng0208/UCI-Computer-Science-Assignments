package labs.lab5;

public abstract class GameElement {
	
	// ADD YOUR INSTANCE VARIABLES HERE
	private String name;
	private double health_score;
	/**
	 * 
	 * @param name
	 * @param healthScore	from 0 (least healthy) to 10 (most healthy)
	 * 						(If below 0, sets it to 0; if above 10, sets it to 10)
	 */						
	public GameElement(String name, double healthScore) {
		this.name = name;
		if (healthScore < 0)
			this.health_score = 0;
		else if (healthScore > 10)
			this.health_score = 10;
		else
			this.health_score = healthScore;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	
	public double getHealthScore() {
		return health_score;
	}
}