package labs.lab4;

import java.util.ArrayList;
import java.util.List;

/**
 * A DVD item in a library
 */
public class DVD extends LibraryItem {
	// ADD YOUR INSTANCE VARIABLES HERE
	private List<String> actors = new ArrayList<String>();
	
	/**
	 * Constructor
	 * 
	 * @param title		title of the DVD
	 * @param actors	actors in the DVD
	 */
	public DVD(String title, List<String> actors) {
		super(title);
		this.actors = actors;
	}

	
	public List<String> getActors() {
		return actors;
	}
	
	
	public void setActors(List<String> actors) {
		this.actors = actors;
	}
	
	
	/**
	 * If this item is not already checked out, this method checks this item out and 
	 * returns the loan period; if it is already checked out, returns the String 
	 * "NOT ALLOWED"; overrides LibraryItem.checkOut()
	 */
	@Override
	public String checkOut() {
		if(!isCheckedOut())
		{
			setCheckedOut(true);
			return "7 days";
		}
		else
		{
			return "NOT ALLOWED";
		}
	}
	
	
	/**
	 * Returns true if the parameter object is a DVD that has the same instance 
	 * variable value(s) as this one
	 */
	public boolean equals(Object otherObject) {
		if(this.getClass() != otherObject.getClass())
			return false;
		DVD other = (DVD) otherObject;
		if(super.equals(other) && (this.actors.equals(other.actors)))
			return true;
		return false;
	}
}