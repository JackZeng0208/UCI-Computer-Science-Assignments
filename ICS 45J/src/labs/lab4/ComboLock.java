package labs.lab4;

import java.util.Arrays;

/**
 * A class to simulate a combination lock.
 */
public class ComboLock {
	// ADD YOUR INSTANCE VARIABLES HERE
	/**
	 * @param status Right: true, left: false
	 */
	private int[] nums = new int[3];
	private int[] test = new int[3];
	private int status;
	private int dial;
	/**
	 * Initializes the combination of the lock. Assume all inputs are valid ints
	 * between 0 and 39 inclusive
	 *
	 * @param num1 first number to turn right to
	 * @param num2 second number to turn left to
	 * @param num3 third number to turn right to
	 */
	public ComboLock(int num1, int num2, int num3) {
		nums[0] = num1;
		nums[1] = num2;
		nums[2] = num3;
		status = 1;
		dial = 0;
	}

	/**
	 * Resets the state of the lock so that it can be opened again; resets the
	 * dial so that it points to 0
	 */
	public void reset() {
		dial = 0;
		status = 1;
		test[0] = 0;
		test[1] = 0;
		test[2] = 0;

	}

	/**
	 * Turns lock left given number of ticks.
	 *
	 * @param ticks number of ticks to turn left; assume ticks always between 0
	 * and 39 inclusive
	 */
	public void turnLeft(int ticks) {
		if (status == 2)
		{
			int temp = ticks + dial;
			dial = temp % 40;
			test[status-1] = dial;
			status++;
			return;
		}
		reset();
	}

	/**
	 * Turns lock right given number of ticks
	 *
	 * @param ticks number of ticks to turn right; assume ticks always between 0
	 * and 39 inclusive
	 */
	public void turnRight(int ticks) {
		if(status == 1 || status == 3)
		{
			if ((dial - ticks) >= 0)
			{
				dial -= ticks;
			}
			else
			{
				int temp = Math.abs(dial - ticks);
				dial = 40 - (temp % 40);
			}
			test[status-1] = dial;
			status++;
			return;
		}
		reset();
	}

	/**
	 * Returns true if the lock can be opened now, false otherwise
	 *
	 * @return true if lock is in open state
	 */
	public boolean open() {
		return (Arrays.equals(nums, test));
	}
}
