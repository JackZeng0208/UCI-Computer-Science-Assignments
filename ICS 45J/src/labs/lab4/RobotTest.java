package labs.lab4;

import static org.junit.Assert.*;

import org.junit.Test;
import java.awt.Point;

public class RobotTest {

	@Test
    public void testConstructor() {
		Robot robot = new Robot();
        assertEquals(robot.getLocation(), new Point(0, 0));
        assertEquals("N", robot.getDirection());
    }

	@Test
    public void testTurnLeft() {
        Robot robot = new Robot();
        robot.turnLeft();
        assertEquals("W", robot.getDirection());
        robot.turnLeft();
        assertEquals("S", robot.getDirection());
        robot.turnLeft();
        assertEquals("E", robot.getDirection());
        robot.turnLeft();
        assertEquals("N", robot.getDirection());
    }
	
	@Test
    public void testTurnRight() {
		Robot robot = new Robot();
        robot.turnRight();
        assertEquals("E", robot.getDirection());
        robot.turnRight();
        assertEquals("S", robot.getDirection());
        robot.turnRight();
        assertEquals("W", robot.getDirection());
        robot.turnRight();
        assertEquals("N", robot.getDirection());
    }
	
	@Test
    public void testMove() {
		Robot robot = new Robot();
        robot.turnRight();
        assertTrue(robot.move());
        assertTrue(robot.move());
        assertTrue(robot.move());
        robot.turnRight();
        assertTrue(robot.move());
        assertTrue(robot.move());
        assertTrue(robot.move());
        assertTrue(robot.move());
        assertTrue(robot.move());
        assertFalse(robot.move());
        assertFalse(robot.move());
        robot.turnRight();
        assertTrue(robot.getLocation().equals(new Point(3, -7)));
        assertEquals("W", robot.getDirection());
        robot.turnRight();
        assertFalse(robot.move());
        assertTrue(robot.move());
        assertTrue(robot.move());
        assertTrue(robot.getLocation().equals(new Point (3, -4)));

    }
}
