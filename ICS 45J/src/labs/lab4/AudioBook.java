package labs.lab4;

/**
 * An audio book item in a library
 */
public class AudioBook extends Book {
	// ADD YOUR INSTANCE VARIABLES HERE
	private double play_time;
	private int check_num;
	/**
	 * Constructor
	 * 
	 * @param title			audio book title
	 * @param author		audio book author
	 * @param playingTime	audio book playing time
	 */
	public AudioBook(String title, String author, double playingTime) {
		super(title, author);
		this.play_time = playingTime;
		this.check_num = 2;
	}

	
	public double getPlayingTime() {
		return this.play_time;
	}
	
	
	public void setPlayingTime(double playingTime) {
		this.play_time = playingTime;
	}
	
	
	/**
	 * If the max number of checkouts for this item has not already been reached,
	 * this method checks this item out and returns the loan period; if no more
	 * check outs are available for this item, returns the String "NOT ALLOWED";
	 * overrides LibraryItem.checkOut()
	 */
	@Override
	public String checkOut() {
		if (check_num > 0)
		{
			check_num--;
			return "28 days";
		}
		else
			return "NOT ALLOWED";
	}
	
	
	/**
	 * Checks in this item  (frees up one checkout for this item); overrides
	 * LibraryItem.checkIn()
	 */
	@Override
	public void checkIn() {
		if (check_num < 2)
			check_num++;
	}
	
	
	/**
	 * Returns true if the parameter object is an AudioBook that has the same
	 * instance variable value(s) as this one
	 */
	public boolean equals(Object otherObject) {
		if(this.getClass() != otherObject.getClass())
			return false;
		AudioBook other = (AudioBook) otherObject;
		if (super.equals(other) && this.play_time == other.play_time && this.check_num == other.check_num)
			return true;
		return false;
	}
}