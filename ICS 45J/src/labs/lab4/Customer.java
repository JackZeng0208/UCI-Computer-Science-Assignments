package labs.lab4;

/**
 * A class for a customer in Robert's pet store. For every $100 spent, the
 * customer gets a 10% discount that will be automatically applied to
 * a future purchase. Note that some results will have more than two places
 * after the decimal points, and that's ok.
 */
public class Customer
{
    // ADD YOUR INSTANCE VARIABLES HERE
    private int discount_num;
    private double purchase_amount;
    private boolean first_discount_check;
    private int previous_hundred_digit;

    /**
     * Creates a new customer
     */
    public Customer() 
    {
        discount_num = 0;
        purchase_amount = 0;
        first_discount_check = false;
        previous_hundred_digit = 0;
    }

    /**
     * adds a purchase for the amount to the customer's account; assume
     * the amount will always be a valid monetary amount > 0
     *
     * @param amount amount of purchase
     */
    public void makePurchase(double amount) {
        double actual_amount = amount;
        if (discount_num > 0)
        {
            actual_amount *= 0.9;
            discount_num--;
        }
        purchase_amount += actual_amount;
        if (purchase_amount >= 100 && !first_discount_check)
        {
            discount_num += (int) purchase_amount / 100;
            previous_hundred_digit = (int) purchase_amount / 100;
            first_discount_check = true;
        }
        if (purchase_amount >= 100 && (previous_hundred_digit < (int) purchase_amount / 100))
        {
            discount_num += (int) purchase_amount / 100 - previous_hundred_digit;
            previous_hundred_digit = (int) purchase_amount / 100;
        }

    }

    /**
     * Returns the number of discounts this customer has on their account
     * to be used on future purchases
     *
     * @return number of discounts to use
     */
    public int getNumDiscountsToUse() {
        return discount_num;
    }

    /**
     * Returns the total amount this customer has spent on all their purchases
     *
     * @return total amount spent
     */
    public double getTotalAmountSpent() {
        return purchase_amount;
    }
}

