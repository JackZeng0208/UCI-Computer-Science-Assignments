package labs.lab4;

import java.awt.Point;

/**
 * A class for simulating a robot wandering on a 10 x 10 plane.
 */
public class Robot {
	
	private Point robot_location;
	/**
	 * North: 0
	 * South: 1
	 * West: 2
	 * East: 3
	 */
	private int direction;

	
	/**
	 * Creates a Robot at location (0, 0), which is in the center of the plane,
	 * and facing north
	 */
	public Robot() {
		robot_location = new Point();
		direction = 0;
	}
	

	/**
	 * Changes the direction but not the location
	 */
	public void turnLeft() {
		switch (direction) {
			case 0 -> direction = 2;
			case 1 -> direction = 3;
			case 2 -> direction = 1;
			case 3 -> direction = 0;
			default -> {
			}
		}
	}
	

	/**
	 * Changes the direction but not the location
	 */
	public void turnRight() {
		switch (direction) {
			case 0 -> direction = 3;
			case 1 -> direction = 2;
			case 2 -> direction = 0;
			case 3 -> direction = 1;
			default -> {
			}
		}
	}

	
	/**
	 * Moves the robot by one unit in the direction it is facing. Returns true if the
	 * robot is still on the plane, or false if it has fallen off.
	 * 
	 * Note 1: In coordinate terms, |x| must be <= 5 and |y| must be <= 5 for the robot
	 * to stay on the plane (see below example).
	 * 
	 * Note 2: This method can still be called on a robot that has fallen off the plane,
	 * and its location should be updated as if it was still on the plane.
	 * 
	 * @return	true if the robot is still on the plane, false otherwise
	 */
	public boolean move() {
		if (direction == 0)
			robot_location.translate(0, 1);
		if (direction == 1)
			robot_location.translate(0, -1);
		if (direction == 2)
			robot_location.translate(-1, 0);
		if (direction == 3)
			robot_location.translate(1, 0);
		int x = robot_location.x;
		int y = robot_location.y;
		if (Math.abs(x) <= 5 && Math.abs(y) <= 5)
			return true;
		else
			return false;
	}

	
	/**
	 * Returns the robot's current location on the plane
	 * 
	 * @return	robot's current location
	 */
	public Point getLocation() {

		return robot_location.getLocation(); // FIX ME
	}

	
	/**
	 * Returns "N", "E", "S", or "W" (for north, east, south, or west, respectively)
	 * 
	 * @return	the robot's direction
	 */
	public String getDirection() {
		if (direction == 0)
			return "N";
		if (direction == 1)
			return "S";
		if (direction == 2)
			return "W";
		if (direction == 3)
			return "E";
		return "";
	}
}
