package labs.lab4;

/**
 * An abstract item that can be checked out of a library
 */
public abstract class LibraryItem {
	// ADD YOUR INSTANCE VARIABLES HERE
	
	private boolean checkout;
	private String title;
	/**
	 * Constructor
	 * 
	 * @param title	title of the item
	 */
	public LibraryItem(String title) {
		this.title = title;
		checkout = false;
	}
	
	
	public String getTitle() {
		return title;
	}
	
	
	public boolean isCheckedOut() {
		return checkout;
	}
	
	
	public void setCheckedOut(boolean checkedOut) {
		checkout = checkedOut;
	}
	
	
	/**
	 * Causes this item to be checked out
	 * 
	 * @return	a message about the check out
	 */
	public abstract String checkOut();
	
	
	/**
	 * Causes this item to be checked in
	 */
	public void checkIn() {
		checkout = false;
	}
	
	
	public String toString() {
		return title;
	}
	
	
	/**
	 * Returns true if the parameter object is a LibraryItem that has the same instance
	 * variable value(s) as this one
	 */
	public boolean equals(Object otherObject) {
		if(this.getClass() != otherObject.getClass())
			return false;
		LibraryItem other = (LibraryItem) otherObject;
		if((other.title.equals(this.title)) && (this.checkout == other.checkout))
			return true;
		return false;
	}
}