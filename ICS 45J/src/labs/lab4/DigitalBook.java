package labs.lab4;

/**
 * Digital book item in a library
 */
public class DigitalBook extends Book {
	// ADD YOUR INSTANCE VARIABLES HERE
	private int page_num;
	private int check_num;
	/**
	 * Constructor
	 * 
	 * @param title		book title
	 * @param author	book author
	 * @param numPages	number of pages
	 */
	public DigitalBook(String title, String author, int numPages) {
		super(title,author);
		this.page_num = numPages;
		check_num = 3;
	}

	
	public int getNumPages() {
		return this.page_num;
	}
	
	
	public void setNumPages(int numPages) {
		page_num = numPages;
	}
	
	
	/**
	 * If the max number of checkouts for this item has not already been reached,
	 * this method checks this item out and returns the loan period; if no more
	 * check outs are available for this item, returns the String "NOT ALLOWED";
	 * overrides LibraryItem.checkOut()
	 */
	@Override
	public String checkOut() {
		if(check_num > 0)
		{
			check_num--;
			return "14 days";
		}else
			return "NOT ALLOWED";
	}
	
	
	/**
	 * Checks in this item  (frees up one checkout for this item); overrides 
	 * LibraryItem.checkIn()
	 */
	@Override
	public void checkIn() {
		if(check_num < 3)
			check_num++;
	}
	
	
	/**
	 * Returns true if the parameter object is a DigitalBook that has the same
	 * instance variable value(s) as this one
	 */
	public boolean equals(Object otherObject) {
		if(this.getClass() != otherObject.getClass())
			return false;
		DigitalBook other = (DigitalBook) otherObject;
		if (super.equals(other) && this.page_num == other.page_num && this.check_num == other.check_num)
			return true;
		return false;
	}
}