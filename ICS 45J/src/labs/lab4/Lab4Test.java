package labs.lab4;
import java.awt.*;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import labs.lab4.*;
import labs.lab4.Robot;
public class Lab4Test {
    private final double EPSILON = 1e-13;
    /**
     * Testcase for Problem 1
     * <p>
     * Test the following:
     * Initial direction
     * Initial location
     * <p>
     * 2 pts
     */
    @Test
    public void problem1_initialization_2pts() {
        // Initialization
        Robot robot = new Robot();
        Assert.assertEquals("N", robot.getDirection());
        Assert.assertEquals(robot.getLocation(), new Point(0, 0)); //Initial point:(0,0)
    }
    /**
     * Testcase for Problem 1
     * <p>
     * Test the following:
     * Turn left
     * <p>
     * 2 pts
     */
    @Test
    public void problem1_turnLeft_2pts() {
        // Initialization
        Robot robot = new Robot();
        Assert.assertEquals("N", robot.getDirection());
        robot.turnLeft();
        Assert.assertEquals("W", robot.getDirection());
        robot.turnLeft();
        Assert.assertEquals("S", robot.getDirection());
        robot.turnLeft();
        Assert.assertEquals("E", robot.getDirection());
        robot.turnLeft();
        Assert.assertEquals("N", robot.getDirection());
    }
    /**
     * Testcase for Problem 1
     * <p>
     * Test the following:
     * Turn right
     * <p>
     * 1 pts
     */
    @Test
    public void problem1_turnRight_1pts() {
        // Initialization
        Robot robot = new Robot();
        Assert.assertEquals("N", robot.getDirection());
        robot.turnRight();
        Assert.assertEquals("E", robot.getDirection());
        robot.turnRight();
        Assert.assertEquals("S", robot.getDirection());
        robot.turnRight();
        Assert.assertEquals("W", robot.getDirection());
        robot.turnRight();
        Assert.assertEquals("N", robot.getDirection());
    }
    /**
     * Testcase for Problem 1
     * <p>
     * Test the following:
     * Initial direction
     * Move along the North direction
     * <p>
     * 1 pts
     */
    @Test
    public void problem1_moveN_1pts() {
        // Initialization
        Robot robot = new Robot();
        for (int i = 0; i < 5; i++) {
            Assert.assertTrue(robot.move()); // From (0,0) to (0,5)
        }
        Assert.assertFalse(robot.move());// (0,6)
    }
    /**
     * Testcase for Problem 1
     * <p>
     * Test the following:
     * Turn left
     * Move along the West direction
     * <p>
     * 1 pts
     */
    @Test
    public void problem1_moveWest_1pts() {
        // Initialization
        Robot robot = new Robot();
        //Check: Direction N -> W
        robot.turnLeft();
        Assert.assertEquals("W", robot.getDirection());
        for (int i = 0; i < 5; i++) {
            Assert.assertTrue(robot.move()); // From (0,0) to (-5,0)
        }
        Assert.assertFalse(robot.move());// (-6,0)
    }
    /**
     * Testcase for Problem 1
     * <p>
     * Test the following:
     * Turn left
     * Move along the South direction
     * <p>
     * 1 pts
     */
    @Test
    public void problem1_moveSouth_1pts() {
        // Initialization
        Robot robot = new Robot();
        //Check: Direction N -> S
        robot.turnLeft();
        robot.turnLeft();
        Assert.assertEquals("S", robot.getDirection());
        for (int i = 0; i < 5; i++) {
            Assert.assertTrue(robot.move()); // From (0,0) to (0,-5)
        }
        Assert.assertFalse(robot.move());// (0,-6)
    }
    /**
     * Testcase for Problem 1
     * <p>
     * Test the following:
     * Turn right
     * Move along the East direction
     * <p>
     * 1 pts
     */
    @Test
    public void problem1_moveEast_1pts() {
        // Initialization
        Robot robot = new Robot();
        //Check: Direction N -> E
        robot.turnRight();
        Assert.assertEquals("E", robot.getDirection());
        for (int i = 0; i < 5; i++) {
            Assert.assertTrue(robot.move()); // From (0,0) to (5,0)
        }
        Assert.assertFalse(robot.move());// (6,0)
    }
    /**
     * Testcase for Problem 1
     * <p>
     * Test the following:
     * Change the position outside
     * <p>
     * 1 pts
     */
    @Test
    public void problem1_PositionOutside_1pts() {
        // Initialization
        Robot robot = new Robot();
        for (int i = 0; i < 5; i++) {
            Assert.assertTrue(robot.move()); // From (0,0) to (0,5)
        }
        Assert.assertFalse(robot.move());// (0,6)
        robot.turnRight();
        Assert.assertFalse(robot.move());// (1,6)
        Assert.assertEquals(new Point(1, 6), robot.getLocation());
        robot.turnRight();
        Assert.assertTrue(robot.move());// (1,5)
        Assert.assertEquals(new Point(1, 5), robot.getLocation());
    }
    /**
     * Testcase for Problem 3
     * <p>
     * Test the following:
     * Purchase 100 (purchase more than $100)
     * <p>
     * 2 pts
     */
    @Test
    public void problem3_purchase100_2pts() {
        Customer customer = new Customer();
        customer.makePurchase(100);
        Assert.assertEquals(1, customer.getNumDiscountsToUse());
        Assert.assertEquals(100, customer.getTotalAmountSpent(), EPSILON);
    }
    /**
     * Testcase for Problem 3
     * <p>
     * Test the following:
     * Purchase 99 (purchase less than $100)
     * <p>
     * 2 pts
     */
    @Test
    public void problem3_purchase99_2pts() {
        Customer customer = new Customer();
        customer.makePurchase(99);
        Assert.assertEquals(0, customer.getNumDiscountsToUse());
        Assert.assertEquals(99, customer.getTotalAmountSpent(), EPSILON);
    }
    /**
     * Testcase for Problem 3
     * <p>
     * Test the following:
     * Purchase 100 + 100 (discount apply correctly)
     * <p>
     * 2 pts
     */
    @Test
    public void problem3_purchase100then100_2pts() {
        Customer customer = new Customer();
        customer.makePurchase(100);
        Assert.assertEquals(1, customer.getNumDiscountsToUse());
        Assert.assertEquals(100, customer.getTotalAmountSpent(), EPSILON);
        customer.makePurchase(100);
        Assert.assertEquals(0, customer.getNumDiscountsToUse());
        Assert.assertEquals(190, customer.getTotalAmountSpent(), EPSILON);
    }
    /**
     * Testcase for Problem 3
     * <p>
     * Test the following:
     * Purchase 99 + 2 + 20 (discount apply only when the amount is more than $100)
     * <p>
     * 2 pts
     */
    @Test
    public void problem3_purchase99then2then20_2pts() {
        Customer customer = new Customer();
        customer.makePurchase(99);
        Assert.assertEquals(0, customer.getNumDiscountsToUse());
        Assert.assertEquals(99, customer.getTotalAmountSpent(), EPSILON);
        customer.makePurchase(2);
        Assert.assertEquals(1, customer.getNumDiscountsToUse());
        Assert.assertEquals(101, customer.getTotalAmountSpent(), EPSILON);
        customer.makePurchase(20);
        Assert.assertEquals(0, customer.getNumDiscountsToUse());
        Assert.assertEquals(119, customer.getTotalAmountSpent(), EPSILON);
    }
    /**
     * Testcase for Problem 3
     * <p>
     * Test the following:
     * Purchase 299 + 2 + 20 + 20 (discount apply when the customer has multiple
     discounts to use, no discount apply when there is no discounts to use)
     * <p>
     * 2 pts
     */
    @Test
    public void problem3_purchase299then2then20then20_2pts() {
        Customer customer = new Customer();
        customer.makePurchase(299);
        Assert.assertEquals(2, customer.getNumDiscountsToUse());
        Assert.assertEquals(299, customer.getTotalAmountSpent(), EPSILON);
        customer.makePurchase(2);
        Assert.assertEquals(2, customer.getNumDiscountsToUse());
        Assert.assertEquals(300.8, customer.getTotalAmountSpent(), EPSILON);
        customer.makePurchase(20);
        Assert.assertEquals(1, customer.getNumDiscountsToUse());
        Assert.assertEquals(318.8, customer.getTotalAmountSpent(), EPSILON);
        customer.makePurchase(20);
        Assert.assertEquals(0, customer.getNumDiscountsToUse());
        Assert.assertEquals(336.8, customer.getTotalAmountSpent(), EPSILON);
        customer.makePurchase(20);
        Assert.assertEquals(0, customer.getNumDiscountsToUse());
        Assert.assertEquals(356.8, customer.getTotalAmountSpent(), EPSILON);
    }
    /**
     * Testcase for Problem 5
     * <p>
     * Test the following:
     * Direction detect
     * <p>
     * 1 pts
     */
    @Test
    public void problem5_testDirection_1pts() {
        // Initialization
        ComboLock lock = new ComboLock(0, 0, 0);
        lock.turnRight(0);
        lock.turnLeft(0);
        lock.turnRight(0);
        Assert.assertTrue(lock.open());
    }
    /**
     * Testcase for Problem 5
     * <p>
     * Test the following:
     * Lock open
     * <p>
     * 2 pts
     */
    @Test
    public void problem5_lockOpen_2pts() {
        // Initialization
        ComboLock lock = new ComboLock(37, 11, 5);
        lock.turnRight(3);
        lock.turnLeft(14);
        lock.turnRight(6);
        Assert.assertTrue(lock.open());
    }
    /**
     * Testcase for Problem 5
     * <p>
     * Test the following:
     * Lock not open with wrong number
     * <p>
     * 3 pts
     */
    @Test
    public void problem5_lockNotOpenWrongNumber_3pts() {
        // Initialization
        ComboLock lock = new ComboLock(37, 11, 5);
        // Test secret3: (37, 11, 4)
        lock.turnRight(3);
        lock.turnLeft(14);
        lock.turnRight(7);
        Assert.assertFalse(lock.open());
        // Test secret2: (37, 12, 5)
        lock.reset();
        lock.turnRight(3);
        lock.turnLeft(15);
        lock.turnRight(7);
        Assert.assertFalse(lock.open());
        // Test secret1: (38, 11, 5)
        lock.reset();
        lock.turnRight(2);
        lock.turnLeft(13);
        lock.turnRight(6);
        Assert.assertFalse(lock.open());
    }
    /**
     * Testcase for Problem 5
     * <p>
     * Test the following:
     * Lock not open with right number wrong direction
     * <p>
     * 4 pts
     */
    @Test
    public void problem5_lockNotOpenWrongDirection_4pts() {
        // Initialization
        ComboLock lock = new ComboLock(37, 11, 5);
        // Test secret3
        lock.turnRight(3);
        lock.turnLeft(14);
        lock.turnLeft(34);
        Assert.assertFalse(lock.open());
        // Test secret2
        lock.reset();
        lock.turnRight(3);
        lock.turnRight(26);
        lock.turnRight(6);
        Assert.assertFalse(lock.open());
        // Test secret1
        lock.reset();
        lock.turnLeft(37);
        lock.turnRight(26);
        lock.turnRight(6);
        Assert.assertFalse(lock.open());
        // Test reverse direction
        lock.reset();
        lock.turnLeft(37);
        lock.turnRight(26);
        lock.turnLeft(34);
        Assert.assertFalse(lock.open());
    }
    /**
     * Testcase for Problem 6
     * <p>
     * Test the following:
     * Check whether BasicAccount is a subclass of BankAccount
     * <p>
     * 1 pts
     */
    @Test
    public void problem6_isBankAccount_1pts() {
        Assert.assertTrue(BankAccount.class.isAssignableFrom(BasicAccount.class));
    }
    /**
     * Testcase for Problem 6
     * <p>
     * Test the following:
     * Withdraw successfully if there is enough money
     * <p>
     * 3 pts
     */
    @Test
    public void problem6_withdraw_3pts() {
        BankAccount account = new BasicAccount(10.00);
        account.withdraw(8.00);
        Assert.assertEquals(2.00, account.getBalance(), EPSILON);
    }
    /**
     * Testcase for Problem 6
     * <p>
     * Test the following:
     * Withdraw unsuccessfully if there is no enough money
     * <p>
     * 3 pts
     */
    @Test
    public void problem6_notWithdraw_3pts() {
        BankAccount account = new BasicAccount(10.00);
        account.withdraw(12.00);
        Assert.assertEquals(10.00, account.getBalance(), EPSILON);
    }
    /**
     * Testcase for Problem 6
     * <p>
     * Test the following:
     * Withdraw money for both successful and unsuccessful scenario
     * <p>
     * 3 pts
     */
    @Test
    public void problem6_hybridWithdraw_3pts() {
        BankAccount account = new BasicAccount(100.00);
        account.withdraw(12.00);
        Assert.assertEquals(88.00, account.getBalance(), EPSILON);
        account.withdraw(112.00);
        Assert.assertEquals(88.00, account.getBalance(), EPSILON);
        account.withdraw(88.00);
        Assert.assertEquals(0.00, account.getBalance(), EPSILON);
    }
    /**
     * Testcase for Problem 7-10
     * <p>
     * Test the following:
     * Class Book, DVD, Magazine is subclasses of LibraryItem
     * Class AudioBook, DigitalBook, PrintBook is subclasses of Book
     * <p>
     * 5 pts
     */
    @Test
    public void problem7to10_instanceOfParentClass_5pts() {
        Assert.assertTrue(LibraryItem.class.isAssignableFrom(Book.class));
        Assert.assertTrue(LibraryItem.class.isAssignableFrom(DVD.class));
        Assert.assertTrue(LibraryItem.class.isAssignableFrom(Magazine.class));
        Assert.assertTrue(Book.class.isAssignableFrom(AudioBook.class));
        Assert.assertTrue(Book.class.isAssignableFrom(DigitalBook.class));
        Assert.assertTrue(Book.class.isAssignableFrom(PrintBook.class));
    }
    /**
     * Testcase for Problem 7-10
     * <p>
     * Test the following:
     * If two Objects of the same type has the same instance variable value(s),
     then equal
     * <p>
     * 10 pts
     */
    @Test
    public void problem7to10_equalOfLibraryItem_10pts() {
        DVD d1 = new DVD("Titanic", List.of("Leonardo Wilhelm DiCaprio", "Kate Elizabeth Winslet"));
                DVD d2 = new DVD("Titanic", List.of("Leonardo Wilhelm DiCaprio", "Kate Elizabeth Winslet"));
                        Assert.assertTrue(d1.equals(d2));
        Magazine m1 = new Magazine("Time", 433, "February 2022");
        Magazine m2 = new Magazine("Time", 433, "February 2022");
        Assert.assertTrue(m1.equals(m2));
        DigitalBook db1 = new DigitalBook("Hillbilly Elegy", "J.D. Vance", 263);
        DigitalBook db2 = new DigitalBook("Hillbilly Elegy", "J.D. Vance", 263);
        Assert.assertTrue(db1.equals(db2));
        PrintBook pb1 = new PrintBook("Caste", "Isabel Wilkerson", 497);
        PrintBook pb2 = new PrintBook("Caste", "Isabel Wilkerson", 497);
        Assert.assertTrue(pb1.equals(pb2));
        AudioBook ab1 = new AudioBook("Hillbilly Elegy", "J.D.  Vance", 5.7);
        AudioBook ab2 = new AudioBook("Hillbilly Elegy", "J.D.  Vance", 5.7);
        Assert.assertTrue(ab1.equals(ab2));
    }
    /**
     * Testcase for Problem 7-10
     * <p>
     * Test the following:
     * If Object type is different, then not equal
     * If variable value(s) is(are) different, then not equal
     * <p>
     * 10 pts
     */
    @Test
    public void problem7to10_notEqualOfLibraryItem_10pts() {
        LibraryItem libraryItem1 = new AudioBook("The Revenant", "Michael Punke",
                314);
        LibraryItem libraryItem2 = new DVD("The Revenant", List.of("Leonardo Wilhelm DiCaprio"));
        Assert.assertFalse(libraryItem1.equals(libraryItem2));
        DVD d1 = new DVD("Titanic", List.of("Leonardo Wilhelm DiCaprio"));
        DVD d2 = new DVD("The Revenant", List.of("Leonardo Wilhelm DiCaprio"));
        Assert.assertFalse(d1.equals(d2));
        DVD d11 = new DVD("Titanic", List.of("Leonardo Wilhelm DiCaprio", "Kate Elizabeth Winslet"));
        DVD d21 = new DVD("Titanic", List.of("Leonardo Wilhelm DiCaprio"));
        Assert.assertFalse(d11.equals(d21));
        Magazine m1 = new Magazine("Times", 433, "February 2022");
        Magazine m2 = new Magazine("Time", 433, "February 2022");
        Assert.assertFalse(m1.equals(m2));
        DigitalBook db1 = new DigitalBook("Hillbilly Elegy", "J.D. Vance", 233);
        DigitalBook db2 = new DigitalBook("Hillbilly Elegy", "J.D. Vance", 263);
        Assert.assertFalse(db1.equals(db2));
        PrintBook pb1 = new PrintBook("Caste", "Isabel Wilkerson", 497);
        PrintBook pb2 = new PrintBook("Caste", "Isabel Wilkerson", 362);
        Assert.assertFalse(pb1.equals(pb2));
        AudioBook ab1 = new AudioBook("Hillbilly", "J.D. Vance", 5.7);
        AudioBook ab2 = new AudioBook("Hillbilly Elegy", "J.D. Vance", 5.7);
        Assert.assertFalse(ab1.equals(ab2));
    }
    /**
     * Testcase for Problem 7-10
     * <p>
     * Test the following:
     * Loan period for all concrete classes.
     * <p>
     * 5 pts
     */
    @Test
    public void problem7to10_checkLoanPeriod_5pts() {
        DVD d = new DVD("Titanic", List.of("Leonardo Wilhelm DiCaprio", "Kate Elizabeth Winslet"));
        Assert.assertEquals("7 days", d.checkOut());
        Magazine m = new Magazine("Time", 433, "February 2022");
        Assert.assertEquals("7 days", m.checkOut());
        DigitalBook db = new DigitalBook("Hillbilly Elegy", "J.D. Vance", 263);
        Assert.assertEquals("14 days", db.checkOut());
        PrintBook pb = new PrintBook("Caste", "Isabel Wilkerson", 497);
        Assert.assertEquals("21 days", pb.checkOut());
        AudioBook ab = new AudioBook("Hillbilly Elegy", "J.D.  Vance", 5.7);
        Assert.assertEquals("28 days", ab.checkOut());
    }
    /**
     * Testcase for Problem 7-10
     * <p>
     * Test the following:
     * Max checkout number is correct
     * <p>
     * 5 pts
     */
    @Test
    public void problem7to10_maxCheckoutNumber_5pts() {
        DVD d = new DVD("Titanic(new)", List.of("Leonardo Wilhelm DiCaprio", "Kate Elizabeth Winslet"));
                Assert.assertEquals("7 days", d.checkOut());
        Assert.assertEquals("NOT ALLOWED", d.checkOut());
        Magazine m = new Magazine("Time(new)", 233, "February 2022");
        Assert.assertEquals("7 days", m.checkOut());
        Assert.assertEquals("NOT ALLOWED", m.checkOut());
        DigitalBook db = new DigitalBook("Hillbilly Elegy(new)", "J.D. Vance",
                263);
        Assert.assertEquals("14 days", db.checkOut());
        Assert.assertEquals("14 days", db.checkOut());
        Assert.assertEquals("14 days", db.checkOut());
        Assert.assertEquals("NOT ALLOWED", db.checkOut());
        PrintBook pb = new PrintBook("Caste(new)", "Isabel Wilkerson", 497);
        Assert.assertEquals("21 days", pb.checkOut());
        Assert.assertEquals("NOT ALLOWED", pb.checkOut());
        AudioBook ab = new AudioBook("Hillbilly Elegy(new)", "J.D.  Vance", 5.7);
        Assert.assertEquals("28 days", ab.checkOut());
        Assert.assertEquals("28 days", ab.checkOut());
        Assert.assertEquals("NOT ALLOWED", ab.checkOut());
    }
    /**
     * Testcase for Problem 7-10
     * <p>
     * Test the following:
     * Checkin function
     * <p>
     * 5 pts
     */
    @Test
    public void problem7to10_maxCheckin_5pts() {
        DVD d = new DVD("Titanic(new)", List.of("Leonardo Wilhelm DiCaprio", "Kate Elizabeth Winslet"));
                Assert.assertEquals("7 days", d.checkOut());
        Assert.assertEquals("NOT ALLOWED", d.checkOut());
        d.checkIn();
        Assert.assertEquals("7 days", d.checkOut());
        Magazine m = new Magazine("Time(new)", 233, "February 2022");
        Assert.assertEquals("7 days", m.checkOut());
        Assert.assertEquals("NOT ALLOWED", m.checkOut());
        m.checkIn();
        Assert.assertEquals("7 days", m.checkOut());
        DigitalBook db = new DigitalBook("Hillbilly Elegy(new)", "J.D. Vance",
                263);
        Assert.assertEquals("14 days", db.checkOut());
        Assert.assertEquals("14 days", db.checkOut());
        Assert.assertEquals("14 days", db.checkOut());
        Assert.assertEquals("NOT ALLOWED", db.checkOut());
        db.checkIn();
        Assert.assertEquals("14 days", db.checkOut());
        PrintBook pb = new PrintBook("Caste(new)", "Isabel Wilkerson", 497);
        Assert.assertEquals("21 days", pb.checkOut());
        Assert.assertEquals("NOT ALLOWED", pb.checkOut());
        pb.checkIn();
        Assert.assertEquals("21 days", pb.checkOut());
        AudioBook ab = new AudioBook("Hillbilly Elegy(new)", "J.D.  Vance", 5.7);
        Assert.assertEquals("28 days", ab.checkOut());
        Assert.assertEquals("28 days", ab.checkOut());
        Assert.assertEquals("NOT ALLOWED", ab.checkOut());
        ab.checkIn();
        Assert.assertEquals("28 days", ab.checkOut());
    }
}