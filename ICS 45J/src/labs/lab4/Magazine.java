package labs.lab4;

/**
 * A magazine item in a library
 */
public class Magazine extends LibraryItem {
	// ADD YOUR INSTANCE VARIABLES HERE
	private int issue_num;
	private String pub_date;
	/**
	 * Constructor
	 * 
	 * @param title				magazine title
	 * @param issueNumber		magazine issue number
	 * @param publicationDate	magazine publication date
	 */
	public Magazine(String title, int issueNumber, String publicationDate) {
		super(title);
		issue_num = issueNumber;
		pub_date = publicationDate;
	}
	
	
	public int getIssueNumber() {
		return issue_num;
	}
	
	
	public void setIssueNumber(int issueNumber) {
		issue_num = issueNumber;
	}

	
	public String getPublicationDate() {
		return pub_date;
	}

	
	public void setPublicationDate(String publicationDate) {
		pub_date = publicationDate;
	}
	
	
	/**
	 * If this item is not already checked out, this method checks this item out and 
	 * returns the loan period; if it is already checked out, returns the String 
	 * "NOT ALLOWED"; overrides LibraryItem.checkOut()
	 */
	@Override
	public String checkOut() {
		if(isCheckedOut())
			return "NOT ALLOWED";
		else
		{
			setCheckedOut(true);
			return "7 days";
		}
	}
	
	
	/**
	 * Returns true if the parameter object is a Magazine that has the same instance 
	 * variable value(s) as this one
	 */
	public boolean equals(Object otherObject) {
		if(this.getClass() != otherObject.getClass())
			return false;
		Magazine other = (Magazine) otherObject;
		if (super.equals(other) && this.issue_num == other.issue_num && this.pub_date == other.pub_date)
			return true;
		return false;
	}
}