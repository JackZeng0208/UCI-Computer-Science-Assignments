package labs.lab4;

/**
 * Print book item in a library
 */
public class PrintBook extends Book {
	// ADD YOUR INSTANCE VARIABLES HERE
	private int page_num;
	/**
	 * Constructor
	 * 
	 * @param title		book title
	 * @param author	book author
	 * @param numPages	number of pages
	 */
	public PrintBook(String title, String author, int numPages) {
		super(title, author);
		this.page_num = numPages;
	}

	
	public int getNumPages() {
		return this.page_num;
	}

	
	public void setNumPages(int numPages) {
		page_num = numPages;
	}

	
	/**
	 * If this item is not already checked out, this method checks this item out
	 * and returns the loan period; if it is already checked out, returns the
	 * String "NOT ALLOWED"; overrides LibraryItem.checkOut()
	 */
	@Override
	public String checkOut() {
		if(isCheckedOut())
		{
			return "NOT ALLOWED";
		}
		else
		{
			setCheckedOut(true);
			return "21 days";
		}
	}

	
	/**
	 * Returns true if the parameter object is a PrintBook that has the same
	 * instance variable value(s) as this one
	 */
	public boolean equals(Object otherObject) {
		if(this.getClass() != otherObject.getClass())
			return false;
		PrintBook other = (PrintBook) otherObject;
		if(super.equals(other) && this.page_num == other.page_num)
			return true;
		return false;
	}
}