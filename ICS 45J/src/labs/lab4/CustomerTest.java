package labs.lab4;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

	@Test
	public void testPurchaseLessThan100() {
		Customer c = new Customer();
		c.makePurchase(1.25);
		assertEquals(1.25, c.getTotalAmountSpent(), 1e-13);
		assertEquals(0, c.getNumDiscountsToUse());
		c.makePurchase(32.1);
		assertEquals(33.35, c.getTotalAmountSpent(), 1e-13);
		assertEquals(0, c.getNumDiscountsToUse());
	}
	
	@Test
	public void testPurchaseMoreThan100() {
		Customer c = new Customer();
		c.makePurchase(35);
		assertEquals(35, c.getTotalAmountSpent(), 1e-13);
		assertEquals(0, c.getNumDiscountsToUse());
		c.makePurchase(95.5);
		assertEquals(130.5, c.getTotalAmountSpent(), 1e-13);
		assertEquals(1, c.getNumDiscountsToUse());
	}
	
	@Test
	public void testDiscountApplied() {
		Customer c = new Customer();
		c.makePurchase(101);
		assertEquals(1, c.getNumDiscountsToUse());
		c.makePurchase(15.5);
		assertEquals(114.95, c.getTotalAmountSpent(), 1e-13);
		assertEquals(0, c.getNumDiscountsToUse());
	}
	
	@Test
	public void testPurchaseEarningMultipleDiscounts() {
		Customer c = new Customer();
		c.makePurchase(23.51);
		assertEquals(23.51, c.getTotalAmountSpent(), 1e-13);
		assertEquals(0, c.getNumDiscountsToUse());
		c.makePurchase(80.29);
		assertEquals(103.8, c.getTotalAmountSpent(), 1e-13);
		assertEquals(1, c.getNumDiscountsToUse());
		c.makePurchase(114);
		assertEquals(206.4, c.getTotalAmountSpent(), 1e-13);
		assertEquals(1, c.getNumDiscountsToUse());
		c.makePurchase(95);
		assertEquals(669.1719, c.getTotalAmountSpent(), 1e-13);
		assertEquals(4, c.getNumDiscountsToUse());
	}
	
	@Test
	public void testOnlyOneDiscountApplied() {
		Customer c = new Customer();
		c.makePurchase(1919);
		assertEquals(19, c.getNumDiscountsToUse());
		c.makePurchase(11.4);
		assertEquals(1929.26, c.getTotalAmountSpent(), 1e-13);
		assertEquals(18, c.getNumDiscountsToUse());
	}
}
