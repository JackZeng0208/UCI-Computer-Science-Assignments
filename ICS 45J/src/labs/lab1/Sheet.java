package labs.lab1;

/**
 * Compute international standard sizes for paper, where A0 = 841 x 1189 mm A1 =
 * 594 x 841 mm A2 = 420 x 594 mm A3 = 292 x 423 mm etc
 */
public class Sheet {

	private int width;
	private int length;
	private int size;
	
	/**
	 * create a sheet of size A0
	 */
	public Sheet() {
		width = 841;
		length = 1189;
		size = 0;
	}

	public Sheet(int w, int l, int s)
	{
		this.width = w;
		this.length = l;
		this.size = s;
	}
	
	/**
	 * @return the width of the paper
	 */
	public int getWidth() {
		return width;
	}

	
	/**
	 * @return the length of the paper
	 */
	public int getLength() {
		return length;
	}

	
	/**
	 * @return the ISO name for the paper
	 */
	public String getName() {
		return "A"+size;
	}

	
	/**
	 *
	 * @return a sheet that is cut in half along the length
	 */
	public Sheet cutInHalf() {
		int new_size = size + 1;
		int new_length = width;
		int new_width = length/2;
		return new Sheet(new_width,new_length,new_size);
	}
}
