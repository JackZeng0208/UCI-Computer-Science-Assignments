package labs.lab1;

/**
 * An employee with a name and salary.
 */
public class Employee {
	private String name;
	private double salary;

	/**
	 * Constructs an employee.
	 * 
	 * @param employeeName  the employee name
	 * @param currentSalary the employee salary
	 */
	public Employee(String employeeName, double currentSalary) {
		this.name = employeeName;
		this.salary = currentSalary;
	}

	/**
	 * Gets the employee name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the employee salary.
	 * 
	 * @return the salary
	 */
	public double getSalary() {
		return this.salary;
	}

	/**
	 * Raises the salary by a given percentage.
	 * 
	 * @param percent the percentage of the raise
	 */
	public void raiseSalary(double percent) {
		this.salary += this.salary * percent * 0.01;
	}
}
