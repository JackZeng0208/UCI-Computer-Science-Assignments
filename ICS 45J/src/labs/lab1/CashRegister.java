package labs.lab1;

/**
 * A cash register totals up sales and computes change due.
 */
public class CashRegister {
	private double purchase;
	private double payment;

	private double total_purchase;
	private int purchase_count;
	private int sale_count;
	private String purchase_detail;

	/**
	 * Constructs a cash register with no money in it.
	 */
	public CashRegister() {
		purchase = 0;
		payment = 0;
		purchase_count = 0;
		sale_count = 0;
		purchase_detail = "";
		total_purchase = 0.0;
	}

	/**
	 * Records the sale of an item.
	 * 
	 * @param amount the price of the item
	 */
	public void recordPurchase(double amount) {
		purchase = purchase + amount;
		purchase_count += 1;
		purchase_detail += amount + "\n";
	}

	/**
	 * Processes a payment received from the customer.
	 * 
	 * @param amount the amount of the payment
	 */
	public void receivePayment(double amount) {
		payment = payment + amount;
	}

	/**
	 * Computes the change due and resets the machine for the next customer.
	 * 
	 * @return the change due to the customer
	 */
	public double giveChange() {
		double change = payment - purchase;
		total_purchase += purchase;
		purchase = 0;
		payment = 0;
		purchase_count = 0;
		sale_count += 1;
		return change;
	}
	

	/**
	 * Returns count of items purchased.
	 * 
	 * @return count of items purchased
	 */
	public int getItemCountInPurchase() {
		return this.purchase_count;
	}

	/**
	 * Get the total amount of all sales for the day.
	 * 
	 * @return the total amount of all sales for the day
	 */
	public double getSalesTotal() {
		return total_purchase;
	}

	/**
	 * Get the total number of sales for the day.
	 * 
	 * @return the number of sales for the day
	 */
	public int getSalesCount() {
		return sale_count;
	}

	/**
	 * Reset counters and totals for the next day's sales.
	 */
	public void reset() {
		purchase = 0;
		payment = 0;
		purchase_count = 0;
		sale_count = 0;
		purchase_detail = "";
		total_purchase = 0;
	}

	public String getReceipt() {
		return purchase_detail + purchase;
	}
}
