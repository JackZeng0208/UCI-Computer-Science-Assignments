package labs.lab7;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

/**
 * Sorts a file of names alphabetically, ignoring case
 */
public class NameSorter {
	private ArrayList<full_name> all_names;
	private String file_name;
	private class full_name implements Comparable<full_name>
	{
		private String first_name;
		private String last_name;

		public full_name(String f, String l)
		{
			this.first_name = f;
			this.last_name = l;
		}
		@Override
		public int compareTo(full_name other) {
			int last_name_result = last_name.compareToIgnoreCase(other.last_name);
			if (last_name_result == 0)
				return first_name.compareToIgnoreCase(other.first_name);
			else
				return last_name_result;
		}
	}
	/**
	 * Constructs a name sorter with the names from the input file
	 * 
	 * @param inputFile	name of the input file
	 */
	public NameSorter(String inputFile) {
		try
		{
			File input_file = new File(inputFile);
			this.file_name = inputFile;
			Scanner in = new Scanner(input_file);
			all_names = new ArrayList<>();
			while(in.hasNextLine())
			{
				String line = in.nextLine();
				String[] temp = line.split(", ");
				all_names.add(new full_name(temp[1], temp[0]));
			}
			in.close();
		} catch (FileNotFoundException e)
		{
			System.out.print("File: " + file_name + " not found");
		}

	}

	/**
	 * Sorts the names from the input file alphabetically, ignoring case, 
	 * then writes the sorted names back to the file, overwriting the 
	 * previous content
	 */
	public void sortNames() {
		try
		{
			Collections.sort(all_names);
			File input_file = new File(file_name);
			String result = "";
			for(full_name i : all_names)
				result += i.last_name + ", " + i.first_name + "\n";
			result = result.trim();
			PrintWriter out = new PrintWriter(input_file);
			out.print(result);
			out.close();
		} catch (FileNotFoundException e)
		{
			System.out.print("File: " + file_name + " not found");
		}

	}


}