package labs.lab7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * A program that reads text from a file and breaks it up into individual words,
 * inserts the words into a tree set, and allows you to get stats about the
 * words.
 */
public class WordCounter {
	private TreeSet<String> uniqueWords;
	// FILL IN ANY OTHER PRIVATE INSTANCE VARIABLES YOU NEED HERE
	private ArrayList<String> all_words;
	private String file_name;
	private int word_num;
	private int unique_word_num;
	/**
	 * Constructor
	 * 
	 * @param filename file from which to read words
	 */
	public WordCounter(String filename) {
		try
		{
			uniqueWords = new TreeSet<>();
			all_words = new ArrayList<>();
			this.file_name = filename;
			File input_file = new File(file_name);
			Scanner in = new Scanner (input_file);
			while (in.hasNextLine())
			{
				String line = in.nextLine();
				String[] result = line.replaceAll("[^a-zA-z0-9 ]", "").toLowerCase().trim().split(" ");
				all_words.addAll(Arrays.asList(result));
				all_words.removeAll(Arrays.asList("",null));
			}
			uniqueWords.addAll(all_words);
			unique_word_num = uniqueWords.size();
			word_num = all_words.size();
		}catch (FileNotFoundException e)
		{
			System.out.print("File: " + filename + " not found");
		}
	}


	/**
	 * Returns the number of unique words in the file
	 * 
	 * @return number of unique words
	 */
	public int getNumUniqueWords() {
		 return unique_word_num;
	}


	/**
	 * Returns the number of words in the file
	 * 
	 * @return number of words
	 */
	public int getNumWords() {
		return word_num;
	}


	/**
	 * returns a list of the unique words with all non-letter and non-digit
	 * characters removed, all in lower case, as a List in alphabetical order
	 * 
	 * @return list of unique words
	 */
	public List<String> getUniqueWords() {
		 List<String> result = new ArrayList<>(uniqueWords);
		 return result;
	}
}
