package labs.lab7;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Class for simulating trading a single stock at varying prices.
 */
public class StockSimulator {

	private Queue<Block> blocks;
	private int total_quantity;
	/**
	 * Constructor
	 */
	public StockSimulator() {
		blocks = new LinkedList<>();
		total_quantity = 0;
	}


	/**
	 * Handle a user buying a given quantity of stock at a given price.
	 * 
	 * @param quantity how many to buy.
	 * @param price    the price to buy at.
	 * 
	 * @throws IllegalArgumentException if the requested quantity/price cannot be
	 *                                  sold i.e., quantity <= 0 or price < 0
	 */
	public void buy(int quantity, int price) throws IllegalArgumentException {
		if (quantity <= 0 || price < 0)
			throw new IllegalArgumentException("Unable to complete purchase");
		blocks.add(new Block(quantity, price));
		total_quantity += quantity;
	}


	/**
	 * Handle a user selling a given quantity of stock at a given price.
	 * 
	 * @param quantity how many to sell.
	 * @param price    the price to sell.
	 * 
	 * @return the gain (can be positive or negative)
	 * 
	 * @throws IllegalArgumentException if the requested quantity cannot be sold
	 *                                  e.g., quantity exceeds quantity owned,
	 *                                  quantity < 1, price < 0
	 */
	public int sell(int quantity, int price) throws IllegalArgumentException {
		int revenue = 0;
		int temp = quantity;
		if (quantity > total_quantity)
			throw new IllegalArgumentException("Unable to complete sale");
		if (quantity < 1 || price < 0)
			throw new IllegalArgumentException("Unable to complete sale");
		while (blocks.size() > 0)
		{
			int cur_q = blocks.peek().getQuantity();
			int cur_p = blocks.peek().getPrice();
			if (temp - cur_q > 0)
			{
				temp -= cur_q;
				revenue += cur_q * (price - cur_p);
				total_quantity -= cur_q;
				blocks.remove();
			}
			else if (temp - cur_q == 0)
			{
				temp = 0;
				revenue += cur_q * (price - cur_p);
				total_quantity -= cur_q;
				blocks.remove();
				break;
			}
			else
			{
				int remaining = Math.abs(temp - cur_q);
				revenue += (cur_q - remaining) * (price - cur_p);
				total_quantity -= (cur_q - remaining);
				break;
			}
		}
		return revenue;
	}


	/**
	 * This is a method for us to test your class
	 * 
	 * @return a List of Blocks owned
	 */
	public List<Block> getBlocks() {
		return (List) blocks;
	}

}
