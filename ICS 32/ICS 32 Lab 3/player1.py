from gameboard import BoardClass
import socket
import time

try:
    server_ip_address = input("Please input the IP address for player 2: ")
    server_port = int(input("Please input the the port for player 2: "))
    player1_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    player1_socket.connect((server_ip_address, server_port))
except socket.error or ConnectionRefusedError:
    print("Connection may have error, do you want to try again?")
    user_decision = input("Type 'y\\Y' for \"Yes\", 'n\\N' for \"No\" ")
    if user_decision == 'Y' or user_decision == 'y':
        time.sleep(1)
        server_ip_address = input("Please input the IP address for player 2: ")
        server_port = int(input("Please input the the port for player 2: "))
        player1_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        player1_socket.connect((server_ip_address, server_port))
    if user_decision == 'N' or user_decision == 'n':
        player1_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        player1_socket.close()
        print("Thank you so much for playing this game!")


def winner_check() -> bool:
    """Check who is the winner of a match. Send further decisions to player2.

    Returns:
        True for continuing games, False for no winner (winner hasn't existed yet or tied), exit() if player1 wants to end the game
    """
    if player1_gameboard.isWinner():
        player1_gameboard.game_number += 1
        player1_gameboard.resetGameBoard()
        print("Do you want to play again?")
        player1_decision = input(
            "Type 'y'\\'Y' in order to play again. Type 'n'\\'N' to quit: ")
        if player1_decision.upper() == 'Y':
            message = "Play Again"
            player1_socket.send(message.encode())
            return True
        if player1_decision.upper() == 'N':
            message = "Fun Times"
            player1_socket.send(message.encode())
            player1_socket.close()
            player1_gameboard.printStats()
            exit()
    return False


def tie_check() -> bool:
    """Determine the game is tie or not. Send further decisions to player2.

    Returns:
        True for tied game, False for no tie (tie game hasn't existed yet or tied), exit() if player1 wants to end the game
    """
    if player1_gameboard.boardIsFull():
        player1_gameboard.game_number += 1
        player1_gameboard.resetGameBoard()
        player1_gameboard.tie_number += 1
        print("Do you want to play again?")
        player1_decision = input(
            "Type 'y'\\'Y' in order to play again. Type 'n'\\'N' to quit: ")
        if player1_decision.upper() == 'Y':
            message = "Play Again"
            player1_socket.send(message.encode())
            return True
        if player1_decision.upper() == 'N':
            message = "Fun Times"
            player1_socket.send(message.encode())
            player1_socket.close()
            player1_gameboard.printStats()
            exit()
    return False


# Establish connection
player1_gameboard = BoardClass("1")
player1_gameboard.user_name = input("Please input your username: ")
player1_socket.send(player1_gameboard.user_name.encode())
player2_username = player1_socket.recv(1024)
player2_username = player2_username.decode()
player1_gameboard.player_type = '1'

while True:
    winner_check()

    # Send the data to player 2
    print("Here are the number of each square on the board:")
    printout_string = '''| 1 | 2 | 3 |
| 4 | 5 | 6 |
| 7 | 8 | 9 |'''
    print(printout_string)
    player1_input_location = input(
        "Please input where do you want to put your 'x' on: ")
    player1_action = input("Please input your action ('x' or 'X'): ")
    if player1_action.upper() == "X":
        player1_gameboard.updateGameBoard(player1_input_location, player1_action)
        player1_socket.send(player1_input_location.encode())
        player1_socket.send(player1_action.encode())
        player1_gameboard.last_turn_player_name = player1_gameboard.user_name
    else:
        print("Invalid input")
        error_message = "error"
        player1_socket.send(error_message.encode())

    player1_gameboard.print_current_board()

    if winner_check():
        continue
    if tie_check():
        continue

    # Receive the data from player 2
    player2_input_location = player1_socket.recv(1024).decode()
    if player2_input_location != 'error':
        player2_action = player1_socket.recv(1024).decode()
        player1_gameboard.updateGameBoard(player2_input_location, player2_action)
        player1_gameboard.last_turn_player_name = player2_username

    player1_gameboard.print_current_board()

    if winner_check():
        continue
    if tie_check():
        continue
