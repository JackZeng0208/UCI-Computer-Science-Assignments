class BoardClass:
    """A class to simulate Tic-tac-toe game

    Attributes:
        player_type: "1" means player1, "2" means player2. This is used for counting losing and winning numbers for player1 and player2.
        user_name: The user name of player
        last_turn_player_name: User name of the last player to have a turn
        win_number: Number of wins
        tie_number: Number of ties
        loss_number: Number of losses
        game_number: Number of matches
        game_board: A list which updates and stores live game board information
    """
    def __init__(self, which_player: str):
        """Create a Tic-tac-toe game board and necessary game information

        Args:
            which_player: "1" means player1, "2" means player2
        """
        self.player_type = which_player
        self.user_name: str = ''
        self.last_turn_player_name: str = ''
        self.win_number: int = 0
        self.tie_number: int = 0
        self.loss_number: int = 0
        self.game_number: int = 0
        self.game_board = ['-' for i in range(10)]

    def resetGameBoard(self) -> None:
        """Set an empty game board

        Returns:
            Reset the list with all '-' element ('-' represents empty)
        """
        self.game_board = ['-' for i in range(10)]

    def updateGameBoard(self, location: str, action: str):
        """Update game board

        Args:
            location:
                Str from '1' to '9', location of a specific space in the grid:
                | 1 | 2 | 3 |
                | 4 | 5 | 6 |
                | 7 | 8 | 9 |
                Ex: game_board[1] -> grid #1
            action:
                Marks from either player1 ('x' or 'X') and player2 ('o' and 'O')

        Returns:
            Input the marks to the game_board[] list. Uppercase all of them in order to judge the game result easier.
        """
        if self.game_board[int(location)] == '-':
            self.game_board[int(location)] = action.upper()
        else:
            print("Invalid input")

    def boardIsFull(self) -> bool:
        """Judge whether the game board is full or not

        Returns:
            True if game board is full, False otherwise.
        """
        if "-" not in self.game_board[1:10]:
            return True
        else:
            return False

    def isWinner(self) -> bool:
        """Judge and update wins and losses numbers

        Returns:
            True if there's winner, False otherwise
        """
        _all_possible_lines = [
            self.game_board[1:4], self.game_board[4:7], self.game_board[7:10],
            self.game_board[1::3], self.game_board[2::3],
            self.game_board[3::3], self.game_board[1::4],
            self.game_board[3:8:2]
        ]
        _uppercase_three_x = ["X"] * 3
        _uppercase_three_o = ["O"] * 3

        if _uppercase_three_x in _all_possible_lines or _uppercase_three_o in _all_possible_lines:
            if _uppercase_three_x in _all_possible_lines:
                if self.player_type == "1":
                    self.win_number += 1
                if self.player_type == "2":
                    self.loss_number += 1
            if _uppercase_three_o in _all_possible_lines:
                if self.player_type == "1":
                    self.loss_number += 1
                if self.player_type == "2":
                    self.win_number += 1
            return True
        return False

    def print_current_board(self):
        """Print current game board status"""
        print("Here's the current game board:")
        current_board_first_row = "| " + self.game_board[
            1] + " | " + self.game_board[2] + " | " + self.game_board[3] + " |"
        current_board_second_row = "| " + self.game_board[
            4] + " | " + self.game_board[5] + " | " + self.game_board[6] + " |"
        current_board_third_row = "| " + self.game_board[
            7] + " | " + self.game_board[8] + " | " + self.game_board[9] + " |"
        print(current_board_first_row)
        print(current_board_second_row)
        print(current_board_third_row + '\n')

    def printStats(self) -> None:
        """Print all game information at the end of the game

        Returns:
            Players user name
            User name of the last person to make a move
            Number of games
            Number of wins
            Number of losses
            Number of ties
        """
        print("Player's username: " + self.user_name)
        print("Name of last person to make a move: " +
              self.last_turn_player_name)
        print("Number of games: " + str(self.game_number))
        print("Number of wins: " + str(self.win_number))
        print("Number of losses: " + str(self.loss_number))
        print("Number of ties: " + str(self.tie_number))
