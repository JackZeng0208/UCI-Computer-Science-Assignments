import socket
from gameboard import BoardClass

player2_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = input("Please input the IP address of this host: ")
server_port = input("Please input the port of your host: ")
player2_socket.bind((server_address, int(server_port)))
player2_socket.listen(10)
player2_gameboard = BoardClass("2")
player1_socket, player1_address = player2_socket.accept()

player1_username = player1_socket.recv(1024).decode()
player1_socket.send(b'player2')
player2_gameboard.user_name = 'player2'


def winner_check() -> bool:
    """Check who is the winner of a match. Receive further decisions from player1.

    Returns:
        True for continuing games, False for no winner (winner hasn't existed yet or tied), exit() if player1 wants to end the game
    """
    if player2_gameboard.isWinner():
        player2_gameboard.game_number += 1
        player1_decision = player1_socket.recv(1024).decode()
        if player1_decision == "Play Again":
            player2_gameboard.resetGameBoard()
            return True
        if player1_decision == "Fun Times":
            player2_gameboard.printStats()
            player1_socket.close()
            player2_socket.close()
        exit()
    return False


def tie_check() -> bool:
    """Determine the game is tie or not. Receive further decisions from player1.

    Returns:
        True for tied game, False for no tie (tie game hasn't existed yet or tied), exit() if player1 wants to end the game
    """
    if player2_gameboard.boardIsFull():
        player2_gameboard.tie_number += 1
        player2_gameboard.game_number += 1
        player1_decision = player1_socket.recv(1024).decode()
        if player1_decision == "Play Again":
            player2_gameboard.resetGameBoard()
            return True
        if player1_decision == "Fun Times":
            player2_gameboard.printStats()
            player1_socket.close()
            player2_socket.close()
        exit()
    return False


while True:
    # Receive data from player 1
    player1_input_location = player1_socket.recv(1024).decode()
    if player1_input_location != 'error':
        player1_action = player1_socket.recv(1024).decode()
        player2_gameboard.updateGameBoard(player1_input_location, player1_action)
        player2_gameboard.last_turn_player_name = player1_username

    player2_gameboard.print_current_board()

    if winner_check():
        continue

    if tie_check():
        continue

    # Send data to player 1
    print("Here are the number of each square on the board:")
    printout_string = '''| 1 | 2 | 3 |
| 4 | 5 | 6 |
| 7 | 8 | 9 |'''
    print(printout_string)
    player2_input_location = input(
        "Please input where do you want to put your 'o' on: ")
    player2_action = input("Please input your action ('o' or 'O'): ")
    if player2_action.upper() == "O":
        player2_gameboard.updateGameBoard(player2_input_location, player2_action)
        player1_socket.send(player2_input_location.encode())
        player1_socket.send(player2_action.encode())
        player2_gameboard.last_turn_player_name = 'player2'
    else:
        print("Invalid input")
        error_message = "error"
        player1_socket.send(error_message.encode())

    player2_gameboard.print_current_board()

    if winner_check():
        continue
    if tie_check():
        continue
