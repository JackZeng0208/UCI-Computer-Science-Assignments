import socket
import threading
from gameboard import BoardClass
import tkinter as tk
from tkinter.constants import *
from tkinter import simpledialog
from tkinter import messagebox

main_canvas = tk.Tk()
server_port = simpledialog.askstring("Port", "Port number:", parent=main_canvas)
server_address = '127.0.0.1'

player2_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
player2_socket.bind((server_address, int(server_port)))
player2_socket.listen(10)

player1_socket = None
player1_address = ''
location = 1
player2_gameboard = None
player2_name = 'player2'
player1_name = 'player1'
is_start = False
user_connection = False

def reset_gameboard():
    button_1["text"] = ' '
    button_2['text'] = ' '
    button_3['text'] = ' '
    button_4['text'] = ' '
    button_5['text'] = ' '
    button_6['text'] = ' '
    button_7['text'] = ' '
    button_8['text'] = ' '
    button_9['text'] = ' '
    main_canvas_right_information_label_turn['text'] = player1_name

def end_game():
    player1_socket.close()
    results = player2_gameboard.computeStats()
    input_game_statistic(results)
    reset_gameboard()
    exit()


def input_game_statistic(game_statistic):
    main_canvas_down_board_loss_number['text'] = game_statistic['loss_number']['player2']
    main_canvas_down_board_tie_number['text'] = game_statistic['tie_number']
    main_canvas_down_board_win_number['text'] = game_statistic['win_number']['player2']
    main_canvas_down_board_game_number['text'] = game_statistic['game_number']


def update_gameboard():
    global location
    location = int(location)
    if location == 9:
        button_9['text'] = 'X'
    elif location == 8:
        button_8['text'] = 'X'
    elif location == 7:
        button_7['text'] = 'X'
    elif location == 6:
        button_6['text'] = 'X'
    elif location == 5:
        button_5['text'] = 'X'
    elif location == 4:
        button_4['text'] = 'X'
    elif location == 3:
        button_3['text'] = 'X'
    elif location == 2:
        button_2['text'] = 'X'
    elif location == 1:
        button_1['text'] = 'X'

def reset_statistic_board():
    main_canvas_down_board_game_number['text'] = ' '
    main_canvas_down_board_win_number['text'] = ' '
    main_canvas_down_board_loss_number['text'] = ' '
    main_canvas_down_board_tie_number['text'] = ' '
    main_canvas_right_information_label_turn['text'] = player1_name


def receive_connection():
    global user_connection, is_start, player1_name, player2_gameboard, player2_name
    while True:
        if is_start:
            if user_connection != True:
                input_name, player1_address = player1_socket.recvfrom(1024)
                input_name = input_name.decode()
                player1_name = input_name
                player2_create_thread(username_define)
                user_connection = True


def receive_choice():
    global is_start, location
    while True:
        if user_connection == True and is_start == True:
            input_information, player1_address = player1_socket.recvfrom(1024)
            input_information = input_information.decode()
            if input_information == 'Play Again':
                reset_gameboard()
            elif input_information == 'Fun Times':
                end_game()
            else:
                location, player2_gameboard.last_turn_player_name = input_information.split('+')
                player2_gameboard.updateGameBoard(player2_gameboard.last_turn_player_name, int(location))
                update_gameboard()
                main_canvas_right_information_label_turn['text'] = player2_name
                if player2_gameboard.isWinner():
                    player2_gameboard.resetGameBoard()
                    player2_gameboard.number_of_games()
                    player2_gameboard.last_turn_player_name = player2_name


def connections():
    global is_start, player1_socket, player1_address
    player1_socket, player1_address = player2_socket.accept()
    player2_answer = messagebox.askyesno("Request From:", str(player1_address))
    if player2_answer:
        player1_socket.send(b'Yes')
        is_start = True
        reset_gameboard()
        reset_statistic_board()
        receive_connection()
    else:
        player1_socket.send(b'No')
        player1_socket.close()


def player2_create_thread(function):
    player2_thread = threading.Thread(target=function)
    player2_thread.start()


def quit_GUI():
    player2_socket.close()
    main_canvas.destroy()


def username_define():
    global player2_name, player2_gameboard
    canvas = tk.Tk()
    canvas.withdraw()
    player2_name = simpledialog.askstring('Player2 name', "Input your name please:", parent=canvas)
    main_canvas_right_information_label_username['text'] = player2_name
    player2_name_encode = player2_name.encode()
    player1_socket.send(player2_name_encode)
    player2_gameboard = BoardClass(player1_name, player2_name)
    canvas.destroy()

player2_create_thread(connections)
player2_create_thread(receive_choice)

def after_clicking_button(button_number):
    button_number_string = str(button_number)
    global player2_gameboard
    if is_start:
        if user_connection:
            if player2_gameboard.last_turn_player_name == player1_name:
                player2_gameboard.last_turn_player_name = player2_name
                change_button(button_number)
                player1_socket.send((button_number_string + '+' + player2_gameboard.last_turn_player_name).encode())
                player2_gameboard.updateGameBoard(player2_gameboard.last_turn_player_name, button_number)
                main_canvas_right_information_label_turn['text'] = player1_name
                if player2_gameboard.isWinner():
                    player2_gameboard.resetGameBoard()
                    player2_gameboard.number_of_games()
                    player2_gameboard.last_turn_player_name = player2_name
                
def check_button(button_number):
    if button_number == 1:
        if button_1['text'] == ' ':
            return True
    elif button_number == 2:
        if button_2['text'] == ' ':
            return True
    elif button_number == 3:
        if button_3['text'] == ' ':
            return True
    elif button_number == 4:
        if button_4['text'] == ' ':
            return True
    elif button_number == 5:
        if button_5['text'] == ' ':
            return True
    elif button_number == 6:
        if button_6['text'] == ' ':
            return True
    elif button_number == 7:
        if button_7['text'] == ' ':
            return True
    elif button_number == 8:
        if button_8['text'] == ' ':
            return True
    elif button_number == 9:
        if button_9['text'] == ' ':
            return True
    else:
        return False

def change_button(button_number):
    if button_number == 1:
        button_1['text'] = 'O'
    elif button_number == 2:
        button_2['text'] = 'O'
    elif button_number == 3:
        button_3['text'] = 'O'
    elif button_number == 4:
        button_4['text'] = 'O'
    elif button_number == 5:
        button_5['text'] = 'O'
    elif button_number == 6:
        button_6['text'] = 'O'
    elif button_number == 7:
        button_7['text'] = 'O'
    elif button_number == 8:
        button_8['text'] = 'O'
    elif button_number == 9:
        button_9['text'] = 'O'

def click_button_1():
    if check_button(1):
        after_clicking_button(1)

def click_button_2():
    if check_button(2):
        after_clicking_button(2)

def click_button_3():
    if check_button(3):
        after_clicking_button(3)

def click_button_4():
    if check_button(4):
        after_clicking_button(4)

def click_button_5():
    if check_button(5):
        after_clicking_button(5)

def click_button_6():
    if check_button(6):
        after_clicking_button(6)

def click_button_7():
    if check_button(7):
        after_clicking_button(7)

def click_button_8():
    if check_button(8):
        after_clicking_button(8)

def click_button_9():
    if check_button(9):
        after_clicking_button(9)

main_canvas.title("tic tac toe player #2")
main_canvas.geometry("700x700")
main_canvas.resizable(1, 1)
main_canvas.configure(background='lightblue')

title_frame = tk.Frame(main_canvas, bg='lightblue', width=1000, height=100)
title_frame.grid(row=0, column=0)

main_canvas_title = tk.Label(title_frame,
                             font=(
                                 'Times New Roman',
                                 30,
                             ),
                             text="Tic-Tac-Toe GUI Game",
                             bg='lightblue',
                             justify='center')
main_canvas_title.grid(row=0, column=0)

main_canvas_board = tk.Frame(main_canvas,
                             relief='groove',
                             bg='lightblue',
                             width=1000,
                             height=600)
main_canvas_board.grid(row=1, column=0)

main_canvas_left_board = tk.Frame(main_canvas_board,
                                  bd=5,
                                  width=600,
                                  height=400,
                                  bg="lightblue",
                                  relief='groove')
main_canvas_left_board.pack(side='left')
main_canvas_right_information = tk.Frame(main_canvas_board,
                                         bd=5,
                                         width=200,
                                         height=400,
                                         bg='lightblue',
                                         relief='groove')
main_canvas_right_information.pack(side='left')

main_canvas_down_board = tk.Frame(main_canvas, bd=5,
                                  relief='groove',
                                  bg='lightblue',
                                  width=1000,
                                  height=100)
main_canvas_down_board.grid(row=2, column=0)

button_1 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_1)
button_2 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_2)
button_3 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_3)
button_4 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_4)
button_5 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_5)
button_6 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_6)
button_7 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_7)
button_8 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_8)
button_9 = tk.Button(main_canvas_left_board, text=' ', fg='black', bg='lightblue', width=7, height=3,
                       font=('Times New Roman', 23), command=click_button_9)

button_1.grid(column=1, row=1, sticky=N + E + W + S)
button_2.grid(column=1, row=2, sticky=N + E + W + S)
button_3.grid(column=1, row=3, sticky=N + E + W + S)
button_4.grid(column=2, row=1, sticky=N + E + W + S)
button_5.grid(column=2, row=2, sticky=N + E + W + S)
button_6.grid(column=2, row=3, sticky=N + E + W + S)
button_7.grid(column=3, row=1, sticky=N + E + W + S)
button_8.grid(column=3, row=2, sticky=N + E + W + S)
button_9.grid(column=3, row=3, sticky=N + E + W + S)

main_canvas_right_information_label_1 = tk.Label(main_canvas_right_information, text='Host name:', bg='lightblue',
                                                 font=('Times New Roman', 25), width=10, height=4).grid(row=0, column=0)
main_canvas_right_information_label_username = tk.Label(main_canvas_right_information, text=player2_name,
                                                        bg='lightblue', font=('Times New Roman', 25), width=10,
                                                        height=4)
main_canvas_right_information_label_username.grid(row=0, column=1)

main_canvas_right_information_label_2 = tk.Label(main_canvas_right_information, text='Whose turn:', bg='lightblue',
                                                 font=('Times New Roman', 25), width=10, height=4).grid(row=1, column=0)
main_canvas_right_information_label_turn = tk.Label(main_canvas_right_information, text=player1_name, bg='lightblue',
                                                    font=('Times New Roman', 25), width=10, height=4)
main_canvas_right_information_label_turn.grid(row=1, column=1)

main_canvas_down_board_game_number_label = tk.Label(main_canvas_down_board, text="Game number:", bg='lightblue',
                                                    font=('Times New Roman', 25), width=15, height=1).grid(row=0,
                                                                                                           column=0,
                                                                                                           sticky=W)
main_canvas_down_board_game_number = tk.Label(main_canvas_down_board, text=" ", bg='lightblue',
                                              font=('Times New Roman', 25), width=20, height=2)
main_canvas_down_board_game_number.grid(row=0, column=1, sticky=W)

main_canvas_down_board_win_number_label = tk.Label(main_canvas_down_board, text="Won:", bg='lightblue',
                                                   font=('Times New Roman', 25), width=10, height=1).grid(row=1,
                                                                                                          column=0,
                                                                                                          sticky=W)
main_canvas_down_board_win_number = tk.Label(main_canvas_down_board, text=" ", bg='lightblue',
                                             font=('Times New Roman', 25), width=20, height=1)
main_canvas_down_board_win_number.grid(row=1, column=1, sticky=W)

main_canvas_down_board_loss_number_label = tk.Label(main_canvas_down_board, text="Lost:", bg='lightblue',
                                                    font=('Times New Roman', 25), width=10, height=1).grid(row=2,
                                                                                                           column=0,
                                                                                                           sticky=W)
main_canvas_down_board_loss_number = tk.Label(main_canvas_down_board, text=" ", bg='lightblue',
                                              font=('Times New Roman', 25), width=20, height=1)
main_canvas_down_board_loss_number.grid(row=2, column=1, sticky=W)

main_canvas_down_board_tie_number_label = tk.Label(main_canvas_down_board, text="Ties:", bg='lightblue',
                                                   font=('Times New Roman', 25), width=10, height=1).grid(row=3,
                                                                                                          column=0,
                                                                                                          sticky=W)
main_canvas_down_board_tie_number = tk.Label(main_canvas_down_board, text=" ", bg='lightblue',
                                             font=('Times New Roman', 25), width=10, height=1)
main_canvas_down_board_tie_number.grid(row=3, column=1, sticky=W)

quit_button = tk.Button(main_canvas_right_information, text='Quit', font=('Times New Roman', 20, 'bold'), height=2,
                        width=5, command=quit_GUI).grid(row=2, column=0)

main_canvas.mainloop()
