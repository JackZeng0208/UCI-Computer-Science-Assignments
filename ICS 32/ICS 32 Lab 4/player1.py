import socket
import threading
from gameboard import BoardClass
import tkinter as tk
from tkinter.constants import *
from tkinter import simpledialog
from tkinter import messagebox

player1_socket = None

location = 1
is_connection = False
is_close = False
player1_gameboard = None
player2_name = 'player2'
player1_name = 'player1'
is_start = False
user_connection = False
game_finish_flag = False

server_address = ''
server_port = ''

def receive_choice():
    global is_start, user_connection, location
    while True:
        if is_start == True and user_connection == True and is_connection == True:
            try:
                name, address = player1_socket.recvfrom(1024)
                name = name.decode()
                if name != '':
                    location, player1_gameboard.last_turn_player_name = name.split('+')
                    player1_gameboard.updateGameBoard(player1_gameboard.last_turn_player_name, int(location))
                    main_canvas_right_information_label_turn['text'] = player1_name
                    update_gameboard()
                    check_game()
            except:
                main_canvas_right_information_label_turn['text'] = player1_name
                pass

def empty_statistic():
    main_canvas_down_board_win_number['text'] = ' '
    main_canvas_down_board_loss_number['text'] = ' '
    main_canvas_down_board_tie_number['text'] = ' '
    main_canvas_down_board_game_number['text'] = ' '

def receive_connection():
    global user_connection, is_start, player1_name, is_close, is_connection, player1_gameboard, player2_name, player1_socket
    while True:
        if is_connection:
            if (user_connection != True) or (is_start != True):
                name, address = player1_socket.recvfrom(1024)
                name = name.decode()
                if name == 'Yes':
                    player1_name = simpledialog.askstring('Input Box', "Enter your name: ", parent=main_canvas)
                    player1_socket.send(player1_name.encode())
                    main_canvas_right_information_label_username['text'] = player1_name
                    is_close = False
                    is_start = True
                elif name == 'No':
                    player1_socket.close()
                    is_connection = True
                else:
                    user_connection = True
                    player2_name = name
                    player1_gameboard = BoardClass(player1_name, player2_name)
                    player1_gameboard.last_turn_player_name = player2_name
                    break

def establish_connection():
    global server_port, server_address, player1_socket, is_connection
    if is_connection != True:
        server_address = simpledialog.askstring('Input Box', 'Host address: ', parent=main_canvas)
        server_port = simpledialog.askstring('Input Box', "Port number: ", parent=main_canvas)
        if server_port != "" and server_address != "":
            try:
                is_connection = True
                player1_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                player1_socket.connect((server_address, int(server_port)))
                main_canvas_right_information_label_turn['text'] = player1_name
                empty_statistic()
                try:
                    receive_connection()
                except Exception:
                    messagebox.showerror("Error Message", "Rejection")
                    is_connection = False
                    messagebox.showinfo("Reminder", 'If you want to reconnect, please click the "connect" button again')
            except Exception:
                messagebox.showerror("Error Message", 'Invalid address or port')
                is_connection = False
                messagebox.showinfo("Reminder", 'If you want to reconnect, please click the "connect" button again')
        else:
            messagebox.showerror("Error Message", 'Invalid address or port')
            is_connection = False
            messagebox.showinfo("Reminder", 'If you want to reconnect, please click the "connect" button again')

def input_game_statistic(game_statistic):
    main_canvas_down_board_loss_number['text'] = game_statistic['loss_number']['player1']
    main_canvas_down_board_tie_number['text'] = game_statistic['tie_number']
    main_canvas_down_board_win_number['text'] = game_statistic['win_number']['player1']
    main_canvas_down_board_game_number['text'] = str(int(game_statistic['game_number']) + 1)

def check_game():
    global game_finish_flag, user_connection, is_start, is_close, is_connection
    game_finish_flag = player1_gameboard.isWinner()
    if game_finish_flag == True:
        message = messagebox.askyesno('Selection', 'Play again or not?')
        if message == True:
            player1_gameboard.number_of_games()
            reset_gameboard()
            player1_socket.send(b'Play Again')
        else:
            player1_socket.send(b'Fun Times')
            user_connection = False
            is_connection = False
            is_close = True
            is_start = False
            player1_socket.close()
            reset_gameboard()
            results = player1_gameboard.computeStats()
            input_game_statistic(results)
            main_canvas_right_information_label_turn['text'] = player1_name


def update_gameboard():
    global location
    location = int(location)
    if location == 9:
        button_9['text'] = 'O'
    elif location == 8:
        button_8['text'] = 'O'
    elif location == 7:
        button_7['text'] = 'O'
    elif location == 6:
        button_6['text'] = 'O'
    elif location == 5:
        button_5['text'] = 'O'
    elif location == 4:
        button_4['text'] = 'O'
    elif location == 3:
        button_3['text'] = 'O'
    elif location == 2:
        button_2['text'] = 'O'
    elif location == 1:
        button_1['text'] = 'O'


def reset_gameboard():
    player1_gameboard.last_turn_player_name = player2_name
    player1_gameboard.resetGameBoard()
    button_1['text'] = ' '
    button_2['text'] = ' '
    button_3['text'] = ' '
    button_4['text'] = ' '
    button_5['text'] = ' '
    button_6['text'] = ' '
    button_7['text'] = ' '
    button_8['text'] = ' '
    button_9['text'] = ' '
    main_canvas_right_information_label_turn['text'] = player1_name


def quit_GUI():
    player1_socket.close()
    main_canvas.destroy()

def player1_create_thread(connection):
    player1_thread = threading.Thread(target=connection)
    player1_thread.start()

player1_create_thread(receive_choice)


def after_clicking_button(button_number):
    button_number_string = str(button_number)
    global player1_gameboard
    if is_start:
        if user_connection:
            if player1_gameboard.last_turn_player_name == player2_name:
                player1_gameboard.last_turn_player_name = player1_name
                change_button(button_number)
                player1_socket.send((button_number_string + '+' + player1_gameboard.last_turn_player_name).encode())
                player1_gameboard.updateGameBoard(player1_gameboard.last_turn_player_name, button_number)
                main_canvas_right_information_label_turn['text'] = player2_name
                check_game()


def check_button(button_number):
    if button_number == 1:
        if button_1['text'] == ' ':
            return True
    elif button_number == 2:
        if button_2['text'] == ' ':
            return True
    elif button_number == 3:
        if button_3['text'] == ' ':
            return True
    elif button_number == 4:
        if button_4['text'] == ' ':
            return True
    elif button_number == 5:
        if button_5['text'] == ' ':
            return True
    elif button_number == 6:
        if button_6['text'] == ' ':
            return True
    elif button_number == 7:
        if button_7['text'] == ' ':
            return True
    elif button_number == 8:
        if button_8['text'] == ' ':
            return True
    elif button_number == 9:
        if button_9['text'] == ' ':
            return True
    else:
        return False

def change_button(button_number):
    if button_number == 1:
        button_1['text'] = 'X'
    elif button_number == 2:
        button_2['text'] = 'X'
    elif button_number == 3:
        button_3['text'] = 'X'
    elif button_number == 4:
        button_4['text'] = 'X'
    elif button_number == 5:
        button_5['text'] = 'X'
    elif button_number == 6:
        button_6['text'] = 'X'
    elif button_number == 7:
        button_7['text'] = 'X'
    elif button_number == 8:
        button_8['text'] = 'X'
    elif button_number == 9:
        button_9['text'] = 'X'

def click_button_1():
    if check_button(1):
        after_clicking_button(1)


def click_button_2():
    if check_button(2):
        after_clicking_button(2)

def click_button_3():
    if check_button(3):
        after_clicking_button(3)


def click_button_4():
    if check_button(4):
        after_clicking_button(4)


def click_button_5():
    if check_button(5):
        after_clicking_button(5)


def click_button_6():
    if check_button(6):
        after_clicking_button(6)


def click_button_7():
    if check_button(7):
        after_clicking_button(7)


def click_button_8():
    if check_button(8):
        after_clicking_button(8)


def click_button_9():
    if check_button(9):
        after_clicking_button(9)
        
main_canvas = tk.Tk()
main_canvas.title("tic tac toe player #1")
main_canvas.geometry("700x700")
main_canvas.resizable(1, 1)
main_canvas.configure(background='lightblue')

title_frame = tk.Frame(main_canvas, bg='lightblue', width=1000, height=100)
title_frame.grid(row=0, column=0)

main_canvas_title = tk.Label(title_frame,
                             font=(
                                 'Times New Roman',
                                 30,
                             ),
                             text="Tic-Tac-Toe GUI Game",
                             bg='lightblue',
                             justify='center')
main_canvas_title.grid(row=0, column=0)

main_canvas_board = tk.Frame(main_canvas,
                             relief='groove',
                             bg='lightblue',
                             width=1000,
                             height=600)
main_canvas_board.grid(row=1, column=0)

main_canvas_left_board = tk.Frame(main_canvas_board,
                                  bd=5,
                                  width=600,
                                  height=400,
                                  bg="lightblue",
                                  relief='groove')
main_canvas_left_board.pack(side='left')
main_canvas_right_information = tk.Frame(main_canvas_board,
                                         bd=5,
                                         width=200,
                                         height=400,
                                         bg='lightblue',
                                         relief='groove')
main_canvas_right_information.pack(side='left')

main_canvas_down_board = tk.Frame(main_canvas, bd=5,
                                  relief='groove',
                                  bg='lightblue',
                                  width=1000,
                                  height=100)
main_canvas_down_board.grid(row=2, column=0)

button_1 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_1)
button_2 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_2)
button_3 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_3)
button_4 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_4)
button_5 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_5)
button_6 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_6)
button_7 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_7)
button_8 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_8)
button_9 = tk.Button(main_canvas_left_board, fg='black', text=' ', bg='lightblue', width=7, height=3,
                     font=('Times New Roman', 23), command=click_button_9)

button_1.grid(column=1, row=1, sticky=N + E + W + S)
button_2.grid(column=1, row=2, sticky=N + E + W + S)
button_3.grid(column=1, row=3, sticky=N + E + W + S)
button_4.grid(column=2, row=1, sticky=N + E + W + S)
button_5.grid(column=2, row=2, sticky=N + E + W + S)
button_6.grid(column=2, row=3, sticky=N + E + W + S)
button_7.grid(column=3, row=1, sticky=N + E + W + S)
button_8.grid(column=3, row=2, sticky=N + E + W + S)
button_9.grid(column=3, row=3, sticky=N + E + W + S)

main_canvas_right_information_label_1 = tk.Label(main_canvas_right_information, text='Host name:', bg='lightblue',
                                                 font=('Times New Roman', 25), width=10, height=4).grid(row=0, column=0)
main_canvas_right_information_label_username = tk.Label(main_canvas_right_information, text='player1', bg='lightblue',
                                                        font=('Times New Roman', 25), width=10, height=4)
main_canvas_right_information_label_username.grid(row=0, column=1)

main_canvas_right_information_label_2 = tk.Label(main_canvas_right_information, text='Whose turn:', bg='lightblue',
                                                 font=('Times New Roman', 25), width=10, height=4).grid(row=1, column=0)
main_canvas_right_information_label_turn = tk.Label(main_canvas_right_information, text=player1_name, bg='lightblue',
                                                    font=('Times New Roman', 25), width=10, height=4)
main_canvas_right_information_label_turn.grid(row=1, column=1)

main_canvas_down_board_game_number_label = tk.Label(main_canvas_down_board, text="Game number:", bg='lightblue',
                                                    font=('Times New Roman', 25), width=15, height=1).grid(row=0,
                                                                                                           column=0,
                                                                                                           sticky=W)
main_canvas_down_board_game_number = tk.Label(main_canvas_down_board, text=" ", bg='lightblue',
                                              font=('Times New Roman', 25), width=20, height=2)
main_canvas_down_board_game_number.grid(row=0, column=1, sticky=W)

main_canvas_down_board_win_number_label = tk.Label(main_canvas_down_board, text="Won:", bg='lightblue',
                                                   font=('Times New Roman', 25), width=10, height=1).grid(row=1,
                                                                                                          column=0,
                                                                                                          sticky=W)
main_canvas_down_board_win_number = tk.Label(main_canvas_down_board, text=" ", bg='lightblue',
                                             font=('Times New Roman', 25), width=20, height=1)
main_canvas_down_board_win_number.grid(row=1, column=1, sticky=W)

main_canvas_down_board_loss_number_label = tk.Label(main_canvas_down_board, text="Lost:", bg='lightblue',
                                                    font=('Times New Roman', 25), width=10, height=1).grid(row=2,
                                                                                                           column=0,
                                                                                                           sticky=W)
main_canvas_down_board_loss_number = tk.Label(main_canvas_down_board, text=" ", bg='lightblue',
                                              font=('Times New Roman', 25), width=20, height=1)
main_canvas_down_board_loss_number.grid(row=2, column=1, sticky=W)

main_canvas_down_board_tie_number_label = tk.Label(main_canvas_down_board, text="Ties:", bg='lightblue',
                                                   font=('Times New Roman', 25), width=10, height=1).grid(row=3,
                                                                                                          column=0,
                                                                                                          sticky=W)
main_canvas_down_board_tie_number = tk.Label(main_canvas_down_board, text=" ", bg='lightblue',
                                             font=('Times New Roman', 25), width=10, height=1)
main_canvas_down_board_tie_number.grid(row=3, column=1, sticky=W)

connection_button = tk.Button(main_canvas_right_information, text='Connect', font=('Times New Roman', 20, 'bold'),
                              height=2, width=7, command=establish_connection).grid(row=2, column=0)
quit_button = tk.Button(main_canvas_right_information, text='Quit', font=('Times New Roman', 20, 'bold'), height=2,
                        width=5, command=quit_GUI).grid(row=2, column=1)

main_canvas.mainloop()