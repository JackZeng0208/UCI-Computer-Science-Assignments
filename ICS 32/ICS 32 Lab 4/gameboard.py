class BoardClass:
    """A class to simulate Tic-tac-toe game

    Attributes:
        last_turn_player_name: User name of the last player to have a turn
        win_number: Number of wins for both players
        tie_number: Number of ties
        loss_number: Number of losses for both players
        game_number: Number of matches
        game_board: A list which updates and stores live game board information
    """
    def __init__(self, name1, name2):
        """Create a Tic-tac-toe game board and necessary game information

        Args:
            player1_name: player1's name
            player2_name: player2's name
            Last_turn_player_name = last turn player
            win_number = number of wins for player1 and player2
            loss_number = number of wins for player1 and player2
            game_number = total game number
            game_board = record every movement
        """
        self.player1_name = name1
        self.player2_name = name2
        self.last_turn_player_name: str = ''
        self.win_number = {"player1": 0, "player2": 0}
        self.tie_number: int = 0
        self.loss_number = {"player1": 0, "player2": 0}
        self.game_number: int = 0
        self.game_board = ['-' for i in range(10)]

    def resetGameBoard(self) -> None:
        """Set an empty game board

        Returns:
            Reset the list with all '-' element ('-' represents empty)
        """
        self.game_board = ['-' for i in range(10)]

    def boardIsFull(self) -> bool:
        """Judge whether the game board is full or not

        Returns:
            True if game board is full, False otherwise.
        """
        if "-" not in self.game_board[1:10]:
            return True
        else:
            return False

    def updateGameBoard(self,player_name,position):
        if player_name == self.player2_name:
            self.last_turn_player_name = player_name
            self.game_board[position] = 'O'
        elif player_name == self.player1_name:
            self.last_turn_player_name = player_name
            self.game_board[position] = 'X'

    def number_of_games(self):
        self.game_number += 1

    def isWinner(self):
        """Judge and update wins and losses numbers

        Returns:
            True if there's winner, False otherwise
        """
        game_finish_flag = False
        _all_possible_lines = [
            self.game_board[1:4], self.game_board[4:7], self.game_board[7:10],
            self.game_board[1::3], self.game_board[2::3],
            self.game_board[3::3], self.game_board[1::4],
            self.game_board[3:8:2]
        ]
        _uppercase_three_x = ["X"] * 3
        _uppercase_three_o = ["O"] * 3

        if _uppercase_three_x in _all_possible_lines or _uppercase_three_o in _all_possible_lines:
            if _uppercase_three_x in _all_possible_lines:
                self.win_number['player1'] += 1
                self.loss_number['player2'] += 1
                game_finish_flag = True
            if _uppercase_three_o in _all_possible_lines:
                self.win_number['player2'] += 1
                self.loss_number['player1'] += 1
                game_finish_flag = True
        elif self.boardIsFull():
            self.tie_number += 1
            game_finish_flag = True
        return game_finish_flag

    def computeStats(self):
        """
        :return: statistics for all data

        """
        result = {}
        result['player1_name'] = self.player1_name
        result['player2_name'] = self.player2_name
        result['win_number'] = self.win_number
        result['loss_number'] = self.loss_number
        result['tie_number'] = self.tie_number
        result['last_turn_player_name'] = self.last_turn_player_name
        result['game_number'] = self.game_number
        result['game_board'] = self.game_board
        return result

