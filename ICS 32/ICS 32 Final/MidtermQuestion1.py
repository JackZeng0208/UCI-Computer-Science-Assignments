''' You are expected to comment your code. At a minimum if statements and for loops
 need at least 1 line of comment to explain what the loop does'''

#------------IMPORT ANY LIBRARIES YOU NEED HERE-----------
from pathlib import Path
import os
import os.path
#---------- CREATE YOUR CUSTOM EXCEPTION HERE-------------
class NotAValidDirectory(Exception):
   pass

'''Write a function that reads all the text files (.txt extension)
located in the directory myPath and writes it to a new Combined
file created in the current directory. It returns the values
defined below. You will raise a custom exception called "NotAValidDirectory"if the directory provided
is not a directory.(Note you will need to create the custom exception called NotAValidDirectory yourself.
'''
def combineFiles(directoryName,combinedFilename):
   '''Use these provided variables and update them with the information described in the comment
   associated with each variable.'''
   totalFileCount = 0 #contains the total number of files in the directory provided
   textFileCount = 0 # provides the number of text files found in the directory
   combinedFileLineCount = 0 #provides the total number of lines in the final combined file

  #----------------YOUR FUNCTION CODE STARTS HERE---------------
   directory = Path(directoryName)
   if directory.is_dir():
      txt_file_path = Path(directoryName).glob("*.txt")
      all_files = os.listdir(directoryName)
      totalFileCount = len(all_files)
      combined_file = open(combinedFilename,'w')

      for file_name in txt_file_path:
         file = open(file_name,'r')
         personal_txt_file = file.readlines()
         textFileCount += 1
         personal_txt_file.append('\n')
         file.close()
         for line in personal_txt_file:
            combined_file.write(line)
            combinedFileLineCount += 1
      combined_file.close()
      combinedFileLineCount = combinedFileLineCount - textFileCount
   else:
      raise NotAValidDirectory
   #------------------- YOUR FUNCTION CODE ENDS HERE------------------   
   return totalFileCount, textFileCount,combinedFileLineCount


'''This will run your program. You should only add code to
handle exceptions that are raised. Any exceptions found will only
display "Exception Found" and end program execution. The program will
only run once.(Does not automatically restart)'''
if __name__ == "__main__":
   directoryName = input("Directory: ")
   combinedFilename = input ("Combined Filename: ")
   try:
      print(combineFiles(directoryName,combinedFilename))
   except NotAValidDirectory:
      print("Exception Found")
      exit()
