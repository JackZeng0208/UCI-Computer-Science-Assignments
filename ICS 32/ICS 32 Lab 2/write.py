import csv
from sportclub import SportClub
from typing import List, Iterable


def separateSports(all_clubs: List[SportClub]) -> Iterable[List[SportClub]]:
    """Separate a list of SportClubs into their own sports

    For example, given the list [SportClub("LA", "Lakers", "NBA"), SportClub("Houston", "Rockets", "NBA"), SportClub("LA", "Angels", "MLB")],
    return the iterable [[SportClub("LA", "Lakers", "NBA"), SportClub("Houston", "Rockets", "NBA")], [SportClub("LA", "Angels", "MLB")]]

    Args:
        all_clubs: A list of SportClubs that contain SportClubs of 1 or more sports.

    Returns:
        An iterable of lists of sportclubs that only contain clubs playing the same sport.
    """
    # TODO: Complete the function
    output_list = []
    all_sports = []
    for clubs in all_clubs:
        all_sports.append(clubs.sport)
    new_all_sports = list(set(all_sports))
    for sports in new_all_sports:
        team_list = []
        for clubs in all_clubs:
            if sports == clubs.sport:
                team_list.append(clubs)
        output_list.append(team_list)
    return output_list


def sortSport(sport: List[SportClub]) -> List[SportClub]:
    """Sort a list of SportClubs by the inverse of their count and their name

    For example, given the list [SportClub("Houston", "Rockets", "NBA", 80), SportClub("LA", "Warriors", "NBA", 130), SportClub("LA", "Lakers", "NBA", 130)]
    return the list [SportClub("LA", "Lakers", "NBA", 130), SportClub("LA", "Warriors", "NBA", 130), SportClub("Houston", "Rockets", "NBA", 80)]

    Args:
        sport: A list of SportClubs that only contain clubs playing the same sport

    Returns:
        A sorted list of the SportClubs
    """
    # TODO: Complete the function
    # hint: check documentation for sorting lists
    # ( https://docs.python.org/3/library/functions.html#sorted , https://docs.python.org/3/howto/sorting.html#sortinghowto )
    output_list = sorted(sport)
    return output_list


def outputSports(sorted_sports: Iterable[List[SportClub]]) -> None:
    """Create the output csv given an iterable of list of sorted clubs

    Create the csv "survey_database.csv" in the current working directory, and output the information:
    "City,Team Name,Sport,Number of Times Picked" for the top 3 teams in each sport.

    Args:
        sorted_sports: an Iterable of different sports, each already sorted correctly
    """
    # TODO: Complete the function
    with open("survey_database.csv", "w", newline='') as output_file:
        field_name = ['City', 'Team Name', 'Sport', 'Number of Times Picked']
        csv_writer = csv.DictWriter(output_file, delimiter=',', fieldnames=field_name)
        csv_writer.writeheader()
        for sports in sorted_sports:
            for i, sublist in enumerate(sports):
                if 3 > i >= 0:
                    csv_writer.writerow({'City': sublist.getCity(), 'Team Name': sublist.getName(), 'Sport': sublist.getSport(), 'Number of Times Picked': sublist.getCount()})
                else:
                    break
        output_file.close()