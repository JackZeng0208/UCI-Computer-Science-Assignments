
from pathlib import Path
import csv
from sportclub import SportClub
from typing import List, Tuple


def readFile(file: Path) -> List[Tuple[str, str, str]]:
    """Read a CSV file and return its content

    A good CSV file will have the header "City,Team Name,Sport" and appropriate content.

    Args:
        file: a path to the file to be read

    Returns:
        a list of tuples that each contain (city, name, sport) of the SportClub

    Raises:
        ValueError: if the reading csv has missing data (empty fields)
    """
    # TODO: Complete the function
    with open(file, 'r', newline='') as my_csv_file:
        csv_data = csv.reader(my_csv_file)
        output_data = []
        for i, row in enumerate(csv_data):
            if i == 0 and row[0] != "City" and row[1] != "Team Name" and row[2] != "Sport":
                raise ValueError
            if len(row) != 3:
                raise ValueError
            if row[0] == '' or row[1] == '' or row[2] == '':
                raise ValueError
            if i > 0:
                output_data.append(tuple(row))
    return output_data


def readAllFiles() -> List[SportClub]:
    """Read all the csv files in the current working directory to create a list of SportClubs that contain unique SportClubs with their corresponding counts

    Take all the csv files in the current working directory, calls readFile(file) on each of them, and accumulates the data gathered into a list of SportClubs.
    Create a new file called "report.txt" in the current working directory containing the number of good files and good lines read.
    Create a new file called "error_log.txt" in the current working directory containing the name of the error/bad files read.

    Returns:
        a list of unique SportClub objects with their respective counts
    """
    # TODO: Complete the function
    csv_file_path = Path(".").glob("*.csv")
    all_data = []
    output_list = []
    team_appearance = []
    good_file_number = 0
    report_file = open("report.txt", "w")
    error_log_file = open("error_log.txt", "w")
    for file_name in csv_file_path:
        try:
            input_file = readFile(file_name)
            all_data.append(input_file)
            good_file_number += 1
        except ValueError:
            if str(file_name) != "survey_database.csv":
                error_log_file.write(str(file_name) + "\n")
            continue
    for lists in all_data:
        for subtuple in lists:
            sportclub_object = SportClub(subtuple[0].title(), subtuple[1].title(), subtuple[2].upper())
            team_appearance.append(subtuple[1].title())
            output_list.append(sportclub_object)
    report_file.writelines("Number of files read: " + str(good_file_number) + "\n")
    report_file.writelines("Number of lines read: " + str(len(output_list)) + "\n")
    new_output_list = list(set(output_list))
    for clubs in new_output_list:
        clubs.count = team_appearance.count(clubs.name)
    report_file.close()
    error_log_file.close()
    return new_output_list