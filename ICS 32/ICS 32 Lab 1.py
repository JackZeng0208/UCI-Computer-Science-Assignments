<<<<<<< HEAD:ICS 32 Lab 1.py
"""
Name: Yixiao (Jack) Zeng
UCINetID: yixiaz8
"""

''' This functions asks the user for the number of steps
they want to climb, gets the value provided by the user
and returns it to the calling function. This function will
raise any exceptions related to none integer user inputs.'''


class IntegerOutOfRangeException(Exception):
    pass


class NoStaircaseSizeException(Exception):
    pass


def getUserInput():
    userSteps = input("Please input your staircase size:")
    if userSteps == "DONE":
        return "DONE"
    else:
        return int(userSteps)


''' This function takes the number of steps as an input parameter,
creates a string that contains the entire steps based on the user input
and returns the steps string to the calling function. This function will raise
any exceptions resulting from invalid integer values. '''


def printSteps(stepCount):
    totalSteps = ""
    if stepCount == 0:
        raise NoStaircaseSizeException
    if stepCount < 0 or stepCount >= 100:
        raise IntegerOutOfRangeException
    if stepCount >= 1 and stepCount < 1000:
        for step in range(stepCount, 0, -1):
            if step == stepCount:
                totalSteps += (step*2-2)*' '+"+-+\n"
            else:
                totalSteps += (step*2-2)*' '+"+-+-+\n"
            totalSteps += (step*2-2)*" "+"| |\n"
            if(step == 1):
                totalSteps += "+-+"
    return totalSteps


''' This function kicks off the running of your program. Once it starts
it will continuously run your program until the user explicitly chooses to
end the running of the program based on the requirements. This function returns
the string "Done Executing" when it ends. Additionally, all exceptions will be
handled (caught) within this function. '''


def runProgram():
    try:
        userSteps = getUserInput()
        if(userSteps == "DONE"):
            return ("Done Executing")
        print(printSteps(userSteps))
        runProgram()
    except ValueError:
        print("Invalid staircase value entered.")
        runProgram()
    except NoStaircaseSizeException:
        print("I cannot draw a staircase with no steps.")
        runProgram()
    except IntegerOutOfRangeException:
        print("That staircase size is out of range.")
        runProgram()


'''Within this condition statement you are to write the code that kicks off
your program. When testing your code the code below this
should be the only code not in a function and must be within the if
statement. I will explain this if statement later in the course.'''

if __name__ == "__main__":
    runProgram()
=======
"""
Name: Yixiao (Jack) Zeng
UCINetID: yixiaz8
"""

''' This functions asks the user for the number of steps
they want to climb, gets the value provided by the user
and returns it to the calling function. This function will
raise any exceptions related to none integer user inputs.'''


class IntegerOutOfRangeException(Exception):
    pass

class NoStaircaseSizeException(Exception):
    pass


def getUserInput():
    userSteps = input("Please input your staircase size:")
    if userSteps == "DONE":
        return userSteps
    try:
        return int(userSteps)
    except:
        raise ValueError


''' This function takes the number of steps as an input parameter,
creates a string that contains the entire steps based on the user input
and returns the steps string to the calling function. This function will raise
any exceptions resulting from invalid integer values. '''


def printSteps(stepCount):
    if stepCount == 0:
        raise NoStaircaseSizeException
    if stepCount < 0 or stepCount > 999:
        raise IntegerOutOfRangeException
    else:
        totalSteps = ""
        for step in range(stepCount, 0, -1):
            if step == stepCount:
                totalSteps += (step*2-2)*' '+"+-+\n"
            else:
                totalSteps += (step*2-2)*' '+"+-+-+\n"
            totalSteps += (step*2-2)*" "+"| |\n"
            if(step == 1):
                totalSteps += "+-+"
    return totalSteps


''' This function kicks off the running of your program. Once it starts
it will continuously run your program until the user explicitly chooses to
end the running of the program based on the requirements. This function returns
the string "Done Executing" when it ends. Additionally, all exceptions will be
handled (caught) within this function. '''


def runProgram():
    while(True):
        try:
            userSteps = getUserInput()
            if(userSteps == "DONE"):
                return ("Done Executing")
            print(printSteps(userSteps))
        except ValueError:
            print("Invalid staircase value entered.")
        except NoStaircaseSizeException:
            print("I cannot draw a staircase with no steps.")
        except IntegerOutOfRangeException:
            print("That staircase size is out of range.")

'''Within this condition statement you are to write the code that kicks off
your program. When testing your code the code below this
should be the only code not in a function and must be within the if
statement. I will explain this if statement later in the course.'''

if __name__ == "__main__":
    runProgram()
>>>>>>> a8a4fc0c1d34a4446dae20d82055f39e4740ab23:lab1.py
