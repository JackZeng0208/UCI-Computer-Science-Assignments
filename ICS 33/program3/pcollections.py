import re, traceback, keyword

def pnamedtuple(type_name, field_names, mutable = False,  defaults =  {}):
    def show_listing(s):
        for line_number, text_of_line in enumerate(s.split('\n'),1):         
            print(f' {line_number: >3} {text_of_line.rstrip()}')

    # put your code here
    # bind class_definition (used below) to the string constructed for the class
    
    # Note: This is the unique() function in week 4 notes
    def unique(iterable):
        iterated = set()
        for i in iterable:
            if i not in iterated:
                iterated.add(i)
                yield i
    
    legal_name_pattern = re.compile(r'^[a-zA-Z][A-Za-z0-9_]*$')
    split_pattern = re.compile(r'[ ,]+')
    legal_name_list = []
    if type(type_name) != str:
        raise SyntaxError
    elif legal_name_pattern.match(type_name) == None:
        raise SyntaxError
    if len(defaults) != 0:
        for key, value in defaults.items():
            if key not in field_names:
                raise SyntaxError
    if type(field_names) not in [str, list]:
        raise SyntaxError("Field_name is not supported")
    else:
        if type(field_names) == str:
            field_names = split_pattern.split(field_names)
        if type(field_names) == list:
            for i in unique(field_names):   
                if i in keyword.kwlist:
                    raise SyntaxError("Field_name is in list of Python module")      
                if legal_name_pattern.match(i) == None:
                    raise SyntaxError("Field_name is not legal")
                legal_name_list.append(i)
    
    for i in legal_name_list:
        if type(i) != str:
            raise SyntaxError
    
    argument = ", ".join(legal_name_list)
    gen_init = ''
    for i in legal_name_list:
        if i in defaults:
            gen_init += f"        self.{i} = {defaults[i]}\n"
        else:
            gen_init += f"        self.{i} = {i}\n"
    
    repr_string = ''
    for i in legal_name_list:
        repr_string += f"{str(i)}={{self.{i}}},"
    repr_string = repr_string[:-1]
    
    get_sth = ''
    for i in field_names:
        get_sth += f'''
    def get_{i}(self):
        return self.{i}
        '''
    class_definition = f'''
class {type_name}:
    _fields = {legal_name_list}
    _mutable = {mutable}
    def __init__(self, {argument}):
{gen_init}
        
    def __repr__(self):
        return f'{type_name}({repr_string})'

    def __getitem__(self, index):
        if type(index) == int:
            if index < 0 or index >= len(self._fields):
                raise IndexError("Index is not in the range")
            else:
                return eval("self.get_" + str(self._fields[index]) + "()")
        elif type(index) == str:
            if index in self._fields:
                return eval("self.get_" + str(index) + "()")
            elif index not in self._fields:
                raise IndexError("Index is not in the field")
        else:
            raise IndexError("Index is illegal")
    
    def __eq__(self, right_value):
        if type(right_value) != {type_name}:
            return False
        else:
            for i in self._fields:
                if self.__getitem__(i) != right_value.__getitem__(i):
                    return False
        return True
        
    def _asdict(self):
        result = dict()
        for i in self._fields:
            result[i] = self.__getitem__(i)
        return result
    
    def _make(iterable):
        return {type_name}(*iterable)
    
    def _replace(self , **kargs):
        for key, value in kargs.items():
            if key not in self._fields:
                raise TypeError
        if self._mutable:
            for key, value in kargs.items():
                self.__dict__[key] = value
            return None
        else:
            result = list()
            for i in self._fields:
                if i in kargs.keys():
                    result.append(kargs[i])
                else:
                    result.append(self.__dict__[i])
            return {type_name}._make(result)
    
    def __setattr__ (self, name, value):
        if self._mutable:
            self.__dict__[name] = value
        elif name not in self.__dict__:
            self.__dict__[name] = value
        else:
            raise AttributeError("pnamedtuple is not mutable")
'''
    class_definition += get_sth
    
    # Debugging aid: uncomment next call to show_listing to display source code
    # show_listing(class_definition)
    
    # Execute str in class_definition in name_space; then bind the attribute
    #   source_code to the class_definition str; finally, return the class
    #   object created; any syntax errors will print a listing of the
    #   of class_definition and trace the error (see the except clause).
    name_space = dict( __name__ = f'pnamedtuple_{type_name}' )                  
    try:
        exec(class_definition,name_space)
        name_space[type_name].source_code = class_definition
    except (TypeError,SyntaxError):                      
        show_listing(class_definition)
        traceback.print_exc()
    return name_space[type_name]


    
if __name__ == '__main__':
    # Test simple pnamedtuple below in script: Point=pnamedtuple('Point','x,y')

    #driver tests
    from courselib import driver
    driver.default_file_name = 'bscp3W22.txt'
#     driver.default_show_exception_message = True
#     driver.default_show_traceback = True
    driver.driver()
