import prompt
from goody import irange
from collections import defaultdict


# List Node class and helper functions (to set up problem)

class LN: # Node
    created = 0
    def __init__(self,value,next=None):
        self.value = value
        self.next  = next
        LN.created  += 1

def list_to_ll(l):
    if l == []:
        return None
    front = rear = LN(l[0])
    for v in l[1:]:
        rear.next = LN(v)
        rear = rear.next
    return front

def str_ll(ll):
    answer = ''
    while ll != None:
        answer += str(ll.value)+'->'
        ll = ll.next
    return answer + 'None'



# Tree Node class and helper functions (to set up problem)

class TN:
    def __init__(self,value,left=None,right=None):
        self.value = value
        self.left  = left
        self.right = right

def list_to_tree(alist):
    if alist == None:
        return None
    else:
        return TN(alist[0],list_to_tree(alist[1]),list_to_tree(alist[2])) 
    
def str_tree(atree,indent_char ='.',indent_delta=2):
    def str_tree_1(indent,atree):
        if atree == None:
            return ''
        else:
            answer = ''
            answer += str_tree_1(indent+indent_delta,atree.right)
            answer += indent*indent_char+str(atree.value)+'\n'
            answer += str_tree_1(indent+indent_delta,atree.left)
            return answer
    return str_tree_1(0,atree) 


# Define alternate ITERATIVELY

def alternate_i(ll1 : LN, ll2 : LN) -> LN:
    # Handle the case of ll1 or ll2 being empty
    if ll1 == None and ll2 == None:
        return None
    
    if ll1 != None and ll2 == None:
        return ll1
    
    if ll1 == None and ll2 != None:
        return ll2
    # Set up for iteration (keep track of front and rear of linked list to return)
    front = rear = LN(None)
    while True:
        if ll1 is None:
            rear.next = ll2
            break
        elif ll2 is None:
            rear.next = ll1
            break
        else:
            rear.next = ll1
            rear = ll1
            ll1 = ll1.next
            rear.next = ll2
            rear = ll2
            ll2 = ll2.next
    if ll1 != None:
        rear.next = ll1
    elif ll2 != None:
        rear.next = ll2
    return front.next

# Define alternate RECURSIVELY

def alternate_r(ll1 : LN, ll2 : LN) -> LN:
    if ll1 == None:
        return ll2
    elif ll2 == None:
        return ll1
    else:
        temp1 = ll1.next
        temp2 = ll2.next
        result = ll1
        ll1.next = ll2
        temp3 = alternate_r(temp1, temp2)
        ll2.next = temp3
        return result
        
# Define count RECURSIVELY

def count(t,value):
    if t is None:
        return 0
    else:
        if t.value == value:
            return 1 + count(t.left, value) + count(t.right, value)
        else:
            return count(t.left, value) + count(t.right, value)
            



class bidict(dict):
    _all_objects = []
    def __init__(self, initial = [], **kargs):
        dict.__init__(self, initial, **kargs)
        self._rdict = defaultdict(set)
        try:
            for key, value in kargs.items():
                hash(key)
                hash(value)
                self._rdict[value].add(key)
        except:
            raise ValueError
        self._rdict = dict(self._rdict)
        self._all_objects.append(self)
    
    def __setitem__(self, key, value):
        result = defaultdict(set)
        l = []
        for key1, value1 in self._rdict.items():
            for i in value1:
                l.append(i)
                if key1 != value and i == key:
                    result[value].add(key)
                else:
                    result[key1].add(i)
        if key not in l:
            result[value].add(key)
        self._rdict = dict(result)
        dict.__setitem__(self, key, value)
    
    def clear(self):
        self._rdict = dict()
        dict.clear(self)
    
    def __delitem__(self, key):
        result = defaultdict(set)
        for key1, value1 in self._rdict.items():
            for i in value1:
                if i != key:
                    result[key1].add(i)
        self._rdict = dict(result)
        dict.__delitem__(self, key)
    
    def __call__(self, value):
        for key1, value1 in self._rdict.items():
            if value == key1:
                return value1
        raise KeyError
    
    def all_objects():
        return bidict._all_objects
    
    def forget(object):
        bidict._all_objects.remove(object)
    
    
# Testing Script

if __name__ == '__main__':
    # print('Testing alternate_i')
    # ll1 = list_to_ll([])
    # ll2 = list_to_ll([])
    # print('\n  ll1= ',str_ll(ll1))
    # print(  '  ll2= ',str_ll(ll2))
    # ll = alternate_i(ll1,ll2)
    # print('  alternate  = ',str_ll(ll))
    # print('  ll1= ',str_ll(ll1))
    # print('  ll2= ',str_ll(ll2))
    #
    # ll1 = list_to_ll([1])
    # ll2 = list_to_ll([])
    # print('\n  ll1= ',str_ll(ll1))
    # print('  ll2= ',str_ll(ll2))
    # ll = alternate_i(ll1,ll2)
    # print('  alternate  = ',str_ll(ll))
    # print('  ll1= ',str_ll(ll1))
    # print('  ll2= ',str_ll(ll2))
    #
    # ll1 = list_to_ll([])
    # ll2 = list_to_ll([1])
    # print('\n  ll1= ',str_ll(ll1))
    # print(  '  ll2= ',str_ll(ll2))
    # ll = alternate_i(ll1,ll2)
    # print('  alternate  = ',str_ll(ll))
    # print('  ll1= ',str_ll(ll1))
    # print('  ll2= ',str_ll(ll2))
    #
    # ll1 = list_to_ll([1])
    # ll2 = list_to_ll([2])
    # print('\n  ll1= ',str_ll(ll1))
    # print(  '  ll2= ',str_ll(ll2))
    # ll = alternate_i(ll1,ll2)
    # print('  alternate  = ',str_ll(ll))
    # print('  ll1= ',str_ll(ll1))
    # print('  ll2= ',str_ll(ll2))
    #
    # ll1 = list_to_ll(['a','b','c','d'])
    # ll2 = list_to_ll(['w','x','y','z'])
    # print('\n  ll1= ',str_ll(ll1))
    # print(  '  ll2= ',str_ll(ll2))
    # ll = alternate_i(ll1,ll2)
    # print('  alternate  = ',str_ll(ll))
    # print('  ll1= ',str_ll(ll1))
    # print('  ll2= ',str_ll(ll2))
    #
    # ll1 = list_to_ll(['a','b','c','d','e','f','g'])
    # ll2 = list_to_ll(['w','x','y','z'])
    # print('\n  ll1= ',str_ll(ll1))
    # print(  '  ll2= ',str_ll(ll2))
    # ll = alternate_i(ll1,ll2)
    # print('  alternate  = ',str_ll(ll))
    # print('  ll1= ',str_ll(ll1))
    # print('  ll2= ',str_ll(ll2))
    #
    # ll1 = list_to_ll(['a','b','c','d'])
    # ll2 = list_to_ll(['w','x','y','z',1,2,3,4])
    # print('\n  ll1= ',str_ll(ll1))
    # print(  '  ll2= ',str_ll(ll2))
    # ll = alternate_i(ll1,ll2)
    # print('  alternate  = ',str_ll(ll))
    # print('  ll1= ',str_ll(ll1))
    # print('  ll2= ',str_ll(ll2))
    #
    # # Put in your own tests here
    #
    #
    print('\n\nTesting alternate_r')
    ll1 = list_to_ll([])
    ll2 = list_to_ll([])
    print('\n  ll1= ',str_ll(ll1))
    print(  '  ll2= ',str_ll(ll2))
    ll = alternate_r(ll1,ll2)
    print('  alternate  = ',str_ll(ll))
    print('  ll1= ',str_ll(ll1))
    print('  ll2= ',str_ll(ll2))
    
    ll1 = list_to_ll([1])
    ll2 = list_to_ll([])
    print('\n  ll1= ',str_ll(ll1))
    print(  '  ll2= ',str_ll(ll2))
    ll = alternate_r(ll1,ll2)
    print('  alternate  = ',str_ll(ll))
    print('  ll1= ',str_ll(ll1))
    print('  ll2= ',str_ll(ll2))
    
    ll1 = list_to_ll([])
    ll2 = list_to_ll([1])
    print('\n  ll1= ',str_ll(ll1))
    print(  '  ll2= ',str_ll(ll2))
    ll = alternate_r(ll1,ll2)
    print('  alternate  = ',str_ll(ll))
    print('  ll1= ',str_ll(ll1))
    print('  ll2= ',str_ll(ll2))
    
    ll1 = list_to_ll([1])
    ll2 = list_to_ll([2])
    print('\n  ll1= ',str_ll(ll1))
    print(  '  ll2= ',str_ll(ll2))
    ll = alternate_r(ll1,ll2)
    print('  alternate  = ',str_ll(ll))
    print('  ll1= ',str_ll(ll1))
    print('  ll2= ',str_ll(ll2))
    
    ll1 = list_to_ll(['a','b','c','d'])
    ll2 = list_to_ll(['w','x','y','z'])
    print('\n  ll1= ',str_ll(ll1))
    print(  '  ll2= ',str_ll(ll2))
    ll = alternate_r(ll1,ll2)
    print('  alternate  = ',str_ll(ll))
    print('  ll1= ',str_ll(ll1))
    print('  ll2= ',str_ll(ll2))
    
    ll1 = list_to_ll(['a','b','c','d','e','f','g'])
    ll2 = list_to_ll(['w','x','y','z'])
    print('\n  ll1= ',str_ll(ll1))
    print(  '  ll2= ',str_ll(ll2))
    ll = alternate_r(ll1,ll2)
    print('  alternate  = ',str_ll(ll))
    print('  ll1= ',str_ll(ll1))
    print('  ll2= ',str_ll(ll2))
    
    ll1 = list_to_ll(['a','b','c','d'])
    ll2 = list_to_ll(['w','x','y','z',1,2,3,4])
    print('\n  ll1= ',str_ll(ll1))
    print(  '  ll2= ',str_ll(ll2))
    ll = alternate_r(ll1,ll2)
    print('  alternate  = ',str_ll(ll))
    print('  ll1= ',str_ll(ll1))
    print('  ll2= ',str_ll(ll2))
    #
    # # Put in your own tests here
    #
    #
    # print('\n\nTesting count')
    # tree = list_to_tree(None)
    # print('\nfor tree = \n',str_tree(tree))
    # for i in [1]:
    #     print('count(tree,'+str(i)+') = ', count(tree,i))
    #
    # tree = list_to_tree([1, [2, None, None], [3, None, None]])
    # print('\nfor tree = \n',str_tree(tree))
    # for i in irange(1,3):
    #     print('count(tree,'+str(i)+') = ', count(tree,i))
    #
    # tree = list_to_tree([3, [2, None, [3, None, None]], [1, [3, None, None], None]])
    # print('\nfor tree = \n',str_tree(tree))
    # for i in irange(1,3):
    #     print('count(tree,'+str(i)+') = ', count(tree,i))
    #
    # tree = list_to_tree([3, [2, [3, None, [2, None, None]], [3, None, None]], [1, [3, None, None], None]])
    # print('\nfor tree = \n',str_tree(tree))
    # for i in irange(1,3):
    #     print('count(tree,'+str(i)+') = ', count(tree,i))
    #
    #
    # # Put in your own tests here
    #
    #
    print('\n\nTesting bidict')
    b1 = bidict(a=1,b=2,c=1)
    print(b1, b1._rdict)
    b1['a']= 2
    print(b1, b1._rdict)
    b1['d']= 2
    print(b1, b1._rdict)
    b1['c']= 2
    print(b1, b1._rdict)
    
    b1.clear()
    print(b1, b1._rdict)
    
    b2 = bidict(a=1,b=2,c=1,d=2)
    del b2['a']
    print(b2, b2._rdict)
    del b2['b']
    print(b2, b2._rdict)
    del b2['c']
    print(b2, b2._rdict)
    del b2['d']
    print(b2, b2._rdict)
    print(b2._rdict)
    
    b3 = bidict(a=1,b=2,c=1,d=2)
    print(b3(1))
    print(b3(2))
    #
    print(bidict.all_objects())
    bidict.forget(b2)
    print(bidict.all_objects())
    bidict.forget(b3)
    print(bidict.all_objects())
    bidict.forget(b1)
    print(bidict.all_objects())

    # Put in your own tests here

   

   
    import driver
    driver.default_file_name = 'bscq6W22.txt'
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    print('\n\n')
    driver.driver()
    
