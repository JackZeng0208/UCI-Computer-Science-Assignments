import copy

import prompt                          # for driver, which prompts user
from collections import defaultdict    # use if you want (see problem descriptions)
from copy import deepcopy
# You might use the following abbreviations (or their full names)
#   for the various parts of the Warehouse database

# i    : for inventory (how many products are available)
# p    : for product
# w    : for warehouse


def read_db (openfile : open) ->  {str:{str:int}}:   
    result = defaultdict(dict)
    for lines in openfile:
        temp = lines.rstrip('\n').split(":")
        for i in range(1, len(temp), 2):
            result[temp[i]].update({temp[0]:int(temp[i+1])})
    return dict(result)



        
def inventory (db : {str:{str:int}}) ->  [(str,int)]:   
    result = dict()
    for key, value in db.items():
        for key2, value2 in value.items():
            if key2 in result:
                result[key2] += value2
            if key2 not in result:
                result.update({key2:value2})
    sorted_result = sorted(result.items(), key= lambda i: (-i[1], i[0]))
    return sorted_result




def resupply (db : {str:{str:int}}, must_have : int) ->  {str: {(str,int)}}:   
    result = defaultdict(set)
    for key, value in db.items():
        for key2, value2 in value.items():
            if key2 in result.keys():
                if value2 < must_have:
                    result[key2].add((key,must_have-value2))
            if key2 not in result.keys():
                if value2 < must_have:
                    result.update({key2:set()})
                    result[key2].add((key,must_have-value2))
    return dict(result)


            
def update_purchases (db : {str:{str:int}}, purchases : [(str,str)] ) ->  {(str,str)}:   
    result = set()
    db_copy = copy.deepcopy(db)
    for i in purchases:
        for key, value in db_copy.items():
            if i[0] in db:
                if i[1] in value:
                    if value[i[1]] == 0:
                        result.add((key, i[1]))
                    elif value[i[1]] > 0:
                        value[i[1]] -= 1
                if i[1] not in value:
                    result.add((key, i[1]))
            elif i[0] not in db:
                result.add((key, i[1]))
    return result






      
def product_locations (db : {str:{str:int}}) ->  [(str,[(str,int)])]:   
    result = defaultdict(list)
    for key, value in db.items():
        for key2, value2 in value.items():
            if key2 not in result:
                result.update({key2:list()})
            if key2 in result:
                result[key2].append((key, value2))
    sorted_result = sorted(result.items(), key=lambda i: (i[0], i[0][0]))
    return sorted_result



def unique_supplier (db : {str:{str:int}}) ->  {str:{str}}:   
    pass


 
if __name__ == '__main__':
    # You can add any of your own specific testing code here
    # You can comment-out tests after your code passes them (but don't change your code!)
     
    # checks whether answer is correct, printing appropriate information
    # Note that dict/defaultdict will compare == if they have the same keys and
    #   associated values, regardless of the fact that they print differently
    def check (answer, correct):
        if (answer == correct):
            print ('    CORRECT')
        else:
            print ('    INCORRECT')
            print ('      was       =',answer)
            print ('      should be =',correct)
        print()
 
  
    # These tests are similar to those that the driver will execute for batch self-checks
    # To run a test, enter True (or just press enter); to skip a test, enter False 
         
    if prompt.for_bool('Test read_db?', True): 
        db = 'db1.txt'
        answer = read_db(open(db))
        print('  db =',db,'\n  read_db =',answer)
        check(answer, {'Irvine':  {'brush': 3,    'comb': 2,    'wallet': 2},
                       'Newport': {'comb': 1,     'stapler': 0},
                       'Tustin' : {'keychain': 3, 'pencil': 4,  'wallet': 3}})


        db = 'db2.txt'
        answer = read_db(open(db))
        print('  db =',db,'\n  read_db =',answer)
        check(answer, {'Irvine':  {'brush': 4, 'comb': 2, 'keychain': 6, 'lipgloss': 3, 'wallet': 3},
                       'Newport': {'brush': 1, 'comb': 5, 'keychain': 3, 'lipgloss': 6, 'wallet': 1},
                       'Tustin':  {'brush': 1, 'comb': 3, 'keychain': 2, 'lipgloss': 4, 'wallet': 2}})

    ##################################################
              

    if prompt.for_bool('Test inventory?', True): 
        db = {'Irvine':  {'brush': 3,    'comb': 2,    'wallet': 2},
              'Newport': {'comb': 1,     'stapler': 0},
              'Tustin' : {'keychain': 3, 'pencil': 4,  'wallet': 3}}

        answer = inventory(db)
        print('  db =',db,'\n  inventory =',answer)
        check(answer, [('wallet', 5), ('pencil', 4), ('brush', 3), ('comb', 3), ('keychain', 3), ('stapler', 0)])

 
        db = {'Irvine':  {'brush': 4, 'comb': 2, 'keychain': 6, 'lipgloss': 3, 'wallet': 3},
              'Newport': {'brush': 1, 'comb': 5, 'keychain': 3, 'lipgloss': 6, 'wallet': 1},
              'Tustin':  {'brush': 1, 'comb': 3, 'keychain': 2, 'lipgloss': 4, 'wallet': 2}}

        answer = inventory(db)
        print('  db =',db,'\n  inventory =',answer)
        check(answer, [('lipgloss', 13), ('keychain', 11), ('comb', 10), ('brush', 6), ('wallet', 6)])

 
    ##################################################
              

    if prompt.for_bool('Test resupply?', True): 
        db = {'Irvine':  {'brush': 3,    'comb': 2,    'wallet': 2},
              'Newport': {'comb': 1,     'stapler': 0},
              'Tustin' : {'keychain': 3, 'pencil': 4,  'wallet': 3}}

        answer = resupply(db,3)
        print('  db =',db,'\n  resupply =',answer)
        check(answer, {'comb': {('Newport', 2), ('Irvine', 1)}, 'wallet': {('Irvine', 1)}, 'stapler': {('Newport', 3)}})

 
        db = {'Irvine':  {'brush': 4, 'comb': 2, 'keychain': 6, 'lipgloss': 3, 'wallet': 3},
              'Newport': {'brush': 1, 'comb': 5, 'keychain': 3, 'lipgloss': 6, 'wallet': 1},
              'Tustin':  {'brush': 1, 'comb': 3, 'keychain': 2, 'lipgloss': 4, 'wallet': 2}}

        answer = resupply(db,4)
        print('  db =',db,'\n  resupply =',answer)
        check(answer, {'comb': {('Tustin', 1), ('Irvine', 2)}, 'lipgloss': {('Irvine', 1)}, 'wallet': {('Tustin', 2), ('Newport', 3), ('Irvine', 1)}, 'brush': {('Tustin', 3), ('Newport', 3)}, 'keychain': {('Tustin', 2), ('Newport', 1)}})

 
    ##################################################
    
         
    if prompt.for_bool('Test update_purchases?', True): 
        db = {'Irvine':  {'brush': 3,    'comb': 2,    'wallet': 2},
              'Newport': {'comb': 1,     'stapler': 0},
              'Tustin' : {'keychain': 3, 'pencil': 4,  'wallet': 3}}
        purchases = [('Irvine', 'brush'), ('Newport','comb'), ('Tustin', 'car'), ('Newport', 'comb')]

        answer = update_purchases(db,purchases)
        print('  db =',db,'\n  update_purchases =',answer)
        check(answer, {('Tustin', 'car'), ('Newport', 'comb')})
        print('  db mutated =',db)
        check(db, {'Irvine':  {'brush': 2,    'comb': 2,    'wallet': 2},
                   'Newport': {'comb': 0,     'stapler': 0},
                   'Tustin' : {'keychain': 3, 'pencil': 4,  'wallet': 3}})

        
        db = {'Irvine':  {'brush': 4, 'comb': 2, 'keychain': 6, 'lipgloss': 3, 'wallet': 3},
              'Newport': {'brush': 1, 'comb': 5, 'keychain': 3, 'lipgloss': 6, 'wallet': 1},
              'Tustin':  {'brush': 1, 'comb': 3, 'keychain': 2, 'lipgloss': 4, 'wallet': 2}}
        purchases = [('Santa-Ana','brush'), ('Tustin', 'brush'), ('Newport','keychain'), ('Newport', 'food'), ('Tustin', 'brush')]

        answer = update_purchases(db,purchases)
        print('  db =',db,'\n  update_purchases =',answer)
        check(answer, {('Newport', 'food'), ('Santa-Ana','brush'),('Tustin', 'brush')})
        print('  db mutated =',db)
        check(db, {'Irvine':  {'brush': 4, 'comb': 2, 'keychain': 6, 'lipgloss': 3, 'wallet': 3},
                   'Newport': {'brush': 1, 'comb': 5, 'keychain': 2, 'lipgloss': 6, 'wallet': 1},
                   'Tustin':  {'brush': 0, 'comb': 3, 'keychain': 2, 'lipgloss': 4, 'wallet': 2}})

        
    ##################################################
   
         
    if prompt.for_bool('Test product_locations?', True): 
        db = {'Irvine':  {'brush': 3,    'comb': 2,    'wallet': 2},
              'Newport': {'comb': 1,     'stapler': 0},
              'Tustin' : {'keychain': 3, 'pencil': 4,  'wallet': 3}}


        answer = product_locations(db)
        print('  db =',db,'\n  product_locations =',answer)
        check(answer, [('brush', [('Irvine', 3)]),
                       ('comb', [('Irvine', 2), ('Newport', 1)]),
                       ('keychain', [('Tustin', 3)]),
                       ('pencil', [('Tustin', 4)]),
                       ('stapler', [('Newport', 0)]),
                       ('wallet', [('Irvine', 2), ('Tustin', 3)])])
 
 
        db = {'Irvine':  {'brush': 4, 'comb': 2, 'keychain': 6, 'lipgloss': 3, 'wallet': 3},
              'Newport': {'brush': 1, 'comb': 5, 'keychain': 3, 'lipgloss': 6, 'wallet': 1},
              'Tustin':  {'brush': 1, 'comb': 3, 'keychain': 2, 'lipgloss': 4, 'wallet': 2}}


        answer = product_locations(db)
        print('  db =',db,'\n  product_locations =',answer)
        check(answer,  [('brush', [('Irvine', 4), ('Newport', 1), ('Tustin', 1)]),
                        ('comb', [('Irvine', 2), ('Newport', 5), ('Tustin', 3)]),
                        ('keychain', [('Irvine', 6), ('Newport', 3), ('Tustin', 2)]),
                        ('lipgloss', [('Irvine', 3), ('Newport', 6), ('Tustin', 4)]),
                        ('wallet', [('Irvine', 3), ('Newport', 1), ('Tustin', 2)])])
 
 
    ##################################################
              

    if prompt.for_bool('Test unique_supplier?', True): 
        db = {'Irvine':  {'brush': 3,    'comb': 2,    'wallet': 2},
              'Newport': {'comb': 1,     'stapler': 0},
              'Tustin' : {'keychain': 3, 'pencil': 4,  'wallet': 3}}
        answer = unique_supplier(db)
        print('  db =',db,'\n  unique_supplier =',answer)
        check(answer, {'Irvine': {'brush'}, 'Newport': {'stapler'}, 'Tustin': {'pencil', 'keychain'}})

        
        db = {'Irvine':  {'brush': 4, 'comb': 2, 'keychain': 6,                'wallet': 3},
              'Newport': {            'comb': 5, 'keychain': 3, 'lipgloss': 6, 'wallet': 1},
              'Tustin':  {            'comb': 3, 'keychain': 2,                'wallet': 2}}


        answer = unique_supplier(db)
        print('  db =',db,'\n  unique_supplier =',answer)
        check(answer, {'Irvine':{'brush'}, 'Newport': {'lipgloss'}})
 
 
 
    ##################################################
