from goody import type_as_str
import inspect

class Check_All_OK:
    """
    Check_All_OK class implements __check_annotation__ by checking whether each
      annotation passed to its constructor is OK; the first one that
      fails (by raising AssertionError) prints its problem, with a list of all
      annotations being tried at the end of the check_history.
    """
       
    def __init__(self,*args):
        self._annotations = args
        
    def __repr__(self):
        return 'Check_All_OK('+','.join([str(i) for i in self._annotations])+')'

    def __check_annotation__(self, check,param,value,check_history):
        for annot in self._annotations:
            check(param, annot, value, check_history+'Check_All_OK check: '+str(annot)+' while trying: '+str(self)+'\n')


class Check_Any_OK:
    """
    Check_Any_OK implements __check_annotation__ by checking whether at least
      one of the annotations passed to its constructor is OK; if all fail 
      (by raising AssertionError) this classes raises AssertionError and prints
      its failure, along with a list of all annotations tried followed by the
      check_history.
    """
    
    def __init__(self,*args):
        self._annotations = args
        
    def __repr__(self):
        return 'Check_Any_OK('+','.join([str(i) for i in self._annotations])+')'

    def __check_annotation__(self, check,param,value,check_history):
        failed = 0
        for annot in self._annotations: 
            try:
                check(param, annot, value, check_history)
            except AssertionError:
                failed += 1
        if failed == len(self._annotations):
            assert False, repr(param)+' failed annotation check(Check_Any_OK): value = '+repr(value)+\
                         '\n  tried '+str(self)+'\n'+check_history                 



class Check_Annotation:
    # Initially bind the class attribute to True allowing checking to occur (but
    #   only if the object's self._checking_on attribute is also bound to True)
    checking_on  = True
  
    # Initially bind self._checking_on = True, to check the decorated function f
    def __init__(self, f):
        self._f = f
        self._checking_on = True

    # Check whether param's annot is correct for value, adding to check_history
    #    if recurs; defines many local function which use it parameters.  
    def check(self,param,annot,value,check_history=''):
        # Define local functions for checking, list/tuple, dict, set/frozenset,
        #   lambda/functions, and str (str for extra credit)
        # Many of these local functions called by check, call check on their
        #   elements (thus are indirectly recursive)
        
        # Initially compare check's function annotation with its arguments

        def AssertionError_message(error_type,error = None, temp=None):
            # error_type should be either "wrong type", "wrong number of elements", "inconsistency", or "inconsistency_lambda"
            value_type = type_as_str(value)
            annot_type = ''
            if temp in [list, tuple, int, str, float]:
                annot_type = str(annot)[8:-2]
            if error_type == 'wrong type':
                return f"'{param}' failed annotation check(wrong type): value = '{value}'\n  was type {value_type} ...should be type {annot_type}\n" + check_history
            if error_type == 'wrong number of elements':
                return f"'{param}' failed annotation check(wrong number of elements): value = {value}\n  annotation had {len(annot)} elements{annot}\n" + check_history
            if error_type == 'inconsistency':
                return f"'{param}' annotation inconsistency: {str(type(annot))} should have 1 item but had {len(annot)}\n  annotation = {annot}" + check_history
            if error_type == 'inconsistency_lambda_1':
                return f"'{param}' annotation inconsistency: predicate should have 1 parameter but had {len(inspect.signature(annot).parameters.keys())}\n  predicate = {annot}\n" + check_history
            if error_type == 'lambda_annotation':
                return f"'{param}' failed annotation check: value = {value}\n  predicate = {annot}\n" + check_history
            if error_type == 'inconsistency_lambda_2':
                return f"'{param}' annotation inconsistency({annot}) raised exception\n  exception = {type(error)}: {error}" + check_history
            
        if annot == None:
            pass
        elif isinstance(annot, list):
            assert type(value) is type(annot), AssertionError_message('wrong type',temp=list)
            annot_len = len(annot)
            if annot_len == 1:
                for i in value:
                    temp = f"{str(type(annot))[8:-2]}[{value.index(i)}] check: {annot[0]}\n"
                    self.check(param, annot[0], i, check_history+temp)
            else:
                assert len(annot) == len(value), AssertionError_message('wrong number of elements')
                for key1, value1 in zip(annot, value):
                    temp = f"{str(type(annot))[8:-2]}[{value.index(value1)}] check: {key1}\n"
                    self.check(param, key1, value1, check_history+temp)
            
            
        elif isinstance(annot, tuple):
            assert type(value) is type(annot), AssertionError_message('wrong type',temp=tuple)
            annot_len = len(annot)
            if annot_len == 1:
                for i in value:
                    temp = f"{str(type(annot))[8:-2]}[{str(value.index(i))}] check: {annot[0]}\n"
                    self.check(param, annot[0], i, check_history+temp)
            elif annot_len != 1:
                assert len(annot) == len(value), AssertionError_message('wrong number of elements')
                for key1, value1 in zip(annot, value):
                    temp = f"{str(type(annot))[8:-2]}[{value.index(value1)}] check: {key1}\n"
                    self.check(param, key1, value1, check_history+temp)
                    
        elif isinstance(annot, dict):
            annot_len = len(annot)
            assert annot_len == 1, AssertionError_message('inconsistency', temp=dict)
            assert isinstance(value, dict), AssertionError_message("wrong type", temp=dict)
            for i in value:
                annot_key = annot.keys()
                annot_temp = list(annot_key)[0]
                temp = f"dict key check: {annot_temp}\n"
                self.check(param, annot_temp, i, check_history+temp)
            for v1 in value.values():
                annot_value = annot.values()
                annot_temp = list(annot_value)[0]
                temp = f"\n dict key check: {annot_temp}\n"
                self.check(param, annot_temp, v1, check_history+temp)
                
        elif isinstance(annot, set):
            annot_len = len(annot)
            annot_type = type(annot)
            assert annot_len == 1, AssertionError_message('inconsistency')
            assert type(value) is annot_type, f"'{param}' failed annotation check(wrong type): value = {value}\n  was type {type_as_str(value)} ...should be type set\n" + check_history
            annot_temp = list(annot)[0]
            for i in value:
                temp = f"\n set key check: {annot_temp}\n"
                self.check(param, annot_temp, i, check_history+temp)
                
        elif isinstance(annot, frozenset):
            annot_len = len(annot)
            annot_type = type(annot)
            assert annot_len == 1, AssertionError_message('inconsistency')
            assert type(value) is annot_type, f"'{param}' failed annotation check(wrong type): value = {value}\n  was type {type_as_str(value)} ...should be type frozenset\n" + check_history
            annot_temp = list(annot)[0]
            for i in value:
                temp = f"\n set key check: {annot_temp}\n"
                self.check(param, annot_temp, i, check_history+temp)
                
        elif inspect.isfunction(annot):
            sig = inspect.signature(annot)
            lambda_parameter_number = len(sig.parameters.keys())
            assert lambda_parameter_number == 1, AssertionError_message("inconsistency_lambda_1")
            try:
                temp_lambda_function = annot(value)
                assert temp_lambda_function, AssertionError_message('lambda_annotation')
            except Exception as error:
                raise AssertionError(AssertionError_message('inconsistency_lambda_2', error))
            
        elif isinstance(annot, str):
            try:
                assert eval(annot, self.param_arg), f"'{param}' failed annotation check(str predicate '{annot}'\n args for evaluation: {', '.join(self.param_arg)}\n" + check_history
            except AssertionError:
                raise AssertionError
            except:
                raise AssertionError
            
        elif type(annot) is type:
            assert type(value) is annot, AssertionError_message('wrong type', temp=annot)
        else:
            try:
                annot.__check_annotation__(self.check, param, value, check_history)
            except AttributeError:
                raise AssertionError(f"'{param}' annotation undecipherable: {annot}")
            except AssertionError as error:
                raise AssertionError(str(error)+check_history)
            except Exception as error:
                raise AssertionError(f"'{param}' annotation raised exception\n  exception = {error}" + check_history)
    # Return result of calling decorated function call, checking present
    #   parameter/return annotations if required
    def __call__(self, *args, **kargs):

        # Return argument/parameter bindings in an OrderedDict (derived from a
        #   regular dict): bind the function header's parameters with that order
        def param_arg_bindings():
            f_signature  = inspect.signature(self._f)
            bound_f_signature = f_signature.bind(*args,**kargs)
            for param in f_signature.parameters.values():
                if not (param.name in bound_f_signature.arguments):
                    bound_f_signature.arguments[param.name] = param.default
            return bound_f_signature.arguments

        # If annotation checking is turned off at the class or function level
        #   just return the result of calling the decorated function
        # Otherwise do all the annotation checking
        if Check_Annotation.checking_on == False or self._checking_on == False:
            return self._f(*args, **kargs)
        try:
            # For each detected annotation, check it using its argument's value
            annotated_dict = self._f.__annotations__
            self.param_arg = param_arg_bindings()
            for key, value in self.param_arg.items():
                if key in annotated_dict:
                    if annotated_dict[key]:
                        self.check(key, annotated_dict[key], value)
                    
            # Compute/remember the value of the decorated function
            result = self._f(*args, **kargs)
            self.param_arg['_return'] = result
            # If 'return' is in the annotation, check it
            s = 'return'
            if s in annotated_dict:
                self.check(s, annotated_dict[s], result)
            # Return the decorated answer
            return result
            
        # On first AssertionError, print the source lines of the function and reraise 
        except AssertionError:
            # print(80*'-')
            # for l in inspect.getsourcelines(self._f)[0]: # ignore starting line #
            #     print(l.rstrip())
            # print(80*'-')
            raise

if __name__ == '__main__':     
    # an example of testing a simple annotation  
    # def f(x:[lambda x:x>0]): pass
    # f = Check_Annotation(f)
    # f([1,0])

    #driver tests
    import driver
    driver.default_file_name = 'bscp4W22.txt'
#     driver.default_show_exception= True
#     driver.default_show_exception_message= True
#     driver.default_show_traceback= True
    driver.driver()