# A Hunter class is derived from a Pulsator and then Mobile_Simulton base.
#   It inherits updating+displaying from Pusator/Mobile_Simulton: it pursues
#   any close prey, or moves in a straight line (see Mobile_Simultion).


from prey  import Prey
from pulsator import Pulsator
from mobilesimulton import Mobile_Simulton
from math import atan2


class Hunter(Pulsator, Mobile_Simulton):  
    dis = 200
    speed = 5
    def __init__(self,x,y):
        Pulsator.__init__(self, x, y)
        self.randomize_angle()
        w, h = self.get_dimension()
        Mobile_Simulton.__init__(self, x, y, w, h, self._angle, Hunter.speed)
        
    def update(self, model):
        prey_set = Pulsator.update(self, model)
        eaten_prey = model.find(lambda s: isinstance(s, Prey) and self.distance(s.get_location()) <= Hunter.dis)
        temp = list()
        target = None
        if len(eaten_prey) != 0:
            for i in eaten_prey:
                temp.append((i, self.distance(i.get_location())))
            temp = sorted(temp, key=lambda s: s[1])
            target = temp[0][0]
            y_dif = target._y - self._y
            x_dif = target._x - self._x
            self.set_angle(atan2(y_dif, x_dif))
        self.move()
        return eaten_prey