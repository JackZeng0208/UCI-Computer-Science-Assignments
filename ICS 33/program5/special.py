# Special prey will randomly change its color in every 15 cycles
from prey import Prey
from random import shuffle

class Special(Prey):
    def __init__(self, x, y):
        self.randomize_angle()
        self._time_count = 0
        self._all_color = ['yellow', 'black', 'green', 'purple', 'grey', 'blue', 'red', 'orange']
        Prey.__init__(self, x, y, 16, 16, self._angle, 7)
        self.color = 'red'
    
    def update(self, model):
        self.move()
        self._time_count += 1
        if self._time_count == 15:
            shuffle(self._all_color)
            self.color = self._all_color[-1]
            self._time_count = 0
    
    def display(self, canvas):
        canvas.create_oval(self._x-8, self._y-8, self._x+8, self._y+8, fill=self.color)