# A Pulsator is a Black_Hole; it updates as a Black_Hole
#   does, but also by growing/shrinking depending on
#   whether or not it eats Prey (and removing itself from
#   the simulation if its dimension becomes 0), and displays
#   as a Black_Hole but with varying dimensions 


from blackhole import Black_Hole


class Pulsator(Black_Hole): 
    def __init__(self, x, y):
        Black_Hole.__init__(self, x, y)
        self._time_between_meal = 0
        
    def update(self, model):
        self._time_between_meal += 1
        eaten_prey = Black_Hole.update(self, model)
        prey_num = len(eaten_prey)
        if self._time_between_meal == 30:
            self.change_dimension(-1,-1)
            if self._height == 0 or self._width == 0:
                model.remove(self)
            self._time_between_meal = 0
        elif prey_num != 0:
            self._time_between_meal = 0
            self.change_dimension(prey_num, prey_num)
        return eaten_prey
