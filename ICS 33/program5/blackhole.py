# A Black_Hole is derived from a Simulton base; it updates by finding+removing
#   any objects (derived from a Prey base) whose center is crosses inside its
#   radius (and returns a set of all eaten simultons); it displays as a black
#   circle with a radius of 10 (e.g., a width/height 20).
# Calling get_dimension for the width/height (for containment and displaying)'
#   will facilitate inheritance in Pulsator and Hunter

from simulton import Simulton
from prey import Prey


class Black_Hole(Simulton):  
    radius = 10
    color = 'black'
    def __init__(self, x, y):
        Simulton.__init__(self, x, y, Black_Hole.radius*2, Black_Hole.radius*2)
    
    def contains(self,xy):
        return self.distance(xy) <= self._width/2
    
    def update(self, model):
        eaten_prey = set()
        for i in model.find(lambda s: isinstance(s, Prey)):
            if self.contains(i.get_location()):
                eaten_prey.add(i)
        for i in eaten_prey:
            model.remove(i)
        return eaten_prey
    
    def display(self, canvas):
        canvas.create_oval(self._x-self._width/2, self._y-self._height/2, self._x+self._width/2, self._y+self._height/2, fill=self.color)
