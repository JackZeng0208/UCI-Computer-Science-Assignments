import re

# Before running the driver on the bsc.txt file, ensure you have put a regular
#   expression pattern in the files repattern1a.txt, repattern1b.txt, and
#   repattern2a.txt. The patterns must be all on the first line

# Rewrite
def expand_re(pat_dict:{str:str}):
    for key in pat_dict:
        for key2 in pat_dict:
            if key not in pat_dict[key2]:
                continue
            elif key in pat_dict[key2]:
                temp = "#" + str(key)+ "#"
                pat_value = pat_dict[key2]
                patterns = re.compile(temp)
                value = key.strip("#")
                temp1 = pat_dict[value]
                input_string = '(?:'+ str(temp1) +')'
                temp2 = re.sub(patterns, input_string, pat_dict[key2])
                pat_dict[key2] = temp2
            else:
                continue
                





def match_params_args(params_string : str, args_string : str, trace = False) -> {str:int}:
        result = dict()
        para = list(params_string)
        arg = list(args_string)
        try:
            if(len(para) == len(arg)):
                for i in range(0, len(para)):
                    result.update({para[i]:int(arg[i])})
        except:
            return AssertionError
        return result
            






if __name__ == '__main__':
    
    p1a = open('repattern1a.txt').read().rstrip() # Read pattern on first line
    print('Testing the pattern p1a: ',p1a)
    for text in open('bm1.txt'):
        text = text.rstrip()
        print('Matching against:',text)
        m = re.match(p1a,text)
        print(' ','Matched' if m != None else "Not matched")
         
    p1b = open('repattern1b.txt').read().rstrip() # Read pattern on first line
    print('\nTesting the pattern p1b: ',p1b)
    for text in open('bm1.txt'):
        text = text.rstrip()
        print('Matching against:',text)
        m = re.match(p1b,text)
        print('  ','Matched with groups ='+ str(m.groups()) if m != None else 'Not matched' )
        print('  ','Matched with names  ='+ str(m.groupdict()) if m != None else 'Not matched' )
         
         
    p2 = open('repattern2.txt').read().rstrip() # Read pattern on first line
    print('\nTesting the pattern p2: ',p2)
    for text in open('bm2.txt'):
        text = text.rstrip()
        print('Matching against:',text)
        m = re.match(p2,text)
        print(' ','Matched' if m != None else "Not matched")
         
 
    print('\nTesting expand_re')
    pd = dict(digit = r'[0-9]', integer = r'[+-]?#digit##digit#*')
    print('  Expanding ',pd)
    expand_re(pd)
    print('  result =',pd)
    # produces/prints the dictionary
    # {'digit': '[0-9]', 'integer': '[+-]?(?:[0-9])(?:[0-9])*'}
     
    pd = dict(integer       = r'[+-]?[0-9]+',
              integer_range = r'#integer#(..#integer#)?',
              integer_list  = r'#integer_range#(,#integer_range#)*',
              integer_set   = r'{#integer_list#?}')
    print('\n  Expanding ',pd)
    expand_re(pd)
    print('  result =',pd)
    # produces/prints the dictionary 
    # {'integer': '[+-]?[0-9]+',
    #  'integer_range': '(?:[+-]?[0-9]+)(..(?:[+-]?[0-9]+))?',
    #  'integer_list': '(?:(?:[+-]?[0-9]+)(..(?:[+-]?[0-9]+))?)(,(?:(?:[+-]?[0-9]+)(..(?:[+-]?[0-9]+))?))*',
    #  'integer_set': '{(?:(?:(?:[+-]?[0-9]+)(..(?:[+-]?[0-9]+))?)(?,(?:(?:[+-]?[0-9]+)(..(?:[+-]?[0-9]+))?))*)?}'
    # }
     
    pd = dict(a='correct',b='#a#',c='#b#',d='#c#',e='#d#',f='#e#',g='#f#')
    print('\n  Expanding ',pd)
    expand_re(pd)
    print('  result =',pd)
    # produces/prints the dictionary 
    # {'a': 'correct',
    #  'b': '(?:correct)',
    #  'c': '(?:(?:correct))',
    #  'd': '(?:(?:(?:correct)))',
    #  'e': '(?:(?:(?:(?:correct))))',
    #  'f': '(?:(?:(?:(?:(?:correct)))))',
    #  'g': '(?:(?:(?:(?:(?:(?:correct))))))'
    # }


    p4a = open('repattern4a.txt').read().rstrip() # Read pattern on first line
    print('\nTesting the pattern p4a: ',p4a)
    for text in open('bm4a.txt'):
        text = text.rstrip()
        print('Matching against:',text)
        m = re.match(p4a,text)
        print('  ','Matched with groupdict ='+ str(m.groupdict()) if m != None else 'Not matched' )


    p4b = open('repattern4b.txt').read().rstrip() # Read pattern on first line
    print('\nTesting the pattern p4b: ',p4b)
    for text in open('bm4b.txt'):
        text = text.rstrip()
        print('Matching against:',text)
        m = re.match(p4b,text)
        print('  ','Matched with groupdict ='+ str(m.groupdict()) if m != None else 'Not matched' )


    print('\nTesting match_params_args')
    print('\nTesting name-only parameters and positional arugments')
    print(match_params_args('',''))
    print(match_params_args('a','1'))
    print(match_params_args('a,b,c','1,2,3'))
    try:
        match_params_args('a,b','1,2,3')
        print('Should have raised AssertionError')
    except AssertionError as exc:
        print('***Raised AssertionError:',exc)
    try:
        match_params_args('a,b,c','1,2')
        print('Should have raised AssertionError')
    except AssertionError as exc:
        print('***Raised AssertionError:',exc)
        
    print('\nTesting includes *name-only parameters too')
    print(match_params_args('*args',''))
    print(match_params_args('*args','1'))
    print(match_params_args('*args','1,2'))
    print(match_params_args('a,b,*args','1,2'))
    print(match_params_args('a,b,*args','1,2,3,4'))
        
    print('\nTesting includes named arguments')
    print(match_params_args('a,b,*args,c,d','1,2,3,4,c=5,d=6',True))
    try:
        print(match_params_args('a,b,*args,c,d','1,2,3,4,d=5'))
        print('Should have raised AssertionError')
    except AssertionError as exc:
        print('***Raised AssertionError:',exc)
    try:
        print(match_params_args('a,b,*args,c,d','1,2,3,4,x=5'))
        print('Should have raised AssertionError')
    except AssertionError as exc:
        print('***Raised AssertionError:',exc)
    try:
        print(match_params_args('a,b,*args,c,d','1,2,3,4,c=5,d=6,a=7'))
        print('Should have raised AssertionError')
    except AssertionError as exc:
        print('***Raised AssertionError:',exc)
    
    print('\nTesting includes default-value parameters')
    print(match_params_args('a=1,b=2,c=4','c=5,b=6,a=7'))
    print(match_params_args('a,b,*args,c=5,d=6','1,2,3,4,d=5',True))
    try:
        print(match_params_args('a,b','b=2'))
        print('Should have raised AssertionError')
    except AssertionError as exc:
        print('***Raised AssertionError:',exc)


        
    print()
    print()
    import driver
    driver.default_file_name = "bscq2W22.txt"
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()
