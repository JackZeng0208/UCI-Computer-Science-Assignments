import prompt 
from goody       import safe_open,irange
from collections import defaultdict # Use defaultdict for prefix and query


def all_prefixes(fq : (str,)) -> {(str,)}:
    result = set()
    length = len(fq)+1
    for i in range(1, length):
        result.add(tuple(fq[:i]))
    return result


def add_query(prefix : {(str,):{(str,)}}, query : {(str,):int}, new_query : (str,)) -> None:
    temp = all_prefixes(new_query)
    for i in temp:
        prefix[i].add(new_query)
    query[new_query] += 1
    return None


def read_queries(open_file : open) -> ({(str,):{(str,)}}, {(str,):int}):
    query = defaultdict(int)
    prefix = defaultdict(set)
    temp = ''
    for lines in open_file:
        temp = lines.rstrip('\n').split(" ")
        add_query(prefix, query, tuple(temp))
    return(prefix,query)


def dict_as_str(d : {None:None}, key : callable=None, reverse : bool=False) -> str:
    result = ''
    for i in sorted(d.keys(), key=key, reverse=reverse):
        result += ('  '+ str(i) + ' -> ' + str(d[i])+'\n')
    return result

def top_n(a_prefix : (str,), n : int, prefix : {(str,):{(str,)}}, query : {(str,):int}) -> [(str,)]:
    result = list()
    temp = list()
    if a_prefix not in prefix:
        return result
    else:
        temp = sorted(query, key= lambda x: (-query[x],x))
        for i in temp:
            if i in prefix[a_prefix]:
                result.append(i)
            if len(result) == n:
                break
        return result
    return result
    




# Script

if __name__ == '__main__':
    # Write script here
    file = open(input("Select a file storing multiple full queries: "))
    prefix, query = read_queries(file)
    print()
    while True:
        print("Prefix dictionary: ")
        print(dict_as_str(prefix, key = lambda i : (len(i), i)))
        print("Query dictionary: ")
        print(dict_as_str(query, key = lambda i : (-query[i],i)))
        single_prefix = input("Select a single prefix sequence (or done to stop): ")
        if single_prefix == 'done':
            break
        temp = tuple(single_prefix.split(" "))
        print("  Matching full queries (up to 3; High->Low frequency) = " + str(top_n(temp, 3, prefix, query)))
        full_query = input("Select a single full query sequence (or done to stop): ")
        print()
        if full_query == 'done':
            break
        temp1 = tuple(full_query.split(" "))
        add_query(prefix, query, temp1)
    # For running batch self-tests
    print()
    import driver
    driver.default_file_name = "bsc5.txt"
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()
