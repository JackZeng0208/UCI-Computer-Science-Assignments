import prompt
from goody       import safe_open
from math        import ceil 
from collections import defaultdict

def read_graph(open_file : open) -> {str:{str}}:
    result_dict = defaultdict(set)
    link = []
    for i in open_file:
        temp = i.rstrip("\n")
        if ";" in i:
            link = temp.split(";")
            result_dict[link[0]].add(link[1])
            result_dict[link[1]].add(link[0])
        elif ";" not in i:
            result_dict[temp] = set()
            continue
    result = dict(result_dict)
    return result
            


def graph_as_str(graph : {str:{str}}) -> str:
    result = ''
    temp1 = ''
    temp2 = []
    for i, person in sorted(graph.items()):
        temp1 = str(sorted(list(person)))
        result += ('  ' + str(i) + " -> " + temp1 + '\n')
    return result
    


def find_influencers(graph : {str:{str}}, trace : bool = False) -> {str}:
    result = set()
    infl_dict = dict()
    for i, person in graph.items():
        number_of_friends = len(person)
        if number_of_friends == 0:
            infl_dict[i] = [-1, number_of_friends, i]
        else:
            infl_dict[i] = [number_of_friends - ceil(number_of_friends/2), number_of_friends, i]
    
    def set_list(dic):
        return [(value[0], value[1], value[2]) for value in dic.values() if value[0] > -1]
    
    lists = set_list(infl_dict)
    lists_length = len(lists)
    
    while bool(lists):
        minimum_tuple = min(lists) 
        if trace == True:
            print(f'influencer dictionary = {infl_dict}')
            print(f'removal candidates = {lists}')
            print(f'{minimum_tuple} is the smallest candidate')
            print(f'Removing {minimum_tuple[2]} as key from influencer dictionary; decrementing every friend\'s value')
        
        del infl_dict[minimum_tuple[2]]
        for i in graph[minimum_tuple[2]]:
            if i in infl_dict:
                infl_dict[i][0] -= 1
                infl_dict[i][1] -= 1
                
        lists = set_list(infl_dict)
        lists_length = len(lists)
        
    if trace == True:    
        print(f'influencer dictionary = {infl_dict}')
        print(f'removal candidates = {lists}')
        
    for i in infl_dict.keys():
        result.add(i)
    return result
 
def all_influenced(graph : {str:{str}}, influencers : {str}) -> {str}:
    influenced_dict = dict()
    result = set()
    
    def friend_number(length):
        return ceil(length/2)
    
    for i in graph.keys():
        if i in influencers:
            influenced_dict.update({i:True})
        elif i not in influencers:
            influenced_dict.update({i:False})
    while True:
        flag = False
        for i in influenced_dict.keys():
            if influenced_dict[i] == True:
                continue
            elif influenced_dict[i] == False:
                if(len(graph[i])== 0):
                    continue
                influenced_friend = 0
                for j in graph[i]:
                    if(influenced_dict[j] == True):
                        influenced_friend += 1
                if friend_number(len(graph[i])) <= influenced_friend:
                    flag = True
                    influenced_dict[i] = True
        if flag == False:
            for i in influenced_dict.keys():
                if influenced_dict[i] == True:
                    result.add(i)
            return result
            
    
if __name__ == '__main__':
    # Write script here
    user_input = ""
    file = open(input("Select a file storing a friendship graph: "))
    output_graph = read_graph(file)
    print(f"Graph: person -> [sorted friends of person]")
    print(f'{graph_as_str(output_graph)}')
    tracing = bool(input("Select Tracing of Execution[True]: ") or True)
    temp = find_influencers(output_graph, tracing)
    print()
    print(f"The influencers set calculated is {temp}")
    while True:
        user_input = input(f"Select a subset of persons (or enter done to stop)[{str(temp)}]: ")
        user_input.strip()
        if user_input == 'done':
            break
        elif user_input == '':
            user_input = temp
        try:
            temp1 = all_influenced(output_graph, user_input)
            temp2 = str(user_input)
        except:
            print(f' Entry Error:{temp2}')
            print('  Please enter a legal String')
        else:
            print(f'People influenced by selected subset ({len(temp1)/len(output_graph)*100}% of graph) = {temp1}\n')
    # For running batch self-tests
    print()
    import driver
    driver.default_file_name = "bsc1.txt"
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()

