import prompt
import goody

# Use these global variables to index the list associated with each name in the dictionary.
# e.g., if men is a dictionary, men['m1'][match] is the woman who matches man 'm1', and 
# men['m1'][prefs] is the list of preference for man 'm1'.
# It would seems that this list might be better represented as a named tuple, but the
# preference list it contains is mutated, which is not allowed in a named tuple. 

match = 0   # Index 0 of list associate with name is match (str)
prefs = 1   # Index 1 of list associate with name is preferences (list of str)


def read_match_preferences(open_file : open) -> {str:[str,[str]]}:
    result = dict()
    for lines in open_file:
        p = [None]
        temp = lines.rstrip('\n').split(';')
        p.append(temp[1:])
        result[temp[0]] = p
    return result


def dict_as_str(d : {str:[str,[str]]}, key : callable=None, reverse : bool=False) -> str:
    result = ''
    for i in sorted(d.keys(), key=key, reverse=reverse):
        result += ('  '+ str(i) + ' -> ' + str(d[i])+'\n')
    return result


def who_prefer(order : [str], p1 : str, p2 : str) -> str:
    if(order.index(p1) > order.index(p2)):
        return p2
    else:
        return p1


def extract_matches(men : {str:[str,[str]]}) -> {(str,str)}:
    result = set()
    for key, value in men.items():
        result.add((key,value[match]))
    return result

def make_match(men : {str:[str,[str]]}, women : {str:[str,[str]]}, trace : bool = False) -> {(str,str)}:
    only_men_copy = men.copy()
    unmatched_men_set = set()
    
    if trace == True:
        print("Women Preferences (unchanging)")
        print(dict_as_str(women))
        
    for i in only_men_copy.keys():
        unmatched_men_set.add(i)
        
    while bool(unmatched_men_set):
        if trace == True:
            print("Men Preferences (current)")
            print(dict_as_str(men))
            print(f"unmatched men = {unmatched_men_set}")
            print()
            
        removed_man = unmatched_men_set.pop()
        highest_female = only_men_copy[removed_man][prefs].pop(match)
        
        if women[highest_female][match] == None:
            only_men_copy[removed_man][match] = highest_female
            women[highest_female][match] = removed_man
            if trace == True:
                print(f"{removed_man} proposes to {highest_female}, who is an unmatched woman; so she accepts the proposal")
                print()
        else:
            most_prefer = who_prefer(women[highest_female][prefs], women[highest_female][match], removed_man)
            if most_prefer == removed_man:
                unmatched_men_set.add(women[highest_female][match])
                only_men_copy[removed_man][match] = highest_female
                women[highest_female][match] = removed_man
                if trace == True:
                    print(f"{removed_man} proposes to {highest_female}, who is an unmatched woman; so she accepts the proposal")
                    print()  
            else:
                unmatched_men_set.add(removed_man)
                if trace == True:
                    print(f"{removed_man} proposes to {highest_female}, who is a matched woman who prefers her current match; so she rejects the proposal")
                    print()
                
    return extract_matches(only_men_copy)


  
    
if __name__ == '__main__':
    # Write script here
    men = input("Select a file storing the preferences of the men: ")
    women = input("Select a file storing the preferences of the women: ")
    print()
    men_file = open(men)
    women_file = open(women)
    men_preferences = read_match_preferences(men_file)
    women_preferences = read_match_preferences(women_file)
    print("Men Preferences")
    print(dict_as_str(men_preferences))
    print("Women Preferences")
    print(dict_as_str(women_preferences))
    tracing = bool(input("Select Tracing of Execution[True]: ") or True)
    print()
    print(f"The calculated matches are {make_match(men_preferences, women_preferences, tracing)}")
    # For running batch self-tests
    print()
    import driver
    driver.default_file_name = "bsc2.txt"
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()
