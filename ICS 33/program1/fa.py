import goody


def read_fa(file : open) -> {str:{str:str}}:
    result = dict()
    for lines in file:
        temp = lines.rstrip('\n').split(';')
        temp1 = zip(temp[1::2], temp[2::2])
        result[temp[0]] = {i:j for i, j in temp1}
    return result

def fa_as_str(fa : {str:{str:str}}) -> str:
    result = ''
    for key, value in sorted(fa.items()):
        result += ('  ' + str(key) + ' transitions: ' + str([(key2, value2) for key2,value2 in sorted(value.items())]) +'\n')      
    return result

    
def process(fa : {str:{str:str}}, state : str, inputs : [str]) -> [None]:
    result = [state]
    temp = state
    for i in inputs:
        if i in fa[state]:
            temp = fa[temp][i]
            result.append((i,temp))
        elif i not in fa[state]:
            result.append((i,None))
            break
    return result

def interpret(fa_result : [None]) -> str:
    result = ('Start state = '+ fa_result[0] + '\n')
    for i, j in fa_result[1:]:
        if j == None:
            result += ('  Input = ' + str(i))
            result += ('; illegal input: simulation terminated\n')
            result += 'Stop state = None\n'
            return result
        else:
            result += ('  Input = ' + str(i) + '; new state = ' + str(j) +'\n')
    result += ('Stop state = ' + str(j) + '\n')
    return result




if __name__ == '__main__':
    # Write script here
    file = open(input("Select a file storing the Finite Automaton: "))
    print()
    print("Finite Automaton: sorted states (str) and sorted lists of transitions [(str,str)]")
    file_dict = read_fa(file)
    print(fa_as_str(file_dict))
    file1 = open(input("Select a file with each line showing a start-state and its inputs: "))
    print()
    for lines in file1:
        lists = lines.rstrip('\n').split(';')
        print(f'Calculated trace of FA from its start-state\n{interpret(process(file_dict, lists[0], lists[1:]))}')
    # For running batch self-tests
    print()
    import driver
    driver.default_file_name = "bsc3.txt"
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()
