import goody
from collections import defaultdict

def read_ndfa(file : open) -> {str:{str:{str}}}:
    result = dict()
    def dictionary(l):
        temp = defaultdict(set)
        for i, j in l:
            temp[i].add(j)
        return dict(temp)
    
    for lines in file:
        temp = lines.rstrip('\n').split(';')
        temp1 = zip(temp[1::2], temp[2::2])
        result.update({temp[0]:dictionary(temp1)})
    return result


def ndfa_as_str(ndfa : {str:{str:{str}}}) -> str:
    result = ''
    for key, value in sorted(ndfa.items()):
        result += ('  ' + str(key) + ' transitions: ' + str([(key2, sorted(value2)) for key2,value2 in sorted(value.items())]) +'\n')      
    return result

       
def process(ndfa : {str:{str:{str}}}, state : str, inputs : [str]) -> [None]:
    result = [state]
    temp = {state}
    for i in inputs:
        temp1 = set()
        for j in temp:
            if i in ndfa[j] and bool(ndfa[j]):
                temp2 = ndfa[j][i]
                temp1.update(temp2)
            elif not bool(ndfa[j]):
                continue
        result.append((i,temp1))
        if not bool(temp1):
            break
        temp = temp1
    return result


def interpret(result : [None]) -> str:
    output_string = ('Start state = '+ result[0] + '\n')
    for i, j in result[1:]:
        output_string += ('  Input = ' + str(i) + '; new possible states = ' + str(sorted(j)) +'\n')
    output_string += ('Stop state(s) = ' + str(sorted(result[-1][1])) + '\n')
    return output_string
    





if __name__ == '__main__':
    # Write script here
    file = open(input("Select a file storing a Non-Deterministic Finite Automaton: "))
    print()
    print("Non-Determinisitc Finite Automaton: sorted states (str) and sorted lists of transitions [(str,[str])]")
    file_dict = read_ndfa(file)
    print(ndfa_as_str(file_dict))
    file1 = open(input("Select a file with each line showing a start-state and its inputs: "))
    print()
    for lines in file1:
        lists = lines.rstrip('\n').split(';')
        print(f'Calculated trace of NDFA from its start-state\n{interpret(process(file_dict, lists[0], lists[1:]))}')
    # For running batch self-tests
    print()
    import driver
    driver.default_file_name = "bsc4.txt"
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()
