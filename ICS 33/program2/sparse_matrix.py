import prompt
from collections import defaultdict

class Sparse_Matrix:

    # I have written str(...) because it is used in the bsc.txt file and
    #   it is a bit subtle to get correct. This function does not depend
    #   on any other method in this class being written correctly, although
    #   it could be simplified by writing self[...] which calls __getitem__.   
    
    def __init__(self, *args):
        self.matrix = defaultdict(int)
        appeared_point = list()
        for i in range(len(list(args))):
            if i == 0 or i == 1:
                assert type(args[i]) == int, "row or column should be integer"
                assert args[i] > 0, "row or column should be positive"
            if i > 1:
                assert type(args[i][0]) == int and type(args[i][1]) == int, "coordinate should be integer"
                assert args[i][0] >= 0 and args[i][0] < args[0], "x-coordinate should be within the range of row"
                assert args[i][1] >= 0 and args[i][1] < args[1], "y-coordinate should be within the range of column"
                assert type(args[i][2]) == int or type(args[i][2]) == float, "value should be either integer or float"
                temp = tuple(args[i][0:2])
                if temp in appeared_point:
                    raise AssertionError(f'Sparse_Matrix.__init__: repeated index {args[i][0:2]}')
                elif temp not in appeared_point:
                    if args[i][2] != 0:
                        appeared_point.append(temp)
                        self.matrix[temp] = args[i][2]
        self.rows = args[0]
        self.cols = args[1]
        temp_matrix = dict(self.matrix)
        self.matrix = temp_matrix
                    
    def size(self):
        return (self.rows, self.cols)
    
    def __len__(self):
        return self.rows*self.cols
    
    def __bool__(self):
        if not self.matrix:
            return False
        else:
            return True
    
    def __repr__(self):
        tuple_str = ''
        for key, value in self.matrix.items():
            tuple_str += f'({key[0]},{key[1]},{value}),'
        tuple_str = tuple_str[:-1]
        return f'Sparse_Matrix({str(self.rows)},{str(self.cols)},{tuple_str})'
    
    def __getitem__(self, position: tuple):
        if len(position) != 2:
            raise TypeError("Not enough data")
        if(position[0] < 0 or position[0] >= self.rows):
            raise TypeError("x-coordinate does not in the range of row")
        if(position[1] < 0 or position[1] >= self.cols):
            raise TypeError("y-coordinate does not in the range of column")
        if not(type(position[0]) == int):
            raise TypeError("x-coordinate is not integer")
        if not(type(position[1]) == int):
            raise TypeError("y-coordinate is not integer")
        if position not in self.matrix.keys():
            return 0
        else:
            return self.matrix[position]
    
    def __setitem__(self,position,value):
        if len(position) != 2:
            raise TypeError("Not enough data")
        if(position[0] < 0 or position[0] >= self.rows):
            raise TypeError("x-coordinate does not in the range of row")
        if(position[1] < 0 or position[1] >= self.cols):
            raise TypeError("y-coordinate does not in the range of column")
        if not(type(position[0]) == int):
            raise TypeError("x-coordinate is not integer")
        if not(type(position[1]) == int):
            raise TypeError("y-coordinate is not integer")
        if not(type(value) == int or type(value) == float):
            raise TypeError("Value is not integer or float")
        if value == 0:
            if position in self.matrix.keys():
                self.matrix.pop(position)
        else:
            self.matrix[position] = value
        
        
    def __delitem__(self, position):
        if len(position) != 2:
            raise TypeError("Not enough data")
        if(position[0] < 0 or position[0] >= self.rows):
            raise TypeError("x-coordinate does not in the range of row")
        if(position[1] < 0 or position[1] >= self.cols):
            raise TypeError("y-coordinate does not in the range of column")
        if not(type(position[0]) == int):
            raise TypeError("x-coordinate is not integer")
        if not(type(position[1]) == int):
            raise TypeError("y-coordinate is not integer")
        self.__setitem__(position, 0)
    
    def row(self, argument):
        assert type(argument) != float and type(argument) != True, "Parameter is not integer"
        try:
            new_argument = int(argument)
            assert 0 <= new_argument < self.rows, "Parameter is not in the range"
        except:
            raise AssertionError("Parameter is not integer")
        result = list()
        for i in range(self.cols):
            result.append(0)
        for key, value in self.matrix.items():
            if key[0] == new_argument:
                result[key[1]] = value
        result_tuple = tuple(result)
        return result_tuple
    
    def col(self, argument):
        assert type(argument) != float and type(argument) != True, "Parameter is not integer"
        try:
            new_argument = int(argument)
            assert 0 <= new_argument < self.cols, "Parameter is not in the range"
        except:
            raise AssertionError("Parameter is not integer")
        result = list()
        for i in range(self.rows):
            result.append(0)
        for key, value in self.matrix.items():
            if key[1] == new_argument:
                result[key[0]] = value
        result_tuple = tuple(result)
        return result_tuple
    
    def details(self):
        tuple_of_row = list()
        for i in range(self.rows):
            tuple_of_row.append(self.row(i))
        tuple_of_row = tuple(tuple_of_row)
        return f'{self.rows}x{self.cols} -> {self.matrix} -> {tuple_of_row}'
    
    def __call__(self, new_row: int, new_column: int):
        if type(new_row) != int and type(new_column) != int and new_row < 0 and new_column < 0:
            raise AssertionError("new_row or new_column does not satisfy the requirement")
        new_matrix = dict()
        for key, value in self.matrix.items():
            if key[0] < new_row and key[1] < new_column:
                new_matrix[key] = value
        self.matrix = new_matrix
        self.rows = new_row
        self.cols = new_column
    
    def __iter__(self):
        temp = list()
        for key in self.matrix.keys():
            temp.append((key[0],key[1],self.matrix[key]))
        sorted_list = sorted(temp, key=lambda i: i[2])
        for tuples in sorted_list:
            yield tuples
    
    def __pos__(self):
        return Sparse_Matrix(self.rows, self.cols, *[(key[0], key[1], self.matrix[key]) for key in self.matrix.keys()])
    
    def __neg__(self):
        return Sparse_Matrix(self.rows, self.cols, *[(key[0], key[1], -self.matrix[key]) for key in self.matrix.keys()])
    
    def __abs__(self):
        return Sparse_Matrix(self.rows, self.cols, *[(key[0], key[1], abs(self.matrix[key])) for key in self.matrix.keys()])
    
    def __add__(self, other_element):
        result_matrix = Sparse_Matrix(self.rows, self.cols, *[(key[0], key[1], self.matrix[key]) for key in self.matrix.keys()])
        if type(other_element) == Sparse_Matrix:
            if other_element.rows != self.rows or other_element.cols != self.cols:
                raise AssertionError
            for key,value in other_element.matrix.items():
                result_matrix[key] += value
            return result_matrix
        elif type(other_element) == int or type(other_element) == float:
            for key, value in self.matrix.items():
                result_matrix[key] += other_element
            return result_matrix
        else:
            raise TypeError
    
    def __radd__(self, other_element):
        return self.__add__(other_element)
    
    def __sub__(self, other_element):
        if type(other_element) == Sparse_Matrix:
            if other_element.rows != self.rows or other_element.cols != self.cols:
                raise AssertionError
            return self + (-other_element)
        elif type(other_element) == int or type(other_element) == float:
            return self + (-other_element)
        else:
            raise TypeError
    
    def __rsub__(self, other_element):
        if type(other_element) == Sparse_Matrix:
            if other_element.rows != self.rows or other_element.cols != self.cols:
                raise AssertionError
            return self + (-other_element)
        elif type(other_element) == int or type(other_element) == float:
            return -(self + (-other_element))
        else:
            raise TypeError
    
    def __mul__(self, other_element):
        if type(other_element) == Sparse_Matrix:
            result_matrix = Sparse_Matrix(self.rows, other_element.cols)
            if other_element.rows != self.cols or other_element.cols != self.rows:
                raise AssertionError
            for r in range(self.rows):
                for c in range(other_element.cols):
                    temp_coordinate = (r,c)
                    temp_sum = 0
                    self_row = self.row(r)
                    other_column = other_element.col(c)
                    for i in range(len(self_row)):
                        temp_sum += self_row[i]*other_column[i]
                    result_matrix[temp_coordinate] = temp_sum
            return result_matrix
        elif type(other_element) == int or type(other_element) == float:
            result_matrix = Sparse_Matrix(self.rows, self.cols, *[(key[0], key[1], self.matrix[key]*other_element) for key in self.matrix.keys()])
            return result_matrix
        else:
            raise TypeError
    
    def __rmul__(self, other_element):
        return self.__mul__(other_element)
                    
    def __pow__(self, other_element):
        if self.rows != self.cols or other_element < 1:
            raise AssertionError
        if type(other_element) != int:
            raise TypeError
        result_matrix = Sparse_Matrix(self.rows, self.cols, *[(key[0], key[1], self.matrix[key]) for key in self.matrix.keys()])
        for i in range(1, other_element):
            result_matrix *= self
        return result_matrix
    
    def __eq__(self, other_element):
        try:
            if type(other_element) == Sparse_Matrix:
                if other_element.rows != self.rows or other_element.cols != self.cols:
                    return False
                for key in self.matrix.keys():
                    if self.matrix[key] != other_element.matrix[key]:
                        return False
                for key in other_element.matrix.keys():
                    if self.matrix[key] != other_element.matrix[key]:
                        return False
            elif type(other_element) == int or type(other_element) == float:
                result_matrix = Sparse_Matrix(self.rows, self.cols, *[(key1, key2, other_element) for key1 in range(self.rows) for key2 in range(self.cols)])
                if result_matrix == self:
                    return True
                else:
                    return False
            else:
                return False   
        except:
            return False
        return True
    
    def __setattr__(self, name, value):
        try:
            self.__dict__[name] = value
        except:
            raise AssertionError
            
            
    def __str__(self):
        size = str(self.rows)+'x'+str(self.cols)
        width = max(len(str(self.matrix.get((r,c),0))) for c in range(self.cols) for r in range(self.rows))
        return size+':['+('\n'+(2+len(size))*' ').join ('  '.join('{num: >{width}}'.format(num=self.matrix.get((r,c),0),width=width) for c in range(self.cols))\
                                                                                             for r in range(self.rows))+']'





if __name__ == '__main__':
    
    #Simple tests before running driver
    #Put your own test code here to test Sparse_Matrix before doing the bsc tests
    #Debugging problems with these tests is simpler
    
    import driver
    driver.default_file_name = 'bscp22W22.txt'
#     driver.default_show_exception = prompt.for_bool('Show exceptions when testing',True)
#     driver.default_show_exception_message = prompt.for_bool('Show exception messages when testing',True)
#     driver.default_show_traceback = prompt.for_bool('Show traceback when testing',True)
    driver.driver()
