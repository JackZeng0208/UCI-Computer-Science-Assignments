from collections import defaultdict
from goody import type_as_str
import prompt

class Bag:
    def __init__(self, *args):
        self._bag = defaultdict(int)
        if not args:
            return None
        else:
            for i in args[0]:
                self._bag[i] += 1
    
    def __repr__(self):
        temp = list()
        temp_str = ''
        result = 'Bag()'
        if not self._bag:
            return result
        for i in self._bag:
            for j in range(self._bag[i]):
                temp.append(i)
        temp_str = str(temp)
        result = f"Bag({temp_str})"
        return result
    
    def __str__(self):
        temp_str = ''
        for key, value in self._bag.items():
            temp_str += f"{key}[{value}],"
        temp_str = temp_str[:-1]
        return f'Bag({temp_str})'

    def __len__(self):
        result = 0
        for i in self._bag:
            result += self._bag[i]
        return result
    
    def unique(self):
        result = 0
        for i in self._bag:
            result += 1
        return result
    
    def __contains__(self, argument):
        if argument in self._bag.keys():
            return True
        elif argument not in self._bag.keys():
            return False
    
    def count(self, argument):
        if argument in self._bag.keys():
            return self._bag[argument]
        else:
            return 0
    
    def add(self, argument):
        if argument in self._bag.keys():
            self._bag[argument] += 1
        else:
            self._bag[argument] = 1
    
    def __add__(self, other_bag):
        result = list()
        if type(other_bag) != Bag:
            raise TypeError
        else:
            for i in self._bag:
                for j in range(self._bag[i]):
                    result.append(i)
            for i in other_bag._bag:
                for j in range(other_bag._bag[i]):
                    result.append(i)
            result_bag = Bag(result)
            return result_bag
    
    def remove(self, argument):
        if argument not in self._bag.keys():
            raise ValueError
        else:
            self._bag[argument] -= 1
            if self._bag[argument] == 0:
                self._bag.pop(argument)
    
    def __eq__(self, other_bag):
        if type(other_bag) != Bag:
            return False
        for i in self._bag:
            if i in other_bag._bag.keys():
                if self._bag[i] != other_bag._bag[i]:
                    return False
            elif i not in other_bag._bag.keys():
                return False
        return True
    
    def __ne__(self, other_bag):
        if type(other_bag) != Bag:
            return True
        for i in self._bag:
            if i in other_bag._bag.keys():
                if self._bag[i] != other_bag._bag[i]:
                    return True
            elif i not in other_bag._bag.keys():
                return True
        return False
            
    def __iter__(self):
        result = list()
        for i in self._bag:
            for j in range(self._bag[i]):
                result.append(i)
        return iter(result)
    
  



if __name__ == '__main__':
    
    #Simple tests before running driver
    #Put your own test code here to test Bag before doing the bsc tests
    #Debugging problems with these tests is simpler

    b = Bag(['d','1','2','3','b','b','d'])
    print(repr(b))
    print(all((repr(b).count('\''+v+'\'')==c for v,c in dict(a=1,b=2,c=1,d=3).items())))
    for i in b:
        print(i)
    b2 = Bag(['a','a','b','x','d'])
    print(b == b2)
    print(repr(b2+b))
    print(str(b2+b))
    print([repr(b+b2).count('\''+v+'\'') for v in 'abdx'])
    b = Bag(['a','b','a'])
    print(repr(b))
    print()
    
    import driver
    driver.default_file_name = 'bscp21W22.txt'
#     driver.default_show_exception = prompt.for_bool('Show exceptions when testing',True)
#     driver.default_show_exception_message = prompt.for_bool('Show exception messages when testing',True)
#     driver.default_show_traceback = prompt.for_bool('Show traceback when testing',True)
    driver.driver()
