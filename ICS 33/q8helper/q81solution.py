from random import randrange
from influence import find_influencers
from goody import irange
from performance import Performance
import random

def random_graph (nodes : int, edges : int) -> {str:{str}}:
    g = {str(n) :set() for n in range(nodes)}
    
    for i in range(int(edges)):
        n1 = str(randrange(nodes))
        n2 = str(randrange(nodes))
        if n1 != n2 and n1 in g and n2 not in g[n1]:
            g[n1].add(n2)
            g[n2].add(n1)
    return g


# Put the code here to generate data for Quiz 8 problem #1
def create_random(num):
    global ran
    edge_num = 5*num
    ran = random_graph(num, edge_num) 
for i in irange(0,7):
    n = 100 * 2**i
    try:
        p = Performance(lambda : find_influencers(ran), lambda : create_random(n),5, f'find_influencers of size {n}')
        p.evaluate()
        p.analyze()
        print()
    except ValueError:
        print(f'find_influencers of size {n}')
        print('Analysis of 5 timings')
        print("0 time-elapsed; cannot show analysis\n")