from distance_helper import min_dist
import prompt
from collections import defaultdict


class Misspelling:
    # __init__ must call this method so I can test Misspelling more easily.
    # This method should consist of only new bindings to attribute names.
    # It should (and will when I grade it) always have a list binding
    #   self.last = some value
    # You may put your own bindings here using any names to test your code;
    #   when I grade your work, I will replace the contents of this method
    #   but the last binding will always be to the attribute last
    def initialize_attributes(self):
        self.amoral = 1
        self.more   = 2
        self.babel  = 3
        self.last   = 4
        
        
    def __init__(self, fix_when_setting=False):
        self.fix_when_setting = fix_when_setting
        self.initialize_attributes()
     
     
    def closest_matches(self,name):
        result_dict = defaultdict(list)
        empty_list = list()
        min_value = len(name)//2
        for key, value in self.__dict__.items():
            minimum = min_dist(key, name)
            if minimum <= min_value:
                result_dict[minimum].append(key)
        if len(result_dict) > 0:
            return result_dict[min(result_dict.keys())]
        else:
            return empty_list
     
     
    def __getattr__(self,name): 
        result = self.closest_matches(name)
        if len(result) == 1:
            return self.__dict__[result[0]]
        else:
            raise NameError(f'(\'Misspelling.__getattr__: name({name}) not found; matches =\', {result})')

    def __setattr__(self,name,value):
        if 'last' not in self.__dict__:
            self.__dict__[name] = value
        elif 'last' in self.__dict__ and name in self.__dict__:
            self.__dict__[name] = value
        if name == 'last':
            self.__dict__['last'] = value
        if name not in self.__dict__ and self.fix_when_setting == True:
            result = self.closest_matches(name)
            if len(result) == 1:
                self.__dict__[result[0]] = value
            else:
                raise NameError(f'(\'Misspelling.__setattr__: name({name}) not found; matches =\', {result})')
        if name not in self.__dict__ and self.fix_when_setting == False:
            raise NameError(f"Misspelling.__setattr__: name({name}) not found and spelling correction disabled")
        
        
        



# You should try to understand the specifications and test your code
#   to ensure it works correctly, according to the specification.
# You are all allowed to ask "what should my code do..." questions online
#   both before and after I post my batch self_check file.
# The driver below will allow you to easily test your code.

if __name__ == '__main__':
    o = Misspelling(prompt.for_bool("Allow fixing mispelling in __setattr__",True)) 
    # Put code here to test object o the same way each time the code is run
    
    
    # Use the while loop below to type in code one on-the-fly when prompted
    while True:
        try:
            print("\n" + str(o.__dict__))   
            test = prompt.for_string("Enter test")
            if test == "quit":
                break;
            if '=' not in test:
                print(eval(test))
            else:
                exec(test)
        except Exception as exc:
            print(exc)



    print()
    import driver
    
    driver.default_file_name = 'bscq32W22.txt'
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()
