from goody import irange, type_as_str
from nntplib import NNTP


class Date:
    month_dict = {1:31, 2:28, 3:31, 4:30, 5:31, 6:30, 7:31, 8:31, 9:30, 10:31, 11:30, 12:31}

    # Q1
    def __init__(self, year, month, day):
        if year >= 0 and type_as_str(year) == "int":
            self.year = year
        else:
            raise AssertionError
        if ((1 <= month <= 12) and type_as_str(month) == "int"):
            self.month = month
        else:
            raise AssertionError
        if day <= self.days_in(year, month) and type_as_str(day) == "int":
            self.day = day
        else:
            raise AssertionError
    
    # Q2
    def __getitem__(self, index):
        if type_as_str(index) == 'str' and (index == 'y' or index == 'm' or index == 'd'):
            if index == 'y':
                return self.year
            if index == 'm':
                return self.month
            if index == 'd':
                return self.day
        elif type_as_str(index) != 'tuple':
            raise IndexError
        if type(index) == tuple:
            result = tuple()
            for i in range(len(index)):
                if(index[i] == 'y'):
                    result += (self.year,)
                elif(index[i] == 'm'):
                    result += (self.month,)
                elif(index[i] == 'd'):
                    result += (self.day,)
                else:
                    raise IndexError
            return result
    
    # Q3
    def __repr__(self):
        return f'Date({self.year},{self.month},{self.day})'

    # Q3
    def __str__(self):
        return f'{self.month}/{self.day}/{self.year}'
    
    # Q4
    def __len__(self):
        result = 0
        for years in range(0, self.year + 1):
            for months in range(1, 13):
                for days in range(1, self.days_in(years, months) + 1):
                    if (years == self.year and months == self.month and days == self.day):
                        return result
                    else:
                        result += 1
    
    # Q5
    def __eq__(self, other):
        try:
            if self.year == other.year and self.month == other.month and self.day == other.day:
                return True
            else:
                return False
        except AttributeError:
            return False
        
    # Q6
    def __lt__(self, other):
        if isinstance(other, self.__class__):
            if self.year < other.year:
                return True
            elif self.year == other.year:
                if self.month < other.month:
                    return True
                elif self.month == other.month:
                    if self.day < other.day:
                        return True
                    else:
                        return False
            return False
        elif isinstance(other, int):
            if self.__len__() < other:
                return True
            return False
        return NotImplemented
    
    # Q7
    def __add__(self, other):
        if type(self) is Date and type(other) is int:
            result = Date(self.year, self.month, self.day)
            if (other > 0):
                for i in range(other):
                    if(result.day <= result.days_in(result.year, result.month)):
                        result.day += 1
                    if result.day > result.days_in(result.year, result.month):
                        result.month += 1
                        result.day = 1
                    if result.month > 12:
                        result.year += 1
                        result.month = 1
                        result.day = 1
            if (other < 0):
                for i in range(abs(other)):
                    if result.day > 0:
                        result.day -= 1
                    if result.day == 0:
                        result.month -= 1
                        if result.month == 0:
                            result.year -= 1
                            result.month = 12
                            result.day = 31
                        elif result.month > 0:
                            result.day = result.days_in(result.year, result.month)
                    
            return result
        else:
            return NotImplemented
    
    def __radd__(self, other): 
        return self.__add__(other)
                
    def __sub__(self, other):
        result = Date(self.year, self.month, self.day)
        if type(other) is Date and type(self) is Date:
            return self.__len__() - other.__len__()
        elif type(other) is int and type(self) is Date:
            result = Date(self.year, self.month, self.day)
            if (other > 0):
                for i in range(abs(other)):
                    if result.day > 0:
                        result.day -= 1
                    if result.day == 0:
                        result.month -= 1
                        if result.month == 0:
                            result.year -= 1
                            result.month = 12
                            result.day = 31
                        elif result.month > 0:
                            result.day = result.days_in(result.year, result.month)
            if (other < 0):
                for i in range(abs(other)):
                    if(result.day <= result.days_in(result.year, result.month)):
                        result.day += 1
                    if result.day > result.days_in(result.year, result.month):
                        result.month += 1
                        result.day = 1
                    if result.month > 12:
                        result.year += 1
                        result.month = 1
                        result.day = 1
            return result
        else:
            return NotImplemented
        
    def __rsub__(self, other):
        return self.__sub__(other)
    
    def __call__(self, year, month, day):
        if year >= 0 and type_as_str(year) == "int":
            self.year = year
        else:
            raise AssertionError
        if ((1 <= month <= 12) and type_as_str(month) == "int"):
            self.month = month
        else:
            raise AssertionError
        if day <= self.days_in(year, month) and type_as_str(day) == "int":
            self.day = day
        else:
            raise AssertionError
        return None
    
    @staticmethod
    def is_leap_year(year):
        return (year % 4 == 0 and year % 100 != 0) or year % 400 == 0
    
    @staticmethod
    def days_in(year, month):
        return Date.month_dict[month] + (1 if month == 2 and Date.is_leap_year(year) else 0)

    

if __name__ == '__main__':
    # Put in your own simple tests for Date before allowing driver to run
    
    print()
    import driver
    
    driver.default_file_name = 'bscq31W22.txt'
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()
        
