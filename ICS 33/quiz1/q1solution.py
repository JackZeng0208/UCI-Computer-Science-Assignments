from predicate import is_even, is_prime, is_positive
from collections import defaultdict  # Use or ignore
from _elementtree import Element


def integrate(f : callable, n : int) -> callable:
    global zcc
    integrate.zcc = 0
    def sign_change(x, y):
        x = int(x)
        y = int(y)
        return ((x^y)<0)
    if(is_positive(n) != True):
        raise AssertionError
    def solve(a,b):
        result = 0.0
        current = 0.0
        dx = (b-a)/n
        value = a
        if(a<=b):
            for i in range(0, n):
                if(sign_change(current, result) == True):
                    integrate.zcc +=1
                result += f(value)*dx
                current = result
                value += dx
            return result
        else:
            raise AssertionError
    return solve



def stocks(db : {str: [(str,int,int)]}) -> {str}:
    return {i[0] for value in db.values() for i in value if i[0] not in value}



def clients_by_volume(db : {str: [(str,int,int)]}) -> [str]:
    return [j[0] for j in sorted({key:sum(abs(i[1]) for i in value) for key, value in db.items()}.items(), key=lambda d:(-d[1],d[0]))]



def stocks_by_volume(db : {str: [(str,int,int)]}) -> [(str,int)]:
    return sorted({elements:sum(abs(j[1]) for j in [i for value in db.values() for i in value] if j[0] == elements) for elements in stocks(db)}.items(), key=lambda d:(-d[1],d[0]))
    


def by_stock(db : {str: [(str,int,int)]}) -> {str: {str: [(int,int)]}}:
    stock = set()
    for value in db.values():
        for i in value:
            if i[0] not in value:
                if i[0] not in stock:
                    stock.add(i[0])
    answer = dict.fromkeys(stock, {})
    for key in answer.keys():
        temp = dict()
        for key2, value2 in db.items():
            l = []
            for i in value2:
                if(key == i[0] and (key2 not in temp)):
                    temp.update({key2:l})
                if(key == i[0] and (key2 in temp)):
                    l.append((i[1],i[2]))
        answer[key] = temp
    return answer

def summary(db : {str: [(str,int,int)]}, prices : {str: int}) -> {str: ({str: int}, int)}:
    answer = dict()
    temp = dict()
    for key,value in db.items():
        temp = dict()
        total_value = 0
        record_zero = []
        for i in value:
            if(i[0] not in temp):
                temp.update({i[0]:0})
            temp[i[0]] += i[1]
        for key2, value2 in temp.items():
            if(value2 == 0):
                record_zero.append(key2)
        for j in record_zero:
            temp.pop(j)
        for key2, value2 in temp.items():
            price = prices[key2] * value2
            total_value += price
        answer.update({key: (temp, total_value)})
    return answer
            
 





if __name__ == '__main__':
    print('Testing integrate')
    f = integrate( (lambda x : x), 10)
    f.zcc = 1
    print(f(-1,1),f.zcc)
    f = integrate( (lambda x : 3*x**2 - 6*x + 1), 1000)
    f.zcc = 2
    print(f(-1,2),f.zcc)
    f = integrate( (lambda x : -5*x**5 + 3*x**4 + 8*x**3 - 1), 1000)
    f.zcc = 3
    print(f(-2,2),f.zcc)
  
   
    # Note: the keys in this dicts are not specified in alphabetical order
    db1 = {
            'Carl': [('Intel', 30, 40), ('Dell' , 20, 50), ('Intel',-10, 60), ('Apple', 20, 55)],
            'Barb': [('Intel', 20, 40), ('Intel',-10, 45), ('IBM',   40, 30), ('Intel',-10, 35)],
            'Alan': [('Intel', 20, 10), ('Dell',  10, 50), ('Apple', 80, 80), ('Dell', -10, 55)],
            'Dawn': [('Apple', 40, 80), ('Apple', 40, 85), ('Apple',-40, 90)]
           }

    db2 = {
            'Hope': [('Intel',30,40), ('Dell',20,50), ('IBM',10,80), ('Apple',20,90), ('QlCom',20,20)],
            'Carl': [('QlCom',30,22), ('QlCom',20,23), ('QlCom',-20,25), ('QlCom',-30,28)],
            'Barb': [('Intel',80,42), ('Intel',-80,45), ('Intel',90,28)],
            'Fran': [('BrCom',210,62), ('BrCom',-20,64), ('BrCom',-10,66), ('BrCom',10,55)],
            'Alan': [('Intel',20,10), ('Dell', 10,50), ('Apple',80,80), ('Dell',-10,55)],
            'Gabe': [('IBM',40,82), ('QlCom',80,25), ('IBM',-20,84), ('BrCom',50,65), ('QlCom',-40,28)],
            'Dawn': [('Apple',40,92), ('Apple',40,98), ('Apple',-40,92)],
            'Evan': [('Apple',50,92), ('Dell',20,50), ('Apple',-10,95), ('Apple',20,95), ('Dell',-20,90)]
           }

    prices1 = {'IBM': 65, 'Intel': 60, 'Dell': 55, 'Apple': 70}
    
    prices2 = {'IBM': 85, 'Intel': 45, 'Dell': 50, 'Apple': 90, 'QlCom': 20, 'BrCom': 70}
   

    print('\nTesting stocks')
    print('stocks(db1):',stocks(db1))
    print('stocks(db2):',stocks(db2))


    print('\nTesting clients_by_volume')
    print ('clients_by_volume(db1):',clients_by_volume(db1))
    print ('clients_by_volume(db2):',clients_by_volume(db2))


    print('\nTesting stocks_by_volume')
    print ('stocks_by_volume(db1):',stocks_by_volume(db1))
    print ('stocks_by_volume(db2):',stocks_by_volume(db2))


    print('\nTesting by_stock')
    print ('by_stock(db1):',by_stock(db1))
    print ('by_stock(db2):',by_stock(db2))


    print('\nTesting summary')
    print ('summary(db1):',summary(db1,prices1))
    print ('summary(db2):',summary(db2,prices2))


    print('\ndriver testing with batch_self_check:')
    import driver
    driver.default_file_name = 'bscq1W22.txt'
#     driver.default_show_traceback = True
#     driver.default_show_exception = True
#     driver.default_show_exception_message = True
    driver.driver()
