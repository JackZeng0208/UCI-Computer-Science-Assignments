#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    srand(time(NULL));
    int v[13];
    for (int i = 1; i <= 12; i++)
        v[i] = 0;
    for (int i = 1; i <= 100; i++)
    {
        int dice_1 = rand() % (6 + 1 - 1) + 1;
        int dice_2 = rand() % (6 + 1 - 1) + 1;
        int sum = dice_1 + dice_2;
        v[sum] += 1;
    }
    for (int i = 2; i <= 12; i++)
    {
        
        printf("%d: ", i);
        char temp[10];
        sprintf(temp, "%d", v[i]);
        if(i < 10)
            printf("%-6s", temp);
        else
            printf("%-5s", temp);
        for (int j = 0; j < v[i]; j++)
            printf("*");
        printf("\n");
    }
    return 0;
}