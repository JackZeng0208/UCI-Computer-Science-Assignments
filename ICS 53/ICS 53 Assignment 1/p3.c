#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct calendar
{
    char event[101];
    int start;
    int end;
};

int cmp_by_start_time(const void *p1, const void *p2)
{
    struct calendar *x = (struct calendar *)p1;
    struct calendar *y = (struct calendar *)p2;
    return (x->start - y->start);
}

int main()
{
    struct calendar c[101];
    char command[21];
    int t[24];
    int count = 0;
    for (int i = 0; i < 24; i++)
        t[i] = 0;
    for (int i = 0; i < 101; i++)
    {
        strncpy(c[i].event, "", sizeof(c[i].event));
        c[i].start = -1;
        c[i].end = -1;
    }
    while (1)
    {
        printf("$ ");
        scanf("%s", &command);
        if (strcmp(command, "quit") == 0)
            break;
        if (strcmp(command, "add") == 0)
        {
            char event_name[101];
            int start_time;
            int end_time;
            int judge = 0;
            scanf("%s%d%d", &event_name, &start_time, &end_time);
            if (start_time >= end_time || (start_time < 0) || (start_time > 23) || (end_time < 0) || (end_time > 23))
                printf("Event overlap error\n");
            else
            {
                for (int i = start_time; i <= end_time; i++)
                {
                    if (t[i] == 0)
                        t[i] = 1;
                    else if ((i == start_time && t[i] == 1) || (i == end_time && t[i] == 1))
                    {
                        continue;
                    }
                    else
                    {
                        judge = 1;
                        for (int j = start_time; j <= i; j++)
                            t[j] = 0;
                        printf("Event overlap error\n");
                        break;
                    }
                }
                if (judge == 0)
                {
                    strcpy(c[count].event, event_name);
                    c[count].start = start_time;
                    c[count].end = end_time;
                    count++;
                }
            }
        }
        if (strcmp(command, "delete") == 0)
        {
            char delete_name[101];
            scanf("%s", &delete_name);
            int start_time_temp = 0;
            int end_time_temp = 0;
            for (int i = 0; i < count; i++)
            {
                if (strcmp(c[i].event, delete_name) == 0)
                {
                    start_time_temp = c[i].start;
                    end_time_temp = c[i].end;
                    for (int j = start_time_temp; j <= end_time_temp; j++)
                        if (t[j] == 1)
                            t[j] = 0;
                    strncpy(c[i].event, "", sizeof(c[i].event));
                    c[i].start = -1;
                    c[i].end = -1;
                    count++;
                    break;
                }
            }
        }
        if (strcmp(command, "printcalendar") == 0)
        {
            qsort(c, count, sizeof(c[0]), cmp_by_start_time);
            for (int i = 0; i < count; i++)
            {
                if (strcmp(c[i].event, "") != 0)
                {
                    printf("%s %d %d\n", c[i].event, c[i].start, c[i].end);
                }
            }
        }
    }
}