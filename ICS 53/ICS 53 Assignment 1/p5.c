#include <stdio.h>
void madd(void **a, void **b, int m, int n)
{
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            *((int *)a + i*n + j) = *((int *)a + i*n + j) + *((int *)b + i*n + j);
            //printf("%d ", *((int *)a + i*n + j));
        }
        //printf("\n");
    }
}

int main()
{
    int a[3][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, b[3][3] = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, m = 3, n = 3;
    madd(a, b, m, n);
}