#include <stdio.h>
#include <string.h>

int main()
{
    char input[101] = "\0";
    fgets(input, 100, stdin);
    input[strlen(input) - 1] = '\0';
    int multi_space_judge = 0;
    for (int i = 0; i < strlen(input); i++)
    {
        if ((input[i] == ' ' || input[i] == '\t') && multi_space_judge == 0)
        {
            multi_space_judge = 1;
            printf("\n");
        }
        else
        {
            if (input[i] != ' ')
            {
                multi_space_judge = 0;
                putchar(input[i]);
            }
        }       
    }
    printf("\n");
    puts(input);
    return 0;
}